class CfgPatches
{
	class TFV_Units_Bluefor_Vehicles_Aircraft
	{
		units[]=
		{
			"TFV_SOCOM_Pelican",
		};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"OPTRE_Vehicles_Air"
		};
		addonRootClass="OPTRE_Vehicles_Air";
	};
};
class CfgVehicles
{
    class Components;
    class Turrets;
    class Pylons;
    class CargoTurret_01;
    class CargoTurret_02;
    class TransportPylonsComponent;
	class OPTRE_Pelican_armed_SOCOM;
	class TFV_SOCOM_Pelican_Hobnob: OPTRE_Pelican_armed_SOCOM
	{
        editorPreview="\OPTRE_Misc\Image\OPTRE\Aircraft\OPTRE_Pelican_armed_CMA.jpg";
		scope=2;
		scopeCurator=2;
		side=1;
		faction="TFV";
		editorCategory="TFV_Editor_Category";
        editorSubcategory="TFV_Editor_Subcategory_Air";
		crew = "TFV_Unit_Valkyire_11_Pilot";
		author="Task Force Vargr Aux Team";
		displayName="Hobnob's Pelican";
		memoryPointLMissile = "Rocket_1";
		memoryPointRMissile = "Rocket_2";
		weapons[] = {"CMFlareLauncher","OPTRE_missiles_Anvil1","OPTRE_M370","OPTRE_missiles_C2GMLS"};
		magazines[] = {"168Rnd_CMFlare_Chaff_Magazine","OPTRE_32Rnd_Anvil1_missiles","OPTRE_12Rnd_C2GMLS_missiles","OPTRE_750Rnd_70mm_HE"};
        memoryPointGun="machinegun";
		memoryPointGunnerOutOptics="";
		memoryPointGunnerOptics="gunner1";
		hiddenSelections[] = {"camo1","camo3","clan","clan_text","insignia"};
		hiddenSelectionsTextures[] = {"\TFV_Units\Bluefor\TFV_Vehicles\Aircraft\Hobnob_Special\data\TFV_SOCOM_Hobnob_CO.paa", ""};
		class Components : Components
        {
            delete TransportPylonsComponent;
        };
		class Turrets: Turrets
		{
			class CopilotTurret;
			class CargoTurret_01: CargoTurret_01
			{
			};
			class CargoTurret_02: CargoTurret_02
			{
			};
		};
		class textureSources
		{
			class TFV_SOCOM_Hobnob
			{
				displayName = "TFV SOCOM Pelican Skin";
				author = "Task Force Vargr Aux Team";
				textures[] = {"\TFV_Units\Bluefor\TFV_Vehicles\Aircraft\Hobnob_Special\data\TFV_SOCOM_Hobnob_CO.paa"};
			};
		};
		textureList[] = {"TFV_SOCOM_Hobnob",1};
	};
};
