class CfgPatches
{
	class TFV_Units_Bluefor
	{
		author="Task Force Vargr Aux Team";
		name="TaskForceVargr";
		addonRootClass="TFV_Units";
		units[]=
			{
				"TFV_Unit_Berserker_Riflemen",
    			"TFV_Unit_Berserker_Medic",
    			"TFV_Unit_Berserker_SL",
    			"TFV_Unit_Berserker_GL",
    			"TFV_Unit_Berserker_AT",
    			"TFV_Unit_Berserker_AR",
    			"TFV_Unit_Berserker_DM",
				"TFV_Unit_Valkyire_11_Pilot",
				"TFV_Unit_Flight_Cadet",
				"TFV_SOCOM_Pelican_Hobnob",
				"TFV_SOCOM_Pelican",
			};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"OPTRE_Vehicles",
			"OPTRE_Vehicles_air",
			"OPTRE_Weapons",
			"A3_Modules_F",
    		"A3_Functions_F",
		};
	};
};
class CfgFactionClasses
{
    class TFV
    {
        displayName = "[Vargr] UNSC";
        priority=1;
		side=1;
		primaryLanguage="EN";
		backpack_tf_faction_radio_api="OPTRE_Como_pack_2";
    };
};
class CfgEditorCategories
{
	class TFV_Editor_Category
	{
		displayName="[Vargr] UNSC";
	};
};
class CfgEditorSubcategories
{
    class TFV_Editor_Subcategory_Air
    {
        displayName = "Aircraft";
    };
    class TFV_Editor_Subcategory_Armor
    {
        displayName = "Armored Vehicles";
    };
    class TFV_Editor_Subcategory_ODST
    {
        displayName = "ODST";
    };
    class TFV_Editor_Subcategory_Marines
    {
        displayName = "Marines";
    };
    class TFV_Editor_Subcategory_Pilots
    {
        displayName = "Pilots";
    };
    class TFV_Editor_Subcategory_Vehicles
    {
        displayName = "Light Vehicles";
    };
    class TFV_Editor_Subcategory_Turrets
    {
        displayName = "Turrets";
    };
};
class CfgGroups
{
	class West
	{
		class TFV
		{
			name="Task Force Vargr";
            class TFV_ODST_Groups
			{
				dlc="TFV";
				name="Berserker ODST";
				class TFV_Sentry
				{
					name="ODST Sentry";
					side=1;
					faction="TFV";
					rarityGroup=0.30000001;
					dlc="TFV";
					class Unit0
					{
						side=1;
						vehicle="TFV_Unit_Berserker_Riflemen";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=1;
						vehicle="TFV_Unit_Berserker_Medic";
						rank="PRIVATE";
						position[]={-1,0,0};
					};
				};
				class TFV_ODST_Squad
				{
					name="ODST Squad";
					side=1;
					faction="TFV";
					rarityGroup=0.30000001;
					dlc="TFV";
					class Unit0
					{
						side=1;
						vehicle="TFV_Unit_Berserker_SL";
						rank="LIEUTENANT";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=1;
						vehicle="TFV_Unit_Berserker_SL";
						rank="SERGEANT";
						position[]={0,-1,0};
					};
					class Unit2
					{
						side=1;
						vehicle="TFV_Unit_Berserker_GL";
						rank="CORPORAL";
						position[]={-1,-1,0};
					};
					class Unit3
					{
						side=1;
						vehicle="TFV_Unit_Berserker_GL";
						rank="CORPORAL";
						position[]={1,-1,0};
					};
					class Unit4
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AT";
						rank="PRIVATE";
						position[]={-1,-2,0};
					};
					class Unit5
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AT";
						rank="PRIVATE";
						position[]={1,-2,0};
					};
					class Unit6
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AR";
						rank="PRIVATE";
						position[]={-1,-3,0};
					};
					class Unit7
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AR";
						rank="PRIVATE";
						position[]={1,-3,0};
					};
					class Unit8
					{
						side=1;
						vehicle="TFV_Unit_Berserker_DM";
						rank="PRIVATE";
						position[]={-1,-4,0};
					};
                    class Unit9
					{
						side=1;
						vehicle="TFV_Unit_Berserker_DM";
						rank="PRIVATE";
						position[]={1,-4,0};
					};
					class Unit10
					{
						side=1;
						vehicle="TFV_Unit_Berserker_Medic";
						rank="PRIVATE";
						position[]={-1,-5,0};
					};
                    class Unit11
					{
						side=1;
						vehicle="TFV_Unit_Berserker_Medic";
						rank="PRIVATE";
						position[]={1,-5,0};
					};
				};
				class TFV_ODST_Fireteam
				{
					name="ODST Fireteam";
					side=1;
					faction="TFV";
					rarityGroup=0.30000001;
					dlc="TFV";
					class Unit0
					{
						side=1;
						vehicle="TFV_Unit_Berserker_GL";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AT";
						rank="PRIVATE";
						position[]={-1,0,0};
					};
					class Unit2
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AR";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
					class Unit3
					{
						side=1;
						vehicle="TFV_Unit_Berserker_DM";
						rank="PRIVATE";
						position[]={-3,0,0};
					};
                    class Unit4
					{
						side=1;
						vehicle="TFV_Unit_Berserker_Medic";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
				};
				class TFV_ODST_Weapons_Team
				{
					name="ODST Weapons Team";
					side=1;
					faction="TFV";
					rarityGroup=0.30000001;
					dlc="TFV";
					class Unit0
					{
						side=1;
						vehicle="TFV_Unit_Berserker_GL";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AR";
						rank="PRIVATE";
						position[]={-1,0,0};
					};
					class Unit2
					{
						side=1;
						vehicle="TFV_Unit_Berserker_Riflemen";
						rank="PRIVATE";
						position[]={-1,0,0};
					};
				};
				class TFV_ODST_AntiTank
				{
					name="ODST Anti-Tank Team";
					side=1;
					faction="TFV";
					rarityGroup=0.30000001;
					dlc="TFV";
					class Unit0
					{
						side=1;
						vehicle="TFV_Unit_Berserker_GL";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=1;
						vehicle="TFV_Unit_Berserker_AT";
						rank="PRIVATE";
						position[]={-1,0,0};
					};
				};
			};
		};
	};
};
