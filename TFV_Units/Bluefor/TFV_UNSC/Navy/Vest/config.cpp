class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Vest
    {
        units[]={};
        weapons[]=
        {
            //Vest
            "TFV_Pilot_Base",
            //Recruit Vests
            "TFV_Pilot_Vest", 
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class ItemInfo;
    class VestItem;
    class HeadgearItem;
    class HitpointsProtectionInfo;
    // Vest
    class V_PlateCarrier1_rgr;
    class TFV_Pilot_Base: V_PlateCarrier1_rgr
    {
        scope=1;
        scopeArsenal=1;
        author="Misriah Armory";
        displayName="[MA] Marine Pouches Base";
        model="MA_Armor\data\Vests\Marine_Pouches\Marine_Pouches.p3d";
        picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
        hiddenSelections[]=
        {
              "camo1",
              "camo2",
              "camo3",
              "camo4",
              "camo5",
              "camo6",
              "camo7",
              "camo8",
              "camo9",
              "camo10",
              "camo11",
              "camo12",
              "camo13",
              "camo14",
              "camo15",
              "camo16",
              "camo17",
              "camo18",
              "camo19",
              "camo20",
    //          "ChestPMLeft",
    //           "ChestPMRight",
    //           "ChestPouch",
    //           "ChestRadio",
    //           "LegLeft",
    //           "LegRight",
    //           "StomachPouch",
    //           "TorsoPM",
    //           "TorsoPMLeft",
    //           "TorsoPMRight",
    //           "TorsoPouch",
    //           "WaistBack",
    //           "WaistGLeft",
    //           "WaistGRight",
    //           "WaistPLeft",
    //           "WaistPRight",
    //           "WaistRLeft",
    //           "WaistRRight",
    //           "WaistSLeft",
    //           "WaistSRight"
        };
        hiddenSelectionsTextures[]=
        {
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
              "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_Green_co.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
              "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_Green_co.paa",
              "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_Green_co.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
              "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa"
        };
        class ItemInfo: VestItem
        {
            hiddenSelections[]=
            {
                "camo1",
                "camo2",
                "camo3",
                "camo4",
                "camo5",
                "camo6",
                "camo7",
                "camo8",
                "camo9",
                "camo10",
                "camo11",
                "camo12",
                "camo13",
                "camo14",
                "camo15",
                "camo16",
                "camo17",
                "camo18",
                "camo19",
                "camo20",
       //          "ChestPMLeft",
       //          "ChestPMRight",
       //          "ChestPouch",
       //          "ChestRadio",
       //          "LegLeft",
       //          "LegRight",
       //          "StomachPouch",
       //          "TorsoPM",
       //          "TorsoPMLeft",
       //          "TorsoPMRight",
       //          "TorsoPouch",
       //          "WaistBack",
       //          "WaistGLeft",
       //          "WaistGRight",
       //          "WaistPLeft",
       //          "WaistPRight",
       //          "WaistRLeft",
       //          "WaistRRight",
       //          "WaistSLeft",
       //          "WaistSRight"
            };
            hiddenSelectionsTextures[]=
            {
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
                "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_Green_co.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa",
                "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_Green_co.paa",
                "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_Green_co.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa",
                "MA_Armor\data\Vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa"
            };
            uniformModel="MA_Armor\data\Vests\Marine_Pouches\Marine_Pouches.p3d";
            containerClass="Supply400";
            mass=20;
            passThrough=0.1;
            modelSides[]={6};
            class HitpointsProtectionInfo
            {
                class Neck
                {
                    hitpointName="HitNeck";
                    armor=20;
                    passThrough=0.5;
                };
                class Legs
                {
                    hitpointName="HitLegs";
                    armor=20;
                    passThrough=0.5;
                };
                class Arms
                {
                    hitpointName="HitArms";
                    armor=25;
                    passThrough=0.1;
                };
                class Hands
                {
                    hitpointName="HitHands";
                    armor=20;
                    passThrough=0.1;
                };
                class Chest
                {
                    hitpointName="HitChest";
                    armor=35;
                    passThrough=0.1;
                };
                class Diaphragm
                {
                    hitpointName="HitDiaphragm";
                    armor=30;
                    passThrough=0.1;
                };
                class Abdomen
                {
                    hitpointName="HitAbdomen";
                    armor=30;
                    passThrough=0.1;
                };
                class Pelvis
                {
                    hitpointName="HitPelvis";
                    armor=30;
                    passThrough=0.1;
                };
                class Body
                {
                    hitpointName="HitBody";
                    passThrough=0.1;
                };
            };
        };
    };
    // Cadet Armor
    class TFV_Pilot_Vest: TFV_Pilot_Base
    {
        scope=2;
        scopeArsenal=2;
        author="Misriah Armory";
        displayName="[TFV] Valkyrie Vest";
        hiddenSelections[]=
        {
            "camo1",
            "camo2",
            "camo3",
            "camo4",
            "camo5",
            "camo6",
            "camo7",
            "camo8",
            "camo9",
            "camo10",
            "camo11",
            "camo12",
            "camo13",
            "camo14",
            "camo15",
            "camo16",
            "camo17",
            "camo18",
            "camo19",
            "camo20",
            "ChestPMLeft",
            //"ChestPMRight",
            "ChestPouch",
            //"ChestRadio",
            "LegLeft",
            "LegRight",
            "StomachPouch",
            //"TorsoPM",
            "TorsoPMLeft",
            "TorsoPMRight",
            "TorsoPouch",
            "WaistBack",
            "WaistGLeft",
            //"WaistGRight",
            "WaistPLeft",
            "WaistPRight",
            "WaistRLeft",
            "WaistRRight",
            "WaistSLeft",
            "WaistSRight"
        };
        class ItemInfo: ItemInfo
        {
            hiddenSelections[]=
            {
                "camo1",
                "camo2",
                "camo3",
                "camo4",
                "camo5",
                "camo6",
                "camo7",
                "camo8",
                "camo9",
                "camo10",
                "camo11",
                "camo12",
                "camo13",
                "camo14",
                "camo15",
                "camo16",
                "camo17",
                "camo18",
                "camo19",
                "camo20",
                "ChestPMLeft",
                //"ChestPMRight",
                "ChestPouch",
                //"ChestRadio",
                "LegLeft",
                "LegRight",
                "StomachPouch",
                //"TorsoPM",
                "TorsoPMLeft",
                "TorsoPMRight",
                "TorsoPouch",
                "WaistBack",
                "WaistGLeft",
                //"WaistGRight",
                "WaistPLeft",
                "WaistPRight",
                "WaistRLeft",
                "WaistRRight",
                "WaistSLeft",
                "WaistSRight"
            };
        };
    };
};
class CfgVehicles{};