class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_Navy_Uniforms
    {
        units[]={};
        weapons[]=
        {
            //Uniform
            "TFV_Valkyrie_BDU",
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class U_B_CombatUniform_mcam;
    class TFV_Pilot_BDU: U_B_CombatUniform_mcam
    {
        scope=2;
        scopeArsenal=2;
        author="Task Force Vargr Aux Team";
        displayName="[TFV] Pilot BDU";
        model="\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
        picture="\MA_Armor\data\Icons\H3_ODST_Uniform.paa";
        class ItemInfo: UniformItem
        {
            uniformModel="-";
            uniformClass="TFV_Pilot_BDU_Base";
            containerClass="Supply200";
            mass=10;
            uniformType="Neopren";
            modelSides[]={6};
        };
    };
};
class CfgVehicles
{
    class B_Soldier_base_F;
    class TFV_Pilot_Base: B_Soldier_base_F
    {
        scope=0;
        scopeCurator=0;
        side=1;
        author="Task Force Vargr Aux Team";
        UniformClass="";
        vehicleClass="";
        model="";
        weapons[]={"Throw","Put"};
        respawnWeapons[]= {"Throw","Put"};
        Items[]={"OPTRE_Biofoam"};
        RespawnItems[]={"OPTRE_Biofoam"};
        magazines[]={};
        respawnMagazines[]={};
        hiddenSelections[]=
        {
            "camo"
        };
        hiddenSelectionsTextures[]={};
    };
    class TFV_Pilot_BDU_Base: TFV_Pilot_Base
    {
        scope=2;
        scopeArsenal=2;
        UniformClass="TFV_Pilot_BDU";
        model="MA_Armor\data\Uniforms\Marine\Marine_Uniform.p3d";
        picture="\MA_Armor\data\Icons\H3_ODST_Uniform.paa";
        hiddenSelections[]=
        {
            "Camo1",
            "Camo2",
            "Camo3",
            "Camo4",
            "Camo5",   
            "Camo6",
            "Camo7",
            "Camo8",
            "Camo9",
            "Camo10",
        //  "Uni_Upper",
        //  "Uni_Lower",
        //  "Uni_Collar",
        //  "Armor_Underpad",
        //  "Armor_Straps",
        //  "Armor_Upper",
        //  "Armor_Lower",
            "Shoulders_Lower1",
            "Shoulders_Lower2",
            "Shoulders_Upper"
        };
        hiddenSelectionsTextures[]=
        {
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Upper_BDU_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Lower_BDU_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Collar_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Padding_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Straps_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Upper_Armor_Co.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Lower_Armor_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Shoulders_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Shoulders_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Undersuit\Valkyrie_Shoulders_CO.paa"
        };
    };
};