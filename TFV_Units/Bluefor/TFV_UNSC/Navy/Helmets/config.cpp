class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Helmets
    {
        units[]={};
        weapons[]=
        {
            //Helmets
            "TFV_Pilot_Helmet_Base",
			//Recruit Helmet
			"TFV_Pilot_Helmet_Cadet",
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class ItemInfo;
    class VestItem;
    class HeadgearItem;
    class HitpointsProtectionInfo;
    //Helmets
    class MA_Helmet_Base;
    class TFV_Pilot_Helmet_Base: MA_Helmet_Base
    {
        scope=1;
        scopeArsenal=1;
        displayName="[MA] CH252 Helmet";
        model="MA_Armor\data\Helmets\CH252\CH252_Helm.p3d";
        picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa"; //TODO: Update this
        hiddenSelections[]=
        {
            "Camo1", // Helmet
            "Camo2", // NVG Mount
            "Camo3", // Helmet Strap
            "Camo4", // Eye Piece
            "Camo5", // Eye Piece Glass
            "Camo6", // Visor Glasses
            "Helmet",
            "NVGMount",
            "EyePiece",
            "EyePieceGlass",
            "VisorGlasses"
        };
        hiddenSelectionsTextures[]=
        {
            "MA_Armor\data\Helmets\CH252\data\MA_Base_TrooperHelmet_CO.paa",
            "MA_Armor\data\Helmets\CH252\data\MA_Base_TrooperHelmet_CO.paa",
            "MA_Armor\data\Helmets\CH252\data\MA_Base_TrooperHelmet_CO.paa",
            "MA_Armor\data\Helmets\CH252\data\MA_HelmetAccessories_A_CO.paa",
            "MA_Armor\data\Helmets\CH252\data\MA_HelmetAccessories_A_CO.paa",
            "MA_Armor\data\Helmets\CH252\data\MA_HelmetAccessories_A_CO.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel="MA_Armor\data\Helmets\CH252\CH252_Helm.p3d";
            hiddenSelections[]=
            {
                "camo1", // Helmet
                "camo2", // NVG Mount
                "camo3", // Helmet
                "camo4", // Eye Piece
                "camo5", // Eye Piece Glass
                "camo6", // Visor Glasses
                "Helmet",
                "NVGMount",
                "EyePiece",
                "EyePieceGlass",
                "VisorGlasses"
            };
            hiddenSelectionsTextures[]=
            {
                "MA_Armor\data\Helmets\CH252\data\MA_Base_TrooperHelmet_CO.paa",
                "MA_Armor\data\Helmets\CH252\data\MA_Base_TrooperHelmet_CO.paa",
                "MA_Armor\data\Helmets\CH252\data\MA_Base_TrooperHelmet_CO.paa",
                "MA_Armor\data\Helmets\CH252\data\MA_HelmetAccessories_A_CO.paa",
                "MA_Armor\data\Helmets\CH252\data\MA_HelmetAccessories_A_CO.paa",
                "MA_Armor\data\Helmets\CH252\data\MA_HelmetAccessories_A_CO.paa"
            };
        };
    };
    //Cadet Helmet
    class TFV_Pilot_Helmet_Cadet: TFV_Pilot_Helmet_Base
    {
        scope=2;
        scopeArsenal=2;
        displayName="[TFV] Valkyrie Helmet (Cadet)";
        model="MA_Armor\data\Helmets\CH252\CH252_Helm.p3d";
        picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa"; //TODO: Update this
        hiddenSelections[]=
        {
            "camo1", // Helmet
            "camo2", // NVG Mount
            "camo3", // Helmet Strap
            "camo4", // Eye Piece
            "camo5", // Eye Piece Glass
            "camo6", // Visor Glasses
        //  "Helmet",
        //  "NVGMount",
            "EyePiece",
            "EyePieceGlass",
        //  "VisorGlasses"
        };
        hiddenSelectionsTextures[]=
        {
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Helmet_Co.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Helmet_Co.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Helmet_Co.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Glasses_Co.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Glasses_Co.paa",
            "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Glasses_Co.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel="MA_Armor\data\Helmets\CH252\CH252_Helm.p3d";
            hiddenSelections[]=
            {
                "camo1", // Helmet
                "camo2", // NVG Mount
                "camo3", // Helmet Strap
                "camo4", // Eye Piece
                "camo5", // Eye Piece Glass
                "camo6", // Visor Glasses
            //  "Helmet",
            //  "NVGMount",
                "EyePiece",
                "EyePieceGlass",
            //  "VisorGlasses"
            };
            hiddenSelectionsTextures[]=
            {
                "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Helmet_Co.paa",
                "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Helmet_Co.paa",
                "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Helmet_Co.paa",
                "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Glasses_Co.paa",
                "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Glasses_Co.paa",
                "TFV_Units\Bluefor\TFV_UNSC\Navy\data\Valkyrie_Helmet\Valkyrie_Glasses_Co.paa"
            };
        };
    };
};
class CfgVehicles{};
