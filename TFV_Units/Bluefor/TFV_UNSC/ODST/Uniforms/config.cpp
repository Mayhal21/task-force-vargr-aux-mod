class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Uniforms
    {
        units[]={};
        weapons[]=
        {
            //Uniform
            "TFV_ODST_BDU",
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class MA_Marine_BDU_ODST_HJ;
    class ItemInfo;
    class U_B_CombatUniform_mcam;
    class TFV_ODST_BDU: U_B_CombatUniform_mcam
    {
        scope=2;
        scopeArsenal=2;
        author="Task Force Vargr Aux Team";
        displayName="[TFV] ODST BDU";
        model="\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
        picture="\MA_Armor\data\Icons\H3_ODST_Uniform.paa";
        class ItemInfo: UniformItem
        {
            uniformModel="-";
            uniformClass="TFV_ODST_BDU_Base";
            containerClass="Supply200";
            mass=10;
            uniformType="Neopren";
            modelSides[]={6};
        };
    };
    /*class TFV_ODST_New_BDU: MA_Marine_BDU_ODST_HJ
    {
        scope=2;
        scopeArsenal=2;
        author="Hades Aux Team";
        displayName="[TFV] NEW ODST BDU";
        model="\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
        picture="HCO_Aux\Company_Logo\Hades_Logo_co.paa";
        class ItemInfo: UniformItem
        {
            uniformModel="-";
            uniformClass="TFV_ODST_NEW_BDU_Base";
            containerClass="Supply200";
            mass=10;
            uniformType="Neopren";
            modelSides[]={6};
        };
    };*/
};
class CfgVehicles
{
    class B_Soldier_base_F;
    class TFV_ODST_Base: B_Soldier_base_F
    {
        scope=0;
        scopeCurator=0;
        side=1;
        author="Task Force Vargr Aux Team";
        UniformClass="";
        vehicleClass="";
        model="";
        weapons[]={"Throw","Put"};
        respawnWeapons[]= {"Throw","Put"};
        Items[]={"OPTRE_Biofoam"};
        RespawnItems[]={"OPTRE_Biofoam"};
        magazines[]={};
        respawnMagazines[]={};
        hiddenSelections[]=
        {
            "camo"
        };
        hiddenSelectionsTextures[]={};
    };
    class TFV_ODST_BDU_Base: TFV_ODST_Base
    {
        scope=1;
        UniformClass="TFV_ODST_BDU";
        model="MA_Armor\data\Uniforms\H3_ODST\H3_ODST_Uniform.p3d";
        picture="\MA_Armor\data\Icons\H3_ODST_Uniform.paa";
        hiddenSelectionsTextures[]=
        {
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\TFV_Undersuit_co.paa"
        };
    };
    /*class MA_Marine_ODST_BDU_HJ;
    class TFV_ODST_NEW_BDU_Base: MA_Marine_ODST_BDU_HJ
    {
        scope=2;
        scopeArsenal=2;
        UniformClass="TFV_ODST_New_BDU";
        model="\MA_Armor\data\Uniforms\M52_ODST_Uniform\M52_ODST_Uniform.p3d";
        picture="\MA_Armor\data\Icons\H3_ODST_Uniform.paa";
        hiddenSelections[]=
        {
            "Camo1",
            "Camo2",
            "Camo3",
            "Camo4",
            "Camo5",   
            "Camo6",
            "Camo7",
            "Camo8",
            "Camo9",
            "Camo10",
    //     "Uni_Upper",
    //     "Uni_Lower",
    //     "Uni_Collar",
    //     "Armor_Underpad",
    //     "Armor_Straps",
    //     "Armor_Upper",
    //     "Armor_Lower",
            "Shoulders_Lower1",
            "Shoulders_Lower2",
            "Shoulders_Upper"
        };
        hiddenSelectionsTextures[]=
        {
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_New_BDU_Upper_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_New_BDU_Lower_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_Collar_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_Soft_Padding_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_Armor_Straps_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_Upper_Armor_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_Lower_Armor_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\TFV_ODST_Shoulders_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\MA_ODST_TrooperShoulders_CO.paa",
            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\BDU\MA_ODST_TrooperShoulders_CO.paa"
        };        
    };*/
};