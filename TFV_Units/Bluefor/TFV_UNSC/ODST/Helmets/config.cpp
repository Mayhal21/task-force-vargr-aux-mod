class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Helmets
    {
        units[]={};
        weapons[]=
        {
            //Helmets
            "TFV_Helmet_Base",
			//Recruit Helmet
			"TFV_Vargr_Recruit_Helmet",
            //Berserker Helmets
            "TFV_Berserker_Helmet",
            "TFV_Berserker_Corpsman_Helmet",
			//New Helmet
			"TFV_ODST_Helmet_NEW",
            //Custom Helmet's
            "TFV_Helmet_Mayhal",
            "TFV_Helmet_Kyrie",
            "TFV_Helmet_Jolly",
            "TFV_Helmet_Smiley",
            "TFV_Helmet_Breach",
            "TFV_Helmet_Doc",
            "TFV_Helmet_Cold",
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class ItemInfo;
    class VestItem;
    class HeadgearItem;
    class HitpointsProtectionInfo;
    //Helmets
    class MA_Helmet_Base;

class TFV_Helmet_Base: MA_Helmet_Base
{
    scope=1;
    scopeArsenal=1;
    author="Task Force Vargr Aux Team";
    ace_hearing_protection=1;
    displayName="[TFV] Base Helmet";
    model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
    picture="";
    hiddenSelections[]=
    {
        "camo1",
        "camo2"
    };
    hiddenSelectionsTextures[]={};
    class ItemInfo: HeadgearItem
    {
        uniformModel="";
		explosionShielding = 2.2;
        mass=40;        modelSides[]={6};
        allowedSlots[]={801,901,701,605};
        hiddenSelections[]=
        {
            "camo1",
            "camo2"
        };
        hiddenSelectionsTextures[]={};
        class HitpointsProtectionInfo
		{
			class Head
			{
				hitpointName="HitHead";
				armor=15;
				passThrough=0.8;
			};
		};
    };
};
//Recruit Helmet
class TFV_Vargr_Recruit_Helmet: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] ODST Helmet [Recruit]";
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
    {
        "TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Helmet_co.paa",
        "TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Red_Visor_co.paa"
    };
    class ItemInfo: ItemInfo
    {
        uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
    };
};
// Berserker Helmets
class TFV_Berserker_Base_Helmet: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] ODST Helmet [Berserker]";
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Red_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
class TFV_Berserker_Helm_Corpsman: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] ODST Medic Helmet [Berserker]";
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Medic\Berserker_Medic_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Red_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
// Custom Helmet's
class TFV_Helmet_Mayhal: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	author="Task Force Vargr Aux Team";
	ace_hearing_protection=1;
	displayName="[TFV] Mayhal's Helmet";
	model="MA_M56SR_Helmet";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Mayhal_Custom\Mayhal_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Mayhal_Custom\Mayhal_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
class TFV_Helmet_Kyrie: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	author="Task Force Vargr Aux Team";
	ace_hearing_protection=1;
	displayName="[TFV] Kyrie's Helmet";
	model="MA_M56SR_Helmet";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Kyrie_Custom\Kyrie_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Kyrie_Custom\Kyrie_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
class TFV_Helmet_Jolly: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Jolly's Helmet";
	ace_hearing_protection=1;
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Helmets\Jolly_Custom\Jolly_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Helmets\Jolly_Custom\Jolly_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
class TFV_Helmet_Smiley: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Smiley's Helmet";
	ace_hearing_protection=1;
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Smiley_Custom\Smiley_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Smiley_Custom\Smiley_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
class TFV_Helmet_Breach: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Breach's Helmet";
	ace_hearing_protection=1;
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Helmets\Breach_Custom\Breach_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Helmets\Breach_Custom\Breach_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";

	};
};
class TFV_Helmet_Doc: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Doc's Helmet";
	ace_hearing_protection=1;
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Helmets\Doc_Custom\Doc_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Helmets\Doc_Custom\Doc_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
class TFV_Helmet_Cold: TFV_Helmet_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Cold's Helmet";
	ace_hearing_protection=1;
	model="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	picture="\MA_Armor\data\Icons\Halo_Reach_ODST_Helmet.paa";
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Cold_Custom\Colds_Helmet_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Cold_Custom\Cold_Visor_co.paa"
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\HR_ODST\HR_ODST.p3d";
	};
};
//New Helmet
class CH252D_Helmet;
class TFV_ODST_Helmet_NEW: CH252D_Helmet
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV]New ODST Helmet";
	model="MA_Armor\data\Helmets\Human_ODST\Reach_ODST_Helm.p3d";
	picture=""; 
	hiddenSelections[]=
	{
		"Camo1", 
		"Camo2", 
	//  "Helmet",
	//  "Visor"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Helmet\TFV_ODST_Helmet_Co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Helmet\TFV_ODST_Visor_Co.paa"     
	};
	class ItemInfo: ItemInfo
	{
		uniformModel="MA_Armor\data\Helmets\Human_ODST\Reach_ODST_Helm.p3d";
		hiddenSelections[]=
		{
			"Camo1", 
			"Camo2", 
		//  "Helmet",
		//  "Visor"    
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Helmet\TFV_ODST_Helmet_Co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Helmet\TFV_ODST_Visor_Co.paa"    
		};
	};
};
};
class CfgVehicles{};
