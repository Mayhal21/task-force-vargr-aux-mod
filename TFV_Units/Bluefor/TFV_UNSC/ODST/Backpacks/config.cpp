class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Backpacks
    {
        units[]={};
        weapons[]={};
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons{};
class CfgVehicles
{
    // Bags
    class MA_Backpack_Base;
    //custom supply sizes
	class ContainerSupply;
	class Supply300: ContainerSupply		// The class name does not really matter, but for clarity, it should be SupplyXX, where XX is the desired capacity value.
	{
		maximumLoad = 450;				// Replace XX with the desired capacity value.
	};
    //Bags go in here 
class TFV_Backpack_Base: MA_Backpack_Base
{
	scope=1;
	scopeArsenal=0;
	author="Task Force Vargr Aux Team";
	displayname="[TFV] Base Backpack";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Radio_Pack_co.paa"
	};
	//tf_encryptionCode="tf_west_radio_code";
	//tf_dialog="rt1523g_radio_dialog";
	//tf_subtype="digital_lr";
	//tf_range=25000;
	//tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel";
	//tf_hasLRradio=1;
};
// Recruit Bags
class TFV_Recruit_Rucksack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] ODST Backpack [Recruit]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Radio_Pack_co.paa"
	};
};
//Berserker Bags
class TFV_M56S_Berserker_Rucksack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Berserker Backpack [Riflemen]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
};
class TFV_M56S_Berserker_LR_Rucksack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Berserker Backpack [LR]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	tf_encryptionCode="tf_west_radio_code";
	tf_dialog="rt1523g_radio_dialog";
	tf_subtype="digital_lr";
	tf_range=25000;
	tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel";
	tf_hasLRradio=1;
};
class TFV_M56S_Berserker_JTAC_Rucksack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Berserker Backpack [JTAC]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	tf_encryptionCode="tf_west_radio_code";
	tf_dialog="rt1523g_radio_dialog";
	tf_subtype="digital_lr";
	tf_range=25000;
	tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel";
	tf_hasLRradio=1;
};
class TFV_M56S_Berserker_Rucksack_Medic: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Berserker Backpack [Corpsman]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=1000;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Medic\Berserker_Medic_Ruck_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	tf_encryptionCode="tf_west_radio_code";
	tf_dialog="rt1523g_radio_dialog";
	tf_subtype="digital_lr";
	tf_range=25000;
	tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel";
	tf_hasLRradio=1;
};
//Combat Engineer Bags
class TFV_CE_M247_Rucksack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Berserker Backpack [CE/247]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	mti_fortify_canFortify = 1;
	mti_fortify_availablePresets[] = { "M247_Preset", 20,"Sandbag_Barriers_Preset", 5 };
};
class TFV_CE_LAU65D_Rucksack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Berserker Backpack [CE/LAU65D]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	mti_fortify_canFortify = 1;
	mti_fortify_availablePresets[] = { "LAU65D_Preset", 20 };
};
class TFV_CE_AU44_Rucksack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Berserker Backpack [CE/AU44]";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=500;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio"
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	mti_fortify_canFortify = 1;
	mti_fortify_availablePresets[] = { "AU44_Preset", 20 };
};
// Custom Bags
class TFV_Mayhal_Backpack: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Mayhal's Backpack";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=1000;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio",
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Mayhal_Custom\Mayhal_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	tf_encryptionCode="tf_west_radio_code";
	tf_dialog="rt1523g_radio_dialog";
	tf_subtype="digital_lr";
	tf_range=25000;
	tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel";
	tf_hasLRradio=1;
};
class TFV_M56S_Cold: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Cold's Backpack";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=1000;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio",

	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Cold_Custom\Cold_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	tf_encryptionCode="tf_west_radio_code";
	tf_dialog="rt1523g_radio_dialog";
	tf_subtype="digital_lr";
	tf_range=25000;
	tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel";
	tf_hasLRradio=1;

};
class TFV_M56S_Kyrie: TFV_Backpack_Base
{
	scope=2;
	scopeArsenal=2;
	displayName="[TFV] Kyrie's Backpack";
	model="MA_Armor\data\Backpacks\ODST_Rucksack\ODST_Ruck.p3d";
	picture="\MA_Armor\data\Icons\ODST_Rucksack.paa";
	maximumLoad=1000;
	hiddenSelections[]=
	{
		"camo1",
		"camo2",
		"Radio",
	};
	hiddenSelectionsTextures[]=
	{
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Kyrie_Custom\Kyrie_Backpack_co.paa",
		"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Radio_Pack_co.paa"
	};
	tf_encryptionCode="tf_west_radio_code";
	tf_dialog="rt1523g_radio_dialog";
	tf_subtype="digital_lr";
	tf_range=25000;
	tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel";
	tf_hasLRradio=1;
};
};
