class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Vest
    {
        units[]={};
        weapons[]=
        {
            //Vest
            "TFV_Vest_Base",
            //Recruit Vests
            "TFV_Vest_Recruit",
            //Berserker 1-1
            "TFV_Berserker_11_Vest_Base",
            "TFV_Berserker_11_Vest_Corpsman",
            "TFV_Berserker_11_Radio_Vest",
            "TFV_Berserker_11_Autoriflemen_Vest",
            "TFV_Berserker_11_Grenadier_Vest",
            //Berserker 1-2
            "TFV_Berserker_12_Vest_Base",
            "TFV_Berserker_12_Vest_Corpsman",
            "TFV_Berserker_12_Radio_Vest",
            "TFV_Berserker_12_Autoriflemen_Vest",
            "TFV_Berserker_12_Grenadier_Vest",
			//Berserker 1-3
			"TFV_Berserker_13_Vest_Base",
            "TFV_Berserker_13_Vest_Corpsman",
            "TFV_Berserker_13_Radio_Vest",
            "TFV_Berserker_13_Autoriflemen_Vest",
            "TFV_Berserker_13_Grenadier_Vest",
			//New Armor
			"TFV_ODST_Vest_Base",
			"TFV_ODST_Vest_Riflemen",
            //Custom Vest
            "TFV_Vest_Mayhal",
            "TFV_Vest_Smiley",
			"TFV_Vest_Kyrie",
            "TFV_Vest_Cold", 
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class ItemInfo;
    class VestItem;
    class HeadgearItem;
    class HitpointsProtectionInfo;
	class M52_ODST_Vest_Base;
    // Vest
    class MA_Vest_Base;
	class TFV_Vest_Base: MA_Vest_Base
	{
		scope = 1;
		scopeArsenal = 0;
		author = "Task Force Vargr Aux Team";
		displayName = "[TFV] Base Vest";
		model = "MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		hiddenSelections[] = 
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"Reach_Forearm_Left",
			"Reach_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Pauldron_Left",
			"Reach_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Reach_Armor_Upper",
			"Reach_Armor_Lower",
			"Forearm_Vent_Left",
			"Forearm_Vent_Right",
			"Canisters"
		};
		hiddenSelectionsTextures[] = 
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"

		};
		class ItemInfo: VestItem
		{
			uniformModel = "MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			modelSides[] = {6};
			containerClass = "Supply500";
			mass = 20;
			vestType = "Rebreather";
			hiddenSelections[] = 
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"Reach_Forearm_Left",
				"Reach_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Pauldron_Left",
				"Reach_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Reach_Armor_Upper",
				"Reach_Armor_Lower",
				"Forearm_Vent_Left",
				"Forearm_Vent_Right",
				"Canisters"
			};
			hiddenSelectionsTextures[] = 
			{
				"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
				"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Upper_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
			};
			class HitpointsProtectionInfo
	        {
	            class Neck
	            {
	                hitpointName="HitNeck";
	                armor=25;
	                passThrough=0.2;
	            };
	            class Legs
	            {
	                hitpointName="HitLegs";
	                armor=25;
	                passThrough=0.4;
	            };
	            class Arms
	            {
	                hitpointName="HitArms";
	                armor=25;
	                passThrough=0.2;
	            };
	            class Hands
	            {
	                hitpointName="HitHands";
	                armor=25;
	                passThrough=0.4;
	            };
	            class Chest
	            {
	                hitpointName="HitChest";
	                armor=25;
	                passThrough=0.4;
	            };
	            class Diaphragm
	            {
	                hitpointName="HitDiaphragm";
	                armor=25;
	                passThrough=0.4;
	            };
	            class Abdomen
	            {
	                hitpointName="HitAbdomen";
	                armor=25;
	                passThrough=0.4;
	            };
	            class Pelvis
	            {
	                hitpointName="HitPelvis";
	                armor=25;
	                passThrough=0.4;
	            };
	            class Body
	            {
	                hitpointName="HitBody";
					armor = 25;
	                passThrough=0.4;
	            };
	        };
		};
	};
	// Recruit Armor
	class TFV_Vest_Recruit: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] ODST Vest [Recruit]";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Vargr_Recruit\Recruit_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};

	//Berserker 1-1
	class TFV_Berserker_11_Vest_Base: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-1 Vest [Riflemen]";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_11\Base\Berserker_11_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
			hiddenSelectionsTextures[] = 
			{
				"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
				"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_11\Base\Berserker_11_Upper_Armor_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
				"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
			};
		};
	};
	class TFV_Berserker_11_Vest_Corpsman: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-1 Vest [Corpsman]";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Medic\Medic_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_11\Medic\Berserker_11_Medic_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_11_Radio_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-1 Vest (SL\TL)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_11\Base\Berserker_11_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_11_Autoriflemen_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-1 Vest (Autoriflemen)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_11\Base\Berserker_11_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_11_Grenadier_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-1 Vest (Grenadier)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_11\Base\Berserker_11_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower"
			};
		};
	};
	//Berserker 1-2
	class TFV_Berserker_12_Vest_Base: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-2 Vest [Riflemen]";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_12\Base\Berserker_12_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_12_Vest_Corpsman: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-2 Vest [Corpsman]";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Medic\Medic_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_12\Medic\Berserker_12_Medic_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_12_Radio_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-2 Vest (SL\TL)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_12\Base\Berserker_12_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_12_Autoriflemen_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-2 Vest (Autoriflemen)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_12\Base\Berserker_12_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_12_Grenadier_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-2 Vest (Grenadier)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_12\Base\Berserker_12_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower"
			};
		};
	};
	//Berserker 1-3
	class TFV_Berserker_13_Vest_Base: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-3 Vest [Riflemen]";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_13\Base\Berserker_13_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_13_Vest_Corpsman: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-3 Vest [Corpsman]";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Medic\Medic_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_13\Medic\Berserker_13_Medic_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_13_Radio_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-3 Vest (SL\TL)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_13\Base\Berserker_13_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_13_Autoriflemen_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-3 Vest (Autoriflemen)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
			"Canisters"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_13\Base\Berserker_13_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
				"Canisters"
			};
		};
	};
	class TFV_Berserker_13_Grenadier_Vest: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Berserker 1-3 Vest (Grenadier)";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"Thigh_Pouch",
			"H3_Armor_Upper",
			"H3_Armor_Lower"
		};
		hiddenSelectionsTextures[]=
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_13\Base\Berserker_13_Upper_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]=
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"Thigh_Pouch",
				"H3_Armor_Upper",
				"H3_Armor_Lower"
			};
		};
	};
	//Custom Armor's
	class TFV_Vest_Mayhal: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Mayhal's Vest";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[] = 
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Shoulder_Radio_Right",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower",
		};
		hiddenSelectionsTextures[]= 
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Mayhal_Custom\Mayhal_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Mayhal_Custom\Mayhal_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]= 
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Shoulder_Radio_Right",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower",
			};
		};
	};
	class TFV_Vest_Smiley: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Smiley's Vest";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[] = 
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower"
		};
		hiddenSelectionsTextures[]= 
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Smiley_Custom\Smiley_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Smiley_Custom\Smiley_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]= 
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower"
			};
		};
	};
	class TFV_Vest_Cold: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Cold's Vest";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[] = 
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower"
		};
		hiddenSelectionsTextures[]= 
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units/Bluefor\TFV_UNSC\ODST\data\Customs\Full\Cold_Custom\Cold_Medic_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Cold_Custom\Cold_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Cold_Custom\Cold_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]= 
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower"
			};
		};
	};
	class TFV_Vest_Kyrie: TFV_Vest_Base
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Kyrie's Vest";
		model="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
		picture="\MA_Armor\data\Icons\H3ODST_Vest.paa";
		hiddenSelections[] = 
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8",
			"camo9",
			"camo10",
			"H3_Forearm_Left",
			"H3_Forearm_Right",
			"H3_Pauldron_Left",
			"H3_Pauldron_Right",
			"Reach_Shoulder_Radio_Left",
			"Reach_Sniper_Pauldron_Left",
			"Reach_Sniper_Pauldron_Right",
			"Reach_CQB_Pauldron_Left",
			"Reach_CQB_Pauldron_Right",
			"H3_Armor_Upper",
			"H3_Armor_Lower"
		};
		hiddenSelectionsTextures[]= 
		{
			"MA_Armor\data\Vests\H3_ODST\H3_Upper_Armor_co.paa",
			"MA_Armor\data\Vests\H3_ODST\H3_Lower_Armor_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Leg_Pouch_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Kyrie_Custom\Kyrie_Armor_Upper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Kyrie_Custom\Kyrie_Armor_Lower_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Customs\Full\Kyrie_Custom\Kyrie_canisters_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_forearms_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Sniper_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_CQB_co.paa",
			"TFV_Units\Bluefor\TFV_UNSC\ODST\data\Berserker\Berserker_Base\Berserker_Shoulder_Radio_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			vestType="Rebreather";
			uniformModel="MA_Armor\data\Vests\H3_ODST\H3_ODST_Armor.p3d";
			containerClass="Supply500";
			hiddenSelections[]= 
			{
				"camo1",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"camo6",
				"camo7",
				"camo8",
				"camo9",
				"camo10",
				"H3_Forearm_Left",
				"H3_Forearm_Right",
				"H3_Pauldron_Left",
				"H3_Pauldron_Right",
				"Reach_Shoulder_Radio_Left",
				"Reach_Sniper_Pauldron_Left",
				"Reach_Sniper_Pauldron_Right",
				"Reach_CQB_Pauldron_Left",
				"Reach_CQB_Pauldron_Right",
				"H3_Armor_Upper",
				"H3_Armor_Lower"
			};
		};
	};
	/*class TFV_ODST_Vest_Base: M52_ODST_Vest_Base
	{
	    scope=1;
	    scopeArsenal=1;
	    author="Task Force Vargr Aux Team";
	    displayName="[TFV] New ODST Armor (Base)";
	    model="MA_Armor\data\Vests\M52_ODST\M52_ODST_Vest.p3d";
	    picture="";
	    hiddenSelections[]=
	    {
	        "Camo1", //CQB Left
	        "Camo2", //CQB Right
	        "Camo3", //Marksman_Left
	        "Camo4", //Marksman_Right
	        "Camo5", //ODST_Bracer_Left
	        "Camo6", //ODST_Bracer_Right
	        "Camo7", //ODST_Chest
	        "Camo8", //ODST_Left
	        "Camo9", //ODST_Right
	        "Camo10", //ChestPMLeft
	        "Camo11", //ChestPMRight
	        "Camo12", //ChestPouch
	        "Camo13", //LShoulderRadio
	        "Camo14", //RShoulderRadio
	        "Camo15", //StomachPouch
	        "Camo16", //TorsoPMLeft
	        "Camo17", //TorsoPMRight
	        "Camo18", //TorsoPouch
	        "Camo19", //WaistBack
	        "Camo20", //WaistGLeft
	        "Camo21", //WaistGRight
	        "Camo22", //WaistPLeft
	        "Camo23", //WaistPRight
	        "Camo24", //WaistRLeft
	        "Camo25", //WaistRRight
	        "Camo26", //WaistSLeft
	        "Camo27", //WaistSRight
	        "Camo28", //LegPouchL
	        "Camo29", //LegPouchR
	        "CQB_Left",
	        "CQB_Right",
	        "Marksman_Left",
	        "Marksman_Right",
	        "ODST_Bracer_Left",
	        "ODST_Bracer_Right",
	        "ODST_Chest",
	        "ODST_Left",
	        "ODST_Right",
	        "ChestPMLeft",
	        "ChestPMRight",
	        "ChestPouch",
	        "LShoulderRadio",
	        "RShoulderRadio",
	        "StomachPouch",
	        "TorsoPMLeft",
	        "TorsoPMRight",
	        "TorsoPouch",
	        "WaistBack",
	        "WaistGLeft",
	        "WaistGRight",
	        "WaistPLeft",
	        "WaistPRight",
	        "WaistRLeft",
	        "WaistRRight",
	        "WaistSLeft",
	        "WaistSRight",
	        "LegPouchL",
	        "LegPouchR"
	    };
	    hiddenSelectionsTextures[]=
	    {
	        "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_CQB_co.paa", //CQB Left
	        "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_CQB_co.paa", //CQB Right
	        "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_Marksman_co.paa", //Marksman_Left
	        "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_Marksman_co.paa", //Marksman_Right
	        "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Vest_Co.paa", //ODST_Bracer_Left
	        "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Vest_Co.paa", //ODST_Bracer_Right
	        "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Vest_Co.paa", //ODST_Chest
	        "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Shoulders_Co.paa", //ODST_Left
	        "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Shoulders_Co.paa", //ODST_Right
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //ChestPMLeft
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //ChestPMRight
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //ChestPouch
	        "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //LShoulderRadio
	        "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //RShoulderRadio
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //StomachPouch
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //TorsoPMLeft
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //TorsoPMRight
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //TorsoPouch
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //WaistBack
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistGLeft
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistGRight
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistPLeft
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistPRight
	        "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //WaistRLeft
	        "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //WaistRRight
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //WaistSLeft
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //WaistSRight
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //LegPouchL
	        "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa"  //LegPouchR
	    };
	    class ItemInfo: VestItem
	    {
	        vestType="Rebreather";
	        hiddenSelections[]=
	        {
	            "Camo1", //CQB Left
	            "Camo2", //CQB Right
	            "Camo3", //Marksman_Left
	            "Camo4", //Marksman_Right
	            "Camo5", //ODST_Bracer_Left
	            "Camo6", //ODST_Bracer_Right
	            "Camo7", //ODST_Chest
	            "Camo8", //ODST_Left
	            "Camo9", //ODST_Right
	            "Camo10", //ChestPMLeft
	            "Camo11", //ChestPMRight
	            "Camo12", //ChestPouch
	            "Camo13", //LShoulderRadio
	            "Camo14", //RShoulderRadio
	            "Camo15", //StomachPouch
	            "Camo16", //TorsoPMLeft
	            "Camo17", //TorsoPMRight
	            "Camo18", //TorsoPouch
	            "Camo19", //WaistBack
	            "Camo20", //WaistGLeft
	            "Camo21", //WaistGRight
	            "Camo22", //WaistPLeft
	            "Camo23", //WaistPRight
	            "Camo24", //WaistRLeft
	            "Camo25", //WaistRRight
	            "Camo26", //WaistSLeft
	            "Camo27", //WaistRRight
	            "Camo28", //LegPouchL
	            "Camo29", //LegPouchR
	            "CQB_Left",
	            "CQB_Right",
	            "Marksman_Left",
	            "Marksman_Right",
	            "ODST_Bracer_Left",
	            "ODST_Bracer_Right",
	            "ODST_Chest",
	            "ODST_Left",
	            "ODST_Right",
	            "ChestPMLeft",
	            "ChestPMRight",
	            "ChestPouch",
	            "LShoulderRadio",
	            "RShoulderRadio",
	            "StomachPouch",
	            "TorsoPMLeft",
	            "TorsoPMRight",
	            "TorsoPouch",
	            "WaistBack",
	            "WaistGLeft",
	            "WaistGRight",
	            "WaistPLeft",
	            "WaistPRight",
	            "WaistRLeft",
	            "WaistRRight",
	            "WaistSLeft",
	            "WaistSRight",
	            "LegPouchL",
	            "LegPouchR"
	        };
	       hiddenSelectionsTextures[]=
	        {
	            "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_CQB_co.paa", //CQB Left
	            "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_CQB_co.paa", //CQB Right
	            "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_Marksman_co.paa", //Marksman_Left
	            "MA_Armor\data\Vests\M52_ODST\Color_Variants\MA_ODST_Marksman_co.paa", //Marksman_Right
	            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Vest_Co.paa", //ODST_Bracer_Left
	            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Vest_Co.paa", //ODST_Bracer_Right
	            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Vest_Co.paa", //ODST_Chest
	            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Shoulders_Co.paa", //ODST_Left
	            "TFV_Units\Bluefor\TFV_UNSC\ODST\data\New_Armor\Vest\TFV_ODST_Shoulders_Co.paa", //ODST_Right
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //ChestPMLeft
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //ChestPMRight
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //ChestPouch
	            "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //LShoulderRadio
	            "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //RShoulderRadio
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //StomachPouch
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //TorsoPMLeft
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //TorsoPMRight
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //TorsoPouch
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //WaistBack
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistGLeft
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistGRight
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistPLeft
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_A_CO.paa", //WaistPRight
	            "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //WaistRLeft
	            "MA_Armor\data\Backpacks\ODST_Rucksack\Attachments\Backpack_Radio_ODST_co.paa", //WaistRRight
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //WaistSLeft
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //WaistSRight
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa", //LegPouchL
	            "MA_Armor\data\vests\Marine_Pouches\data\MA_TrooperExtras_B_CO.paa"  //LegPouchR
	        };
	        uniformModel="MA_Armor\data\Vests\M52_ODST\M52_ODST_Vest.p3d";
	        containerClass="Supply500";
	        mass=20;
	        passThrough=0.1;
	        modelSides[]={6};
	        class HitpointsProtectionInfo
	        {
	            class Neck
	            {
	                hitpointName="HitNeck";
	                armor=20;
	                passThrough=0.5;
	            };
	            class Legs
	            {
	                hitpointName="HitLegs";
	                armor=20;
	                passThrough=0.5;
	            };
	            class Arms
	            {
	                hitpointName="HitArms";
	                armor=25;
	                passThrough=0.1;
	            };
	            class Hands
	            {
	                hitpointName="HitHands";
	                armor=20;
	                passThrough=0.1;
	            };
	            class Chest
	            {
	                hitpointName="HitChest";
	                armor=35;
	                passThrough=0.1;
	            };
	            class Diaphragm
	            {
	                hitpointName="HitDiaphragm";
	                armor=30;
	                passThrough=0.1;
	            };
	            class Abdomen
	            {
	                hitpointName="HitAbdomen";
	                armor=30;
	                passThrough=0.1;
	            };
	            class Pelvis
	            {
	                hitpointName="HitPelvis";
	                armor=30;
	                passThrough=0.1;
	            };
	            class Body
	            {
	                hitpointName="HitBody";
	                passThrough=0.1;
	            };
	        };
	    };
	};

	class TFV_ODST_Vest_Riflemen: TFV_ODST_Vest_Base
	{
	    scope=2;
	    scopeArsenal=2;
	    author="Task Force Vargr Aux Team";
	    displayName="[TFV] New ODST Armor (Riflemen)";
	    hiddenSelections[]=
	    {
	        "Camo1", //CQB Left
	        "Camo2", //CQB Right
	        "Camo3", //Marksman_Left
	        "Camo4", //Marksman_Right
	        "Camo5", //ODST_Bracer_Left
	        "Camo6", //ODST_Bracer_Right
	        "Camo7", //ODST_Chest
	        "Camo8", //ODST_Left
	        "Camo9", //ODST_Right
	        "Camo10", //ChestPMLeft
	        "Camo11", //ChestPMRight
	        "Camo12", //ChestPouch
	        "Camo13", //LShoulderRadio
	        "Camo14", //RShoulderRadio
	        "Camo15", //StomachPouch
	        "Camo16", //TorsoPMLeft
	        "Camo17", //TorsoPMRight
	        "Camo18", //TorsoPouch
	        "Camo19", //WaistBack
	        "Camo20", //WaistGLeft
	        "Camo21", //WaistGRight
	        "Camo22", //WaistPLeft
	        "Camo23", //WaistPRight
	        "Camo24", //WaistRLeft
	        "Camo25", //WaistRRight
	        "Camo26", //WaistSLeft
	        "Camo27", //WaistSRight
	        "Camo28", //LegPouchL
	        "Camo29", //LegPouchR
	        "CQB_Left",
	        "CQB_Right",
	        "Marksman_Left",
	        "Marksman_Right",
	        //"ODST_Bracer_Left",
	        //"ODST_Bracer_Right",
	        //"ODST_Chest",
	        //"ODST_Left",
	        //"ODST_Right",
	        "ChestPMLeft",
	        "ChestPMRight",
	        "ChestPouch",
	        "LShoulderRadio",
	        "RShoulderRadio",
	        "StomachPouch",
	        "TorsoPMLeft",
	        "TorsoPMRight",
	        "TorsoPouch",
	        "WaistBack",
	        "WaistGLeft",
	        "WaistGRight",
	        "WaistPLeft",
	        "WaistPRight",
	        "WaistRLeft",
	        "WaistRRight",
	        "WaistSLeft",
	        "WaistSRight",
	        "LegPouchL",
	        "LegPouchR"
	    };
	    class ItemInfo: ItemInfo
	    {
	        hiddenSelections[]=
	        {
	            "Camo1", //CQB Left
	            "Camo2", //CQB Right
	            "Camo3", //Marksman_Left
	            "Camo4", //Marksman_Right
	            "Camo5", //ODST_Bracer_Left
	            "Camo6", //ODST_Bracer_Right
	            "Camo7", //ODST_Chest
	            "Camo8", //ODST_Left
	            "Camo9", //ODST_Right
	            "Camo10", //ChestPMLeft
	            "Camo11", //ChestPMRight
	            "Camo12", //ChestPouch
	            "Camo13", //LShoulderRadio
	            "Camo14", //RShoulderRadio
	            "Camo15", //StomachPouch
	            "Camo16", //TorsoPMLeft
	            "Camo17", //TorsoPMRight
	            "Camo18", //TorsoPouch
	            "Camo19", //WaistBack
	            "Camo20", //WaistGLeft
	            "Camo21", //WaistGRight
	            "Camo22", //WaistPLeft
	            "Camo23", //WaistPRight
	            "Camo24", //WaistRLeft
	            "Camo25", //WaistRRight
	            "Camo26", //WaistSLeft
	            "Camo27", //WaistSRight
	            "Camo28", //LegPouchL
	            "Camo29", //LegPouchR
	            "CQB_Left",
	            "CQB_Right",
	            "Marksman_Left",
	            "Marksman_Right",
	            //"ODST_Bracer_Left",
	            //"ODST_Bracer_Right",
	            //"ODST_Chest",
	            //"ODST_Left",
	            //"ODST_Right",
	            "ChestPMLeft",
	            "ChestPMRight",
	            "ChestPouch",
	            "LShoulderRadio",
	            "RShoulderRadio",
	            "StomachPouch",
	            "TorsoPMLeft",
	            "TorsoPMRight",
	            "TorsoPouch",
	            "WaistBack",
	            "WaistGLeft",
	            "WaistGRight",
	            "WaistPLeft",
	            "WaistPRight",
	            "WaistRLeft",
	            "WaistRRight",
	            "WaistSLeft",
	            "WaistSRight",
	            "LegPouchL",
	            "LegPouchR"
	        };
	    };
	};*/
};
class CfgVehicles{};