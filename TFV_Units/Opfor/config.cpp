class CfgPatches
{
	class TFV_Units_Opfor
	{
		author="TFV";
		name="TFV_OPFOR";
		units[]=
		{
			//NCA
			"TFV_NCA_IZ",
			"TFV_NCA_Officer",
			"TFV_NCA_SL",
			"TFV_NCA_TL",
			"TFV_NCA_Riflemen",
			"TFV_NCA_Grenadier",
			"TFV_NCA_Medic",
			"TFV_NCA_Marksman",
			"TFV_NCA_RTO",
			"TFV_NCA_Sniper",
			"TFV_NCA_AR",
			"TFV_NCA_Crewman",
			"TFV_NCA_Rifleman_AT",
			"TFV_NCA_Rifleman_AA",
			"TFV_NCA_Breacher",
			"TFV_NCA_Engineer",
			"TFV_NCA_LAU",
			"TFV_NCA_PelicanAV",
			"TFV_NCA_Pelican",
			//SU
			"TFV_SU_Ander",
			"TFV_SU_Officer",
			"TFV_SU_SL",
			"TFV_SU_TL",
			"TFV_SU_Rifleman",
			"TFV_SU_GL",
			"TFV_SU_Medic",
			"TFV_SU_Marksman",
			"TFV_SU_RTO",
			"TFV_SU_Sniper",
			"TFV_SU_AutoRifleman",
			"TFV_SU_Crewman",
			"TFV_SU_Rifleman_AT",
			"TFV_SU_Rifleman_AA",
			"TFV_SU_Breacher",
			"TFV_SU_Engineer",
			"TFV_FRI_LAU",
			"TFV_M92_BOLT_FRI",
			"TFV_M48_Buzz_FRI",
			"TFV_SAM_Regular",
			"TFV_RAD_Regular",
			"TFV_Felix_Rotary_Frieden",
			"TFV_Witherwing_Frieden",
			"TFV_Ansaldo_Frieden",
			"TFV_M494_IFV",
			"TFV_M808B_FRI",
			"TFV_M808BM_FRI",
			"TFV_Raptor_Frieden_IFV",
			"TFV_Raptor_Frieden_APC",
			"TFV_D56I",
			"TFV_D56V",
			"TFV_D81LRTA",
			"TFV_D81LRTI",
			"TFV_M247H_FRI",
		};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"OPTRE_Vehicles",
			"OPTRE_Vehicles_air",
			"OPTRE_Weapons",
			"A3_Modules_F",
    		"A3_Functions_F",
			"TCF_Main",
		};
	};
};
class CfgFactionClasses
{
	class TFV_FactionClasses_URF
    {
        displayName = "[Vargr] United Rebel Front";
        priority=1;
		side=0;
		primaryLanguage="EN";
		backpack_tf_faction_radio_api="OPTRE_Como_pack_2";
    };
};
class CfgGroups
{
	class East
	{
		class TFV_OPFOR_Spacer
		{
			name= "[Vargr] United Rebel Front"
			class TFV_SU
			{
				dlc="TFV";
				name="Seccessionist Union";
				class TFV_SU_Sentry
				{
					name="Seccessionist Sentry";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="TFV";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_Medic";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
				};
				class TFV_Seccessionist_RifleSquad
				{
					name="Seccessionist Rifle Squad";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_SL";
						rank="SERGEANT";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_TL";
						rank="CORPORAL";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_SU_GL";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
					class Unit3
					{
						side=0;
						vehicle="TFV_SU_Medic";
						rank="PRIVATE";
						position[]={-6,0,0};
					};
					class Unit4
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="PRIVATE";
						position[]={-8,0,0};
					};
					class Unit5
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="PRIVATE";
						position[]={-10,0,0};
					};
					class Unit6
					{
						side=0;
						vehicle="TFV_SU_Marksman";
						rank="PRIVATE";
						position[]={-12,0,0};
					};
					class Unit7
					{
						side=0;
						vehicle="TFV_SU_AutoRifleman";
						rank="PRIVATE";
						position[]={-14,0,0};
					};
					class Unit8
					{
						side=0;
						vehicle="TFV_SU_Engineer";
						rank="CORPORAL";
						position[]={-16,0,0};
					};
					class Unit9
					{
						side=0;
						vehicle="TFV_SU_Medic";
						rank="PRIVATE";
						position[]={-18,0,0};
					};
				};
				class TFV_Seccessionist_Fireteam
				{
					name="Seccessionist Fireteam";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_SU_Breacher";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
					class Unit3
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="PRIVATE";
						position[]={-6,0,0};
					};
				};
				class TFV_Seccessionist_MachineGunTeam
				{
					name="Seccessionist Machine Gun Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_AutoRifleman";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
				};
				class TFV_Seccessionist_CmdSquad
				{
					name="Seccessionist Command Squad";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_Ander";
						rank="SERGEANT";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_Officer";
						rank="CORPORAL";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_SU_RTO";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
					class Unit3
					{
						side=0;
						vehicle="TFV_SU_Rifleman_AT";
						rank="PRIVATE";
						position[]={-6,0,0};
					};
					class Unit4
					{
						side=0;
						vehicle="TFV_SU_Medic";
						rank="PRIVATE";
						position[]={-8,0,0};
					};
					class Unit5
					{
						side=0;
						vehicle="TFV_SU_AutoRifleman";
						rank="PRIVATE";
						position[]={-10,0,0};
					};
				};
				class TFV_Seccessionist_AntiTank
				{
					name="Seccessionist Anti-Tank Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_Rifleman_AT";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
				};
				class TFV_Seccessionist_AntiAir
				{
					name="Seccessionist Anti-Air Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_Rifleman";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_Rifleman_AA";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
				};
				class TFV_Seccessionist_Sniper
				{
					name="Seccessionist Sniper Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_SU_Sniper";
						rank="SERGEANT";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_SU_Marksman";
						rank="CORPORAL";
						position[]={-2,0,0};
					};
				};
			};
			class TFV_NCA
			{
				dlc="TFV";
				name="New Colonial Alliance";
				class TFV_NCA_Sentry
				{
					name="Colonial Sentry";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="TFV";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_TL";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_Medic";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
				};
				class TFV_NCA_RifleSquad
				{
					name="Colonial Rifle Squad";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_SL";
						rank="SERGEANT";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_TL";
						rank="CORPORAL";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_NCA_Grenadier";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
					class Unit3
					{
						side=0;
						vehicle="TFV_NCA_Medic";
						rank="PRIVATE";
						position[]={-6,0,0};
					};
					class Unit4
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="PRIVATE";
						position[]={-8,0,0};
					};
					class Unit5
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="PRIVATE";
						position[]={-10,0,0};
					};
					class Unit6
					{
						side=0;
						vehicle="TFV_NCA_Marksmen";
						rank="PRIVATE";
						position[]={-12,0,0};
					};
					class Unit7
					{
						side=0;
						vehicle="TFV_NCA_AR";
						rank="PRIVATE";
						position[]={-14,0,0};
					};
					class Unit8
					{
						side=0;
						vehicle="TFV_NCA_Engineer";
						rank="CORPORAL";
						position[]={-16,0,0};
					};
					class Unit9
					{
						side=0;
						vehicle="TFV_NCA_Medic";
						rank="PRIVATE";
						position[]={-18,0,0};
					};
				};
				class TFV_NCA_Fireteam
				{
					name="Colonial Fireteam";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_NCA_Breacher";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
					class Unit3
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="PRIVATE";
						position[]={-6,0,0};
					};
				};
				class TFV_NCA_MachineGunTeam
				{
					name="Colonial Machine Gun Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_AR";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
				};
				class TFV_NCA_CmdSquad
				{
					name="Colonial Command Squad";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_IZ";
						rank="SERGEANT";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_Officer";
						rank="CORPORAL";
						position[]={-2,0,0};
					};
					class Unit2
					{
						side=0;
						vehicle="TFV_NCA_RTO";
						rank="PRIVATE";
						position[]={-4,0,0};
					};
					class Unit3
					{
						side=0;
						vehicle="TFV_NCA_Rifleman_AT";
						rank="PRIVATE";
						position[]={-6,0,0};
					};
					class Unit4
					{
						side=0;
						vehicle="TFV_NCA_Medic";
						rank="PRIVATE";
						position[]={-8,0,0};
					};
					class Unit5
					{
						side=0;
						vehicle="TFV_NCA_AR";
						rank="PRIVATE";
						position[]={-10,0,0};
					};
				};
				class TFV_NCA_AntiTank
				{
					name="Colonial Anti-Tank Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_Riflemen_AT";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
				};
				class TFV_NCA_AntiAir
				{
					name="Colonial Anti-Air Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_Riflemen";
						rank="CORPORAL";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_Riflemen_AA";
						rank="PRIVATE";
						position[]={-2,0,0};
					};
				};
				class TFV_NCA_Sniper
				{
					name="Colonial Sniper Team";
					side=0;
					faction="TFV_FactionClasses_URF";
					rarityGroup=0.30000001;
					dlc="OPTRE";
					class Unit0
					{
						side=0;
						vehicle="TFV_NCA_Sniper";
						rank="SERGEANT";
						position[]={0,0,0};
					};
					class Unit1
					{
						side=0;
						vehicle="TFV_NCA_Marksman";
						rank="CORPORAL";
						position[]={-2,0,0};
					};
				};
			};
		};
	};
};
