class CfgPatches
{
	class TFV_Units_New_Colonial_Alliance
	{
		author="Task Force Vargr Aux Team";
		name="TaskForceVargr";
		addonRootClass="TCF_Units";
		units[]={};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"TCF_OPFOR"
		};
	};
};
class CfgEditorCategories
{
	class TFV_Editor_Category_NCA
	{
		displayName="[Vargr] New Colonial Allaince";
		scopeCurator=2;
		scopeeditor=2;
		scope=2;
	};
};
class CfgEditorSubcategories
{
	class TFV_Editor_Subcategory_AC_NCA
    {
        displayName = "Aircraft";
    };
    class TFV_Editor_Subcategory_AV_NCA
    {
        displayName = "Armored Vehicles";
    };
    class TFV_Editor_Subcategory_Inf_NCA
    {
        displayName = "Colonials";
    };
    class TFV_Editor_Subcategory_Vehcles_NCA
    {
        displayName = "Vehicles";
    };
    class TFV_Editor_Subcategory_Turrets_NCA
    {
        displayName = "Turrets";
    };
};
