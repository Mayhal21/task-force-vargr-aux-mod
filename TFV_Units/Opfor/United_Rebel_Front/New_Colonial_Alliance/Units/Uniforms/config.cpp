class CfgPatches
{
    class TFV_Patch_Units_Opfor_United_Rebel_Front_New_Colonial_Alliance_Uniforms
    {
        units[]={};
        weapons[]=
        {
            //Uniform
            "TFV_NCA_UNI_Base"
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class uniform_base;
    class TFV_NCA_UNI_Base: uniform_base
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[TFV] NCA BDU";
		picture="\A3\characters_f_beta\data\ui\icon_U_IR_CrewUniform_rucamo_ca.paa";
		model="\A3\Characters_F\Common\Suitpacks\suitpack_original_F.p3d";
		class ItemInfo: UniformItem
		{
			model="\A3\Characters_F\Common\Suitpacks\suitpack_original_F.p3d";
			uniformClass="TFV_NCA_BDU";
			containerClass="Supply40";
			mass=40;
		};
	};
};
class CfgVehicles
{
    class O_Soldier_F;
	class TFV_NCA_Base: O_Soldier_F
	{
        scope=0;
        scopeCurator=0;
        side=0;
        author="Task Force Vargr Aux Team";
        UniformClass="";
        vehicleClass="";
        model="";
        weapons[]={"Throw","Put"};
        respawnWeapons[]= {"Throw","Put"};
        Items[]={"OPTRE_Biofoam"};
        RespawnItems[]={"OPTRE_Biofoam"};
        magazines[]={};
        respawnMagazines[]={};
        hiddenSelections[]=
        {
            "camo"
        };
        hiddenSelectionsTextures[]={};
	};
    class TFV_NCA_BDU: TFV_NCA_Base
	{
		author="Task Force Vargr Aux Team";
		displayName="Base";
		scope=1;
		model="\OPTRE_UNSC_Units\Army\uniform.p3d";
		hiddenSelections[]=
		{
			"camo",
			"camo2",
			"insignia",
			"clan",
			"A_SlimLeg"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\New_Colonial_Alliance\Units\data\TFV_NCA_Uniform.paa",
			"TFV_Units\Opfor\United_Rebel_Front\New_Colonial_Alliance\Units\data\TFV_NCA_Uniform.paa"
		};
	};
};