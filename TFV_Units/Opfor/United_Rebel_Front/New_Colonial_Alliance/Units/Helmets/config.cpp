class CfgPatches
{
    class TFV_Patch_Units_Opfor_New_Colonial_Alliance_Helmets
    {
        units[]={};
        weapons[]=
        {
            //Helmets
            "TFV_Beogao_Helm",
            "TFV_Soft_Helm"
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={"TCF_EQUIPMENT"};
    };
};
class CfgWeapons
{
    class H_HelmetB;
    class H_HelmetO_ocamo: H_HelmetB
    {
    	class ItemInfo;
    };
    class TFV_Beogao_Helm: H_HelmetO_ocamo
    {
    	displayName="Gao Assault Helm (BeoGao)";
    	picture="\A3\Characters_F\data\ui\icon_H_HelmetO_oucamo_ca.paa";
    	hiddenSelectionsTextures[]=
    	{
    		"TCF_OPFOR\GMF\data\tech_helm_beogao_CO.paa"
    	};
    	allowedFacewear[]=
    	{
    		"G_Lowprofile",
    		1
    	};
    	class ItemInfo: ItemInfo
    	{
    		uniformModel="\A3\Characters_F\OPFOR\headgear_o_helmet_ballistic";
    	};
    };
    class TFV_Soft_Helm: H_HelmetO_ocamo
    {
    	displayName="Gao Assault Helm (Soft Grey)";
    	picture="\A3\Characters_F\data\ui\icon_H_HelmetO_oucamo_ca.paa";
    	hiddenSelectionsTextures[]=
    	{
    		"TCF_OPFOR\GMF\data\tech_helm_soft_CO.paa"
    	};
    	allowedFacewear[]=
    	{
    		"G_Lowprofile",
    		1
    	};
    	class ItemInfo: ItemInfo
    	{
    		uniformModel="\A3\Characters_F\OPFOR\headgear_o_helmet_ballistic";
    	};
    };
};  