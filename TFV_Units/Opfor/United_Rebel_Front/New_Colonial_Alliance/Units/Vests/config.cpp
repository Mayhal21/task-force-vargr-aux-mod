class CfgPatches
{
    class TFV_Patch_Units_Opfor_United_Rebel_Front_New_Colonial_Alliance_Vest
    {
        units[]={};
        weapons[]=
        {
            //Vest
            "TFV_NCA_Vest_Base",
            "TFV_NCA_Vest"
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]=
        {"OPTRE_UNSC_Units"};
    };
};
class CfgWeapons
{
    class OPTRE_UNSC_M52A_Armor1_WDL;
    class ItemInfo;
    class VestItem;
    class Neck;
	class Legs;
	class Arms;
	class Hands;
	class Chest;
	class Diaphragm;
	class Abdomen;
	class Pelvis;
	class Body;
    class HitpointsProtectionInfo;
    class OPTRE_UNSC_M52A_Armor3_WDL: OPTRE_UNSC_M52A_Armor1_WDL
	{
		dlc="OPTRE";
        scope=0;
		author="Article 2 Studios";
		displayName="M52A Body Armor (Light) [Woodland]";
        model="\OPTRE_UNSC_Units\Army\armor.p3d";
		hiddenSelections[]=
		{
			"camo",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"A_Ghillie",
			"A_KneesMarLeft",
			"A_KneesMarRight",
			"A_ODST",
			"A_ShinArmorLeft",
			"A_ShinArmorRight",
			"A_TacPad",
			"A_ThighArmorLeft",
			"A_ThighArmorRight",
			"AS_LargeLeft",
			"AS_LargeRight",
			"AS_MediumLeft",
			"AS_MediumRight",
			"AS_SmallLeft",
			"AS_SmallRight",
			"AS_ODSTCQBLeft",
			"AS_ODSTCQBRight",
			"AS_ODSTLeft",
			"AS_ODSTRight",
			"AS_ODSTSniperLeft",
			"AS_ODSTSniperRight",
			"AP_AR",
			"AP_BR",
			"AP_Canteen",
			"AP_GL",
			"AP_Knife",
			"AP_MGThigh",
			"AP_AR",
			"AP_Pack",
			"AP_Pistol",
			"AP_Rounds",
			"AP_SG",
			"AP_SMG",
			"AP_Sniper",
			"AP_Thigh",
			"AP_Frag",
			"AP_Smoke",
			"APO_AR",
			"APO_BR",
			"APO_Knife",
			"APO_SMG",
			"APO_Sniper",
			"CustomKit_Scorch"
		};
		hiddenSelectionsTextures[]=
		{
			"OPTRE_UNSC_Units\Army\data\vest_woodland_co.paa",
			"optre_unsc_units\army\data\armor_woodland_co.paa",
			"optre_unsc_units\army\data\legs_woodland_co.paa",
			"optre_unsc_units\army\data\ghillie_woodland_co.paa",
			"optre_unsc_units\army\data\odst_armor_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			containerClass="Supply100";
			mass=70;
			hiddenSelections[]=
			{
				"camo",
				"camo2",
				"camo3",
				"camo4",
				"camo5",
				"A_Ghillie",
				"A_KneesMarLeft",
				"A_KneesMarRight",
				"A_ODST",
				"A_ShinArmorLeft",
				"A_ShinArmorRight",
				"A_TacPad",
				"A_ThighArmorLeft",
				"A_ThighArmorRight",
				"AS_LargeLeft",
				"AS_LargeRight",
				"AS_MediumLeft",
				"AS_MediumRight",
				"AS_SmallLeft",
				"AS_SmallRight",
				"AS_ODSTCQBLeft",
				"AS_ODSTCQBRight",
				"AS_ODSTLeft",
				"AS_ODSTRight",
				"AS_ODSTSniperLeft",
				"AS_ODSTSniperRight",
				"AP_AR",
				"AP_BR",
				"AP_Canteen",
				"AP_GL",
				"AP_Knife",
				"AP_MGThigh",
				"AP_AR",
				"AP_Pack",
				"AP_Pistol",
				"AP_Rounds",
				"AP_SG",
				"AP_SMG",
				"AP_Sniper",
				"AP_Thigh",
				"AP_Frag",
				"AP_Smoke",
				"APO_AR",
				"APO_BR",
				"APO_Knife",
				"APO_SMG",
				"APO_Sniper",
				"CustomKit_Scorch"
			};
			hiddenSelectionsTextures[]=
			{
				"OPTRE_UNSC_Units\Army\data\vest_woodland_co.paa",
				"optre_unsc_units\army\data\armor_woodland_co.paa",
				"optre_unsc_units\army\data\legs_woodland_co.paa",
				"optre_unsc_units\army\data\ghillie_woodland_co.paa",
				"optre_unsc_units\army\data\odst_armor_co.paa"
			};
			class HitpointsProtectionInfo: HitpointsProtectionInfo
			{
				class Neck: Neck
				{
					armor=15;
				};
				class Legs: Legs
				{
					armor=15;
				};
				class Arms: Arms
				{
					armor=15;
				};
				class Hands: Hands
				{
					armor=15;
				};
				class Chest: Chest
				{
					armor=15;
				};
				class Diaphragm: Diaphragm
				{
					armor=15;
				};
				class Abdomen: Abdomen
				{
					armor=15;
				};
				class Pelvis: Pelvis
				{
					armor=15;
				};
				class Body: Body
				{
					armor=15;
				};
			};
		};
	};
    class TFV_NCA_Vest:OPTRE_UNSC_M52A_Armor3_WDL
	{
		dlc="OPTRE";
        scope=2;
		author="Task Force Vargr Aux Team";
        picture="\A3\Characters_F_Enoch\Vests\Data\UI\icon_V_CArrierRigKBT_01_light_Olive_F_CA.paa";
		displayName="[TFV] NCA Vest";
        model="\OPTRE_UNSC_Units\Army\armor.p3d";
		hiddenSelectionsTextures[]=
		{
			"OPTRE_UNSC_Units\Army\data\vest_odst_co.paa",
			"optre_unsc_units\army\data\armor_odst_co.paa",
			"optre_unsc_units\army\data\legs_odst_co.paa",
			"optre_unsc_units\army\data\ghillie_woodland_co.paa",
			"optre_unsc_units\army\data\odst_armor_co.paa"
		};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[]=
			{
				"OPTRE_UNSC_Units\Army\data\vest_odst_co.paa",
				"optre_unsc_units\army\data\armor_odst_co.paa",
				"optre_unsc_units\army\data\legs_odst_co.paa",
				"optre_unsc_units\army\data\ghillie_woodland_co.paa",
				"optre_unsc_units\army\data\odst_armor_co.paa"
			};
		};
	};
};