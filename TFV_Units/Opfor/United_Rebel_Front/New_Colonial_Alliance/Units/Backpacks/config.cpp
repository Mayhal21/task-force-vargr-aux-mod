class CfgPatches
{
    class TFV_Patch_Units_Opfor_New_Colonial_Alliance_Backpacks
    {
        units[]={};
        weapons[]={};
        author="Task Force Vargr Aux Team";
        requiredAddons[]={"TCF_EQUIPMENT"};
    };
};
class CfgVehicles
{
    class B_combinationunitrespirator_01_f;
	class TFV_GMF_M73_Backpack: B_combinationunitrespirator_01_f
	{
		scope=1;
		scopeCurator=1;
		maximumLoad=400;
		class TransportMagazines
		{
			class _xx_OPTRE_200Rnd_95x40_Box_Tracer
			{
				magazine="OPTRE_200Rnd_95x40_Box_Tracer";
				count=5;
			};
		};
	};
	class TFV_GMF_AA_Backpack: B_combinationunitrespirator_01_f
	{
		scope=1;
		scopeCurator=1;
		maximumLoad=400;
		class TransportMagazines
		{
			class _xx_OPTRE_M41_Twin_HEAT_G
			{
				magazine="OPTRE_M41_Twin_HEAT_G";
				count=5;
			};
		};
	};
	class TFV_GMF_AT_Backpack: B_combinationunitrespirator_01_f
	{
		scope=1;
		scopeCurator=1;
		maximumLoad=400;
		class TransportMagazines
		{
			class _xx_OPTRE_M41_Twin_HEAT
			{
				magazine="OPTRE_M41_Twin_HEAT";
				count=5;
			};
		};
	};
	class TFV_GMF_Breacher_Backpack: B_combinationunitrespirator_01_f
	{
		scope=1;
		scopeCurator=1;
		maximumLoad=400;
		class TransportMagazines
		{
			class _xx_OPTRE_6Rnd_8Gauge_pellets
			{
				magazine="OPTRE_6Rnd_8Gauge_pellets";
				count=12;
			};
			class _xx_OPTRE_6Rnd_8Gauge_slugs
			{
				magazine="OPTRE_6Rnd_8Gauge_slugs";
				count=12;
			};
		};
	};
}; 