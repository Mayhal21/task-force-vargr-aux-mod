class CfgPatches
{
	class TFV_Units_Opfor_New_Colonial_Alliance_Vehicles
	{
		author="Task Force Vargr Aux Team";
		name="TaskForceVargr";
		addonRootClass="TFV_Units_Opfor_New_Colonial_Alliance";
		units[]=
		{
			"TFV_NCA_LAU",
			"TFV_NCA_PelicanAV",
			"TFV_NCA_Pelican"
		};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]={};
	};
};
class CfgVehicles
{
	class StaticWeapon;
	class StaticAAWeapon: StaticWeapon
	{
		class Turrets;
	};
	class OPTRE_LAU65D_pod: StaticAAWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class TFV_NCA_LAU: OPTRE_LAU65D_pod
	{
		scope=2;
		scopeCurator=2;
		side=0;
		crew="TFV_NCA_Crewman";
        faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_NCA";
		editorSubcategory="TFV_Editor_Subcategory_Turrets_NCA";
		displayName="Colonial AA System";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_GMF_LAU.jpg";
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				gunnerType="TFV_NCA_Crewman";
			};
		};
	};
	class OPTRE_Pelican_armed;
	class TFV_NCA_PelicanAV: OPTRE_Pelican_armed
	{
		scope=2;
		scopeCurator=2;
		scopeEditor=2;
		side=0;
		displayName="Colonial AV Pelican";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_NCA";
		editorSubcategory="TFV_Editor_Subcategory_AC_NCA";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_GMF_PelicanAV.jpg";
		crew="TFV_NCA_Crewman";
		class textureSources
		{
			class TFV_GMF
			{
				displayName="NCA";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\New_Colonial_Alliance\Vehicles\data\Pelican_GMF.paa"
				};
			};
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\New_Colonial_Alliance\Vehicles\data\Pelican_GMF.paa"
		};
		textureList[]=
		{
			"TFV_NCA",
			1
		};
	};
	class OPTRE_Pelican_unarmed;
	class TFV_NCA_Pelican: OPTRE_Pelican_unarmed
	{
		scope=2;
		scopeCurator=2;
		scopeEditor=2;
		side=0;
		displayName="Colonial Pelican";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_NCA";
		editorSubcategory="TFV_Editor_Subcategory_AC_NCA";
		crew="TFV_NCA_Crewman";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_GMF_Pelican.jpg";
		class textureSources
		{
			class TFV_GMF
			{
				displayName="NCA";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\New_Colonial_Alliance\Vehicles\data\Pelican_GMF.paa"
				};
			};
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\New_Colonial_Alliance\Vehicles\data\Pelican_GMF.paa"
		};
		textureList[]=
		{
			"TFV_NCA",
			1
		};
	};
};
