class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Vest
    {
        units[]={};
        weapons[]=
        {
            //Vest
            "TFV_SU_Vest_Light",
            "TFV_SU_Vest_Medium",
            "TFV_SU_Vest_ULight",
            "TFV_SU_Vest_Heavy"
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
	class OPTRE_UNSC_M52A_Armor3_DES;
	class VestItem;
	class V_PlateCarrier2_rgr;
	class TFV_SU_Vest_Light: V_PlateCarrier2_rgr
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[TFV] Union Light Vest";
		picture="\A3\Characters_F_Enoch\Vests\Data\UI\icon_V_CArrierRigKBT_01_light_Olive_F_CA.paa";
		model="\A3\Characters_F_Enoch\Vests\V_CarrierRigKBT_01_light_F.p3d";
		hiddenSelections[]=
		{
			"Camo"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\Frivest.paa"
		};
		class ItemInfo: VestItem
		{
			mass=40;
			hiddenSelections[]=
			{
				"Camo"
			};
			containerClass="Supply200";
			uniformModel="\A3\Characters_F_Enoch\Vests\V_CarrierRigKBT_01_light_F.p3d";
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=15;
					passThrough=0.30000001;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=15;
					passThrough=0.30000001;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=15;
					passThrough=0.30000001;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.30000001;
				};
			};
		};
	};
	class TFV_SU_Vest_Medium: V_PlateCarrier2_rgr
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[TFV] Union Medium Vest";
		picture="\A3\Characters_F_Enoch\Vests\Data\UI\icon_V_CArrierRigKBT_01_light_Olive_F_CA.paa";
		model="\A3\Characters_F_Enoch\Vests\V_CarrierRigKBT_01_heavy_F.p3d";
		hiddenSelections[]=
		{
			"Camo"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\Frivest.paa"
		};
		class ItemInfo: VestItem
		{
			mass=40;
			hiddenSelections[]=
			{
				"Camo"
			};
			containerClass="Supply200";
			uniformModel="\A3\Characters_F_Enoch\Vests\V_CarrierRigKBT_01_heavy_F.p3d";
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=15;
					passThrough=0.30000001;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=15;
					passThrough=0.30000001;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=15;
					passThrough=0.30000001;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.30000001;
				};
			};
		};
	};
	class TFV_SU_Vest_ULight: V_PlateCarrier2_rgr
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[TFV] Union Ultra Light Vest";
		picture="\A3\Characters_F_Enoch\Vests\Data\UI\icon_V_CArrierRigKBT_01_light_Olive_F_CA.paa";
		model="\A3\Characters_F_Enoch\Vests\V_CarrierRigKBT_01_F.p3d";
		hiddenSelections[]=
		{
			"Camo"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\Frivest.paa"
		};
		class ItemInfo: VestItem
		{
			mass=40;
			hiddenSelections[]=
			{
				"Camo"
			};
			containerClass="Supply200";
			uniformModel="\A3\Characters_F_Enoch\Vests\V_CarrierRigKBT_01_F.p3d";
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=15;
					passThrough=0.30000001;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=15;
					passThrough=0.30000001;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=15;
					passThrough=0.30000001;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.30000001;
				};
			};
		};
	};
	class TFV_SU_Vest_Heavy: OPTRE_UNSC_M52A_Armor3_DES
	{
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		displayName="[TFV] Union Heavy Armor";
		model="\TCF_Equipment\Vests\FriedenTacticalArmour\FriVest.p3d";
		picture="\OPTRE_UNSC_Units\Army\icons\Army_vest_wdl.paa";
		class ItemInfo: VestItem
		{
			uniformModel="\TCF_Equipment\Vests\FriedenTacticalArmour\FriVest.p3d";
			containerClass="Supply120";
			overlaySelectionsInfo[]=
			{
				"Ghillie_hide"
			};
			mass=75;
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=35;
					passThrough=0.1;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=35;
					passThrough=0.1;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=35;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=35;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=35;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.1;
					armor=35;
				};
			};
		};
	};
};