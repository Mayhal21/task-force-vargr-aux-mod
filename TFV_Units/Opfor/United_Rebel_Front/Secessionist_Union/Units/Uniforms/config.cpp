class CfgPatches
{
    class TFV_Patch_Units_Opfor_United_Rebel_Front_Secessionist_Union_Uniforms
    {
        units[]={};
        weapons[]=
        {
            //Uniform
            "TFV_SU_UNI_Base",
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgWeapons
{
    class UniformItem;
    class uniform_base;
    class TFV_SU_UNI_Base: uniform_base
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[TFV] Union BDU";
		picture="\A3\characters_f_beta\data\ui\icon_U_IR_CrewUniform_rucamo_ca.paa";
		model="\A3\Characters_F\Common\Suitpacks\suitpack_original_F.p3d";
		class ItemInfo: UniformItem
		{
			model="\A3\Characters_F\Common\Suitpacks\suitpack_original_F.p3d";
			uniformClass="TFV_SU_Autumn";
			containerClass="Supply40";
			mass=40;
		};
	};
};
class CfgVehicles
{
    class O_Soldier_F;
	class TFV_SU_Base: O_Soldier_F
	{
        scope=0;
        scopeCurator=0;
        side=0;
        author="Task Force Vargr Aux Team";
        UniformClass="";
        vehicleClass="";
        model="";
        weapons[]={"Throw","Put"};
        respawnWeapons[]= {"Throw","Put"};
        Items[]={"OPTRE_Biofoam"};
        RespawnItems[]={"OPTRE_Biofoam"};
        magazines[]={};
        respawnMagazines[]={};
        hiddenSelections[]=
        {
            "camo"
        };
        hiddenSelectionsTextures[]={};
	};
    class TFV_SU_Autumn: TFV_SU_Base
	{
		author="J.Burgess";
		displayName="Base";
		scope=1;
		model="\a3\Characters_F_Enoch\Uniforms\I_E_Soldier_01_F.p3d";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"insignia"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\Fri_U_A_W.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\Fri_U_A_W.paa",
			"a3\characters_f_enoch\uniforms\data\i_e_soldier_01_gloves_black_co.paa"
		};
	};
};