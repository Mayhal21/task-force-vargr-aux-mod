class CfgPatches
{
    class TFV_Patch_Units_Bluefor_TFV_UNSC_ODST_Backpacks
    {
        units[]={};
        weapons[]={};
        author="Task Force Vargr Aux Team";
        requiredAddons[]={};
    };
};
class CfgVehicles
{
    //custom supply sizes
	class ContainerSupply;
	class Supply300: ContainerSupply		// The class name does not really matter, but for clarity, it should be SupplyXX, where XX is the desired capacity value.
	{
		maximumLoad = 450;				// Replace XX with the desired capacity value.
	};
    //Bags go in here 
    class OPTRE_UNSC_Rucksack_Heavy;
	class TFV_M73_Backpack: OPTRE_UNSC_Rucksack_Heavy
	{
		scope=1;
		scopeCurator=1;
		scopeArsenal=1;
		baseWeapon="OPTRE_UNSC_Rucksack_Heavy";
		class TransportMagazines
		{
			class _xx_OPTRE_200Rnd_95x40_Box_Tracer
			{
				magazine="OPTRE_200Rnd_95x40_Box_Tracer";
				count=5;
			};
		};
	};
};
