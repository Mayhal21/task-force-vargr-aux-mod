class CfgPatches
{
    class TFV_Patch_Units_Opfor_Secessionist_Union_Helmets
    {
        units[]={};
        weapons[]=
        {
            //Helmets
            "TFV_MC_AUTUMN",
            "TFV_Black_beret",
            "TFV_Helmet_Aut"
        };
        author="Task Force Vargr Aux Team";
        requiredAddons[]={"TCF_EQUIPMENT"};
    };
};
class CfgWeapons
{
    class UniformItem;
    class H_HelmetB;
    class TCF_Beret_CGC;
    class ItemInfo;
    class VestItem;
    class HeadgearItem;
    class HitpointsProtectionInfo;
    //Helmets
    class HelmetBase;
	class H_MilCap_ocamo: HelmetBase
	{
		class ItemInfo;
	};
    class TFV_MC_AUTUMN: H_MilCap_ocamo
	{
		displayName="[TFV] Union Military Cap (Autumn)";
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\TCF_MC_Autumn.paa"
		};
		allowedFacewear[]=
		{
			"G_Balaclava_ti_g_blk_f",
			1
		};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[]=
			{
				"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\TCF_MC_Autumn"
			};
		};
	};
	class TFV_Black_beret: TCF_Beret_CGC
	{
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Team";
		displayName="[TFV] Union Beret (Black)";
		weaponPoolAvailable=1;
		subItems[]=
		{
			"ItemcTabHCam"
		};
		ace_hearing_protection=5;
		ace_hearing_lowerVolume=0.25;
		picture="\A3\Characters_F_Bootcamp\Data\UI\icon_H_Beret_Colonel_ca.paa";
		model="a3\characters_f_epb\BLUFOR\headgear_beret02";
		hiddenSelections[]=
		{
			"Camo"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\FRI_Beret.paa"
		};
		visionMode[]=
		{
			"Normal",
			"TI",
			"NVG"
		};
		thermalMode[]={0,1,2,3,4,5};
	};
	class TFV_Helmet_Aut: H_HelmetB
	{
		author="LM Mods";
		scope=2;
		displayName="[TFV] Union Ballistic Helmet (Autumn)";
		picture="\A3\characters_f\Data\UI\icon_H_helmet_plain_ca.paa";
		model="\TCF_EQUIPMENT\Helmets\BallisticHelmet\LWHA.p3d";
		hiddenSelections[]=
		{
			"Camo"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Units\data\lwh_coW.paa"
		};
		class ItemInfo: HeadgearItem
		{
			mass=40;
			hiddenSelections[]=
			{
				"Camo"
			};
			uniformModel="\TCF_EQUIPMENT\Helmets\BallisticHelmet\LWHA.p3d";
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitPointName="HitHead";
					armor=7;
					passThrough=0.5;
				};
			};
		};
	};
};