class CfgPatches
{
	class TFC_Units_Seccessionist_Union
	{
		author="Task Force Vargr Aux Team";
		name="Taskforcevargr";
		addonRootClass="TFV_Units";
		units[]={};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"TCF_OPFOR"
		};
	};
};
class CfgEditorCategories
{
	class TFV_Editor_Category_SU
	{
		displayName = "[Vargr] Secessionist Union";
		scopeCurator=2;
		scopeeditor=2;
		scope=2;
	};
};
class CfgEditorSubcategories
{
	class TFV_Editor_Subcategory_AC_SU
    {
        displayName = "Aircraft";
    };
    class TFV_Editor_Subcategory_AV_SU
    {
        displayName = "Armored Vehicles";
    };
    class TFV_Editor_Subcategory_Inf_SU
    {
        displayName = "Regulars";
    };
    class TFV_Editor_Subcategory_Vehcles_SU
    {
        displayName = "Vehicles";
    };
	class TFV_Editor_Subcategory_Turrets_SU
    {
        displayName = "Turrets";
    };
};

