class CfgPatches
{
	class TFV_Units_Seccessionist_Union_Vehicles
	{
		author="Task Force Vargr Aux Team";
		name="TaskForceVargr";
		addonRootClass="TFV_Units_Seccessionist_Union";
		units[]=
		{
			"TFV_FRI_LAU",
			"TFV_M92_BOLT_FRI",
			"TFV_M48_Buzz_FRI",
			"TFV_SAM_Regular",
			"TFV_RAD_Regular",
			"TFV_Felix_Rotary_Frieden",
			"TFV_Witherwing_Frieden",
			"TFV_Ansaldo_Frieden",
			"TFV_M494_IFV",
			"TFV_M808B_FRI",
			"TFV_M808BM_FRI",
			"TFV_Raptor_Frieden_IFV",
			"TFV_Raptor_Frieden_APC",
			"TFV_D56I",
			"TFV_D56V",
			"TFV_D81LRTA",
			"TFV_D81LRTI",
			"TFV_M247H_FRI"
		};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"TCF_OPFOR_Frieden"
		};
	};
};
class CfgVehicles
{
	class StaticWeapon;
	class StaticAAWeapon: StaticWeapon
	{
		class Turrets;
	};
	class OPTRE_LAU65D_pod: StaticAAWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class TFV_FRI_LAU: OPTRE_LAU65D_pod
	{
		scope=2;
		scopeCurator=2;
		side=0;
		crew="TFV_SU_Crewman";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_Turrets_SU";
		displayName="LAU-65D/SGM-151";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_FRI_LAU.jpg";
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				gunnerType="TFV_SU_Crewman";
			};
		};
	};
	class B_SAM_System_01_F;
	class TFV_M92_BOLT_FRI_Base: B_SAM_System_01_F
	{
		scope=0;
		scopeCurator=0;
		side=0;
		displayName="M92 Bolt SAM";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_Turrets_SU";
		editorPreview="\TCF_MISC\EditorPreviews\LM_OPCAN_M92_BOLT_FRI.jpg";
		hiddenSelections[]=
		{
			"camo1",
			"Camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\M92_BOLT_FRI_co.paa"
		};
		textureList[]=
		{
			"INS",
			1
		};
		class TextureSources
		{
			class INS
			{
				displayName="INS";
				author="$STR_A3_Bohemia_Interactive";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\M92_BOLT_FRI_co.paa"
				};
			};
		};
		class Turrets;
	};
	class TFV_M92_BOLT_FRI_Base_F: TFV_M92_BOLT_FRI_Base
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class TFV_M92_BOLT_FRI: TFV_M92_BOLT_FRI_Base_F
	{
		scope=2;
		scopeCurator=2;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[]=
				{
					"OPTRE_M95"
				};
				magazines[]=
				{
					"OPTRE_6Rnd_ASGM7_rockets",
					"OPTRE_6Rnd_ASGM7_rockets",
					"OPTRE_6Rnd_ASGM7_rockets",
					"OPTRE_6Rnd_ASGM7_rockets",
					"OPTRE_6Rnd_ASGM7_rockets"
				};
			};
		};
	};
	class B_AAA_System_01_F;
	class TFV_M48_Buzz_FRI_Base: B_AAA_System_01_F
	{
		scope=0;
		scopeCurator=0;
		side=0;
		displayName="M48 Buzzsaw CIWS";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_Turrets_SU";
		editorPreview="\TCF_MISC\EditorPreviews\LM_OPCAN_M48_Buzz_FRI.jpg";
		hiddenSelections[]=
		{
			"camo1",
			"Camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Buzzard\M48_BUZZ_G_FRI_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Buzzard\M48_BUZZ_B_FRI_co.paa"
		};
		textureList[]=
		{
			"Green",
			1
		};
		class TextureSources
		{
			class Green
			{
				displayName="Green";
				author="$STR_A3_Bohemia_Interactive";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Buzzard\M48_BUZZ_G_FRI_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Buzzard\M48_BUZZ_B_FRI_co.paa"
				};
			};
		};
		class Turrets;
	};
	class TFV_M48_Buzz_FRI_Base_F: TFV_M48_Buzz_FRI_Base
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class TFV_M48_Buzz_FRI: TFV_M48_Buzz_FRI_Base_F
	{
		scope=2;
		scopeCurator=2;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[]=
				{
					"OPTRE_M71"
				};
				magazines[]=
				{
					"OPTRE_9999Rnd_20mm_AAHEIAP",
					"OPTRE_9999Rnd_20mm_AAHEIAP",
					"OPTRE_9999Rnd_20mm_AAHEIAP",
					"OPTRE_9999Rnd_20mm_AAHEIAP",
					"OPTRE_9999Rnd_20mm_AAHEIAP"
				};
			};
		};
	};
	class SAM_System_03_base_F;
	class TFV_SAM_Regular: SAM_System_03_base_F
	{
		displayName="MIM-145 Defender";
		scope=2;
		scopeCurator=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_Turrets_SU";
		editorPreview="\TCF_MISC\EditorPreviews\FRI_SAM_Regular.jpg";
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\SAM\FRI_SAM01_01_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\SAM\FRI_SAM01_02_co.paa"
		};
		crew="O_UAV_AI";
		typicalCargo[]=
		{
			"B_UAV_AI"
		};
	};
	class Radar_System_01_base_F;
	class TFV_RAD_Regular: Radar_System_01_base_F
	{
		displayName="AN/MPQ-105 Radar";
		scope=2;
		scopeCurator=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_Turrets_SU";
		editorPreview="\TCF_MISC\EditorPreviews\FRI_RAD_Regular.jpg";
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\RAD\FRI_RAD01_01_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\RAD\FRI_RAD01_02_co.paa"
		};
		crew="O_UAV_AI";
		typicalCargo[]=
		{
			"O_UAV_AI"
		};
	};
	
	class I_Heli_light_03_F;
	class TFV_Felix_Rotary_Frieden: I_Heli_light_03_F
	{
		author="Task Force Vargr Aux Team";
		crew="TFV_SU_Crewman";
		scope=2;
		scopeArsenal=2;
		scopeCurator=2;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_Felix_Rotary_Frieden.jpg";
		displayName="UH-95 Felix";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AC_SU";
		hiddenSelections[]=
		{
			"camo"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Heli_Light_03_base_frieden_CO.paa"
		};
		side=0;
		transportSoldier=4;
	};
	class I_Plane_Fighter_04_F;
	class TFV_Witherwing_Frieden: I_Plane_Fighter_04_F
	{
		author="Task Force Vargr Aux Team";
		crew="TFV_SU_Crewman";
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_Witherwing_Frieden.jpg";
		displayName="V-F22 Witherwing";
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AC_SU";
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Witherwing\Witherwing_01_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Witherwing\Witherwing_02_CO.paa"
		};
	};
	class MRAP_02_base_F;
	class MRAP_02_hmg_base_F: MRAP_02_base_F
	{
		class Turrets;
	};
	class O_MRAP_02_HMG_F: MRAP_02_hmg_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class TFV_Ansaldo_Frieden: O_MRAP_02_HMG_F
	{
		displayName="M32 Ansaldo";
		author="Task Force Vargr Aux Team";
		crew="TFV_SU_Crewman";
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_Ansaldo_Frieden.jpg";
		tf_hasLRradio=1;
		tf_isolatedAmount=0.64999998;
		tf_range=120000;
		ace_cargo_size=10;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_Vehcles_SU";
		class TransportMagazines
		{
			delete _xx_1Rnd_HE_Grenade_shell;
			delete _xx_30Rnd_65x39_caseless_green;
			delete _xx_150Rndx762x54_Box;
			delete _xx_16rn_9x21_Mag;
			delete _xx_RPG32_F;
			delete _xx_1Rnd_SmokeOrange_Grenade_shell;
			delete _xx_1Rnd_SmokeRed_Grenade_shell;
			delete _xx_1Rnd_Smoke_Grenade_shell;
			delete _xx_1Rnd_SmokeYellow_Grenade_shell;
		};
		class TransportItems
		{
			delete _xx_FirstAidKit;
			delete _xx_SmokeShellOrange;
			delete _xx_SmokeShellRed;
			delete _xx_SmokeShellYellow;
			delete _xx_SmokeShell;
			delete _xx_HandGrenade;
			class _xx_ACE_epinephrine
			{
				name="ACE_epinephrine";
				count=40;
			};
			class _xx_ACE_adenosine
			{
				name="ACE_adenosine";
				count=40;
			};
			class _xx_ACE_Banana
			{
				name="ACE_Banana";
				count=10;
			};
			class _xx_ACE_splint
			{
				name="ACE_splint";
				count=20;
			};
		};
		class TransportWeapons
		{
			delete _xx_arifle_Katiba_F;
		};
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[]=
				{
					"TCF_M250_APC",
					"TCF_MG460_APC",
					"SmokeLauncher"
				};
				magazines[]=
				{
					"TCF_40Rnd_HEDP_Belt",
					"TCF_40Rnd_HEDP_Belt",
					"TCF_400Rnd_127x99_M250HMG",
					"TCF_400Rnd_127x99_M250HMG",
					"TCF_400Rnd_127x99_HE_M250HMG",
					"TCF_400Rnd_127x99_HE_M250HMG",
					"SmokeLauncherMag",
					"SmokeLauncherMag"
				};
			};
		};
		textureList[]=
		{
			"OPF",
			0
		};
		class TextureSources
		{
			class ANS_Frieden
			{
				displayName="Union";
				author="Task Force Vargr Aux Team";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Ansaldo\Ansaldo_FRI_Front_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Ansaldo\Ansaldo_FRI_Back_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Turret_Frieden.paa"
				};
				factions[]=
				{
					"TFV_FactionClasses_URF"
				};
			};
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Ansaldo\Ansaldo_FRI_Front_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Ansaldo\Ansaldo_FRI_Back_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Turret_Frieden.paa"
		};
	};
	class OPTRE_M494;
	class TFV_M494_IFV: OPTRE_M494
	{
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		displayName="M494 Oryx";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_M494_IFV.jpg";
		crew="TFV_SU_Crewman";
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AV_SU";
		textureList[]=
		{
			"Frieden",
			1
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_armor_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_main_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_net_ca.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_net_ca.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_net_ca.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_turret_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorpion_mg_co.paa"
		};
		class textureSources
		{
			class Frieden
			{
				displayName="Union";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_armor_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_main_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_net_ca.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_net_ca.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_net_ca.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Oryx\fri_oryx_turret_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorpion_mg_co.paa"
				};
			};
		};
	};
	class OPTRE_M808B_Base;
	class TFV_M808B_FRI: OPTRE_M808B_Base
	{
		crew="TFV_SU_Crewman";
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		side=0;
		displayName="M808B Scorpion";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_M808B_FRI.jpg";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AV_SU";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_lopo_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_tur_wood_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\fri_det_3_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_decals_ca.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
			"OPTRE_Vehicles\Scorpion\data\texture\mine_roller_co.paa"
		};
		class textureSources
		{
			class colorstand
			{
				displayName="Frieden";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_lopo_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_tur_wood_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\fri_det_3_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_decals_ca.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
					"OPTRE_Vehicles\Scorpion\data\texture\mine_roller_co.paa"
				};
			};
		};
		tf_range=25000;
		tf_isolatedAmount=0.40000001;
		tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel;";
		tf_hasLRradio=1;
		enableRadio=1;
	};
	class OPTRE_M808BM_Base;
	class TFV_M808BM_FRI: OPTRE_M808BM_Base
	{
		crew="TFV_SU_Crewman";
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		side=0;
		displayName="M808B/M Scorpion";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_M808BM_FRI.jpg";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AV_SU";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4",
			"camo5",
			"camo6",
			"camo7",
			"camo8"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_lopo_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_tur_wood_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\fri_det_3_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_decals_ca.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
			"OPTRE_Vehicles\Scorpion\data\texture\mine_roller_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorpion_mg_co.paa"
		};
		class textureSources
		{
			class colorstand
			{
				displayName="Frieden";
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_lopo_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_tur_wood_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\fri_det_3_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_decals_ca.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorp_net_wood_ca.paa",
					"OPTRE_Vehicles\Scorpion\data\texture\mine_roller_co.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Scorpion\fri_Scorpion_mg_co.paa"
				};
			};
		};
		tf_range=25000;
		tf_isolatedAmount=0.40000001;
		tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel;";
		tf_hasLRradio=1;
		enableRadio=1;
	};
	class APC_Wheeled_03_base_F;
	class I_APC_Wheeled_03_base_F: APC_Wheeled_03_base_F
	{
		class Turrets;
	};
	class I_APC_Wheeled_03_cannon_F: I_APC_Wheeled_03_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
		class EventHandlers;
	};
	class TFV_Raptor_Frieden_IFV: I_APC_Wheeled_03_cannon_F
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		displayName="M3 Cougar IFV";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_Raptor_Frieden_IFV.jpg";
		picture="\A3\armor_f_gamma\APC_Wheeled_03\Data\UI\APC_Wheeled_03_CA.paa";
		crew="TFV_SU_Crewman";
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AV_SU";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Raptor\Raptor_01_Ext_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Raptor\Raptor_02_Ext_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Raptor\Raptor_Turret_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Raptor\Raptor_03_Ext_co.paa"
		};
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				body="mainTurret";
				gun="mainGun";
				weapons[]=
				{
					"TCF_M1024_30mm",
					"TCF_M247_APC",
					"missiles_titan",
					"SmokeLauncher"
				};
				magazines[]=
				{
					"TCF_80Rnd_30mm_HEAT",
					"TCF_80Rnd_30mm_HEAT",
					"TCF_80Rnd_30mm_HEAT",
					"TCF_80Rnd_30mm_HEAT",
					"TCF_80Rnd_30mm_HEAT",
					"TCF_80Rnd_30mm_HEAT",
					"TCF_60Rnd_30mm_APFSDS",
					"TCF_60Rnd_30mm_APFSDS",
					"TCF_60Rnd_30mm_APFSDS",
					"TCF_60Rnd_30mm_APFSDS",
					"TCF_400Rnd_762x51_Box",
					"TCF_400Rnd_762x51_Box",
					"TCF_400Rnd_762x51_Box",
					"TCF_400Rnd_762x51_Box",
					"TCF_400Rnd_762x51_Box",
					"TCF_400Rnd_762x51_Box",
					"2Rnd_GAT_missiles",
					"2Rnd_GAT_missiles",
					"SmokeLauncherMag",
					"SmokeLauncherMag"
				};
			};
		};
		class TransportMagazines
		{
			delete _xx_1Rnd_HE_Grenade_shell;
			delete _xx_30Rnd_65x39_caseless_green;
			delete _xx_150Rndx762x54_Box;
			delete _xx_16rn_9x21_Mag;
			delete _xx_RPG32_F;
			delete _xx_1Rnd_SmokeOrange_Grenade_shell;
			delete _xx_1Rnd_SmokeRed_Grenade_shell;
			delete _xx_1Rnd_Smoke_Grenade_shell;
			delete _xx_1Rnd_SmokeYellow_Grenade_shell;
		};
		class TransportItems
		{
			delete _xx_FirstAidKit;
			delete _xx_SmokeShellOrange;
			delete _xx_SmokeShellRed;
			delete _xx_SmokeShellYellow;
			delete _xx_SmokeShell;
			delete _xx_HandGrenade;
			class _xx_ACE_epinephrine
			{
				name="ACE_epinephrine";
				count=40;
			};
			class _xx_ACE_adenosine
			{
				name="ACE_adenosine";
				count=40;
			};
			class _xx_ACE_Banana
			{
				name="ACE_Banana";
				count=10;
			};
			class _xx_ACE_splint
			{
				name="ACE_splint";
				count=20;
			};
		};
	};
	class TFV_Raptor_Frieden_APC: I_APC_Wheeled_03_cannon_F
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		displayName="M3 Cougar APC";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_Raptor_Frieden_APC.jpg";
		crew="TFV_SU_Crewman";
		class EventHandlers: EventHandlers
		{
			init="if (local (_this select 0)) then {{(_this select 0) animate [_x, 1]} forEach ['HideHull','HideTurret']}";
		};
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AV_SU";
		side=0;
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3",
			"camo4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Raptor\Raptor_01_Ext_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Raptor\Raptor_02_Ext_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Raptor\Raptor_03_Ext_co.paa"
		};
		threat[]={0,0,0};
		class TransportMagazines
		{
			delete _xx_1Rnd_HE_Grenade_shell;
			delete _xx_30Rnd_65x39_caseless_green;
			delete _xx_150Rndx762x54_Box;
			delete _xx_16rn_9x21_Mag;
			delete _xx_RPG32_F;
			delete _xx_1Rnd_SmokeOrange_Grenade_shell;
			delete _xx_1Rnd_SmokeRed_Grenade_shell;
			delete _xx_1Rnd_Smoke_Grenade_shell;
			delete _xx_1Rnd_SmokeYellow_Grenade_shell;
		};
		class TransportItems
		{
			delete _xx_FirstAidKit;
			delete _xx_SmokeShellOrange;
			delete _xx_SmokeShellRed;
			delete _xx_SmokeShellYellow;
			delete _xx_SmokeShell;
			delete _xx_HandGrenade;
			class _xx_ACE_epinephrine
			{
				name="ACE_epinephrine";
				count=40;
			};
			class _xx_ACE_adenosine
			{
				name="ACE_adenosine";
				count=40;
			};
			class _xx_ACE_Banana
			{
				name="ACE_Banana";
				count=10;
			};
			class _xx_ACE_splint
			{
				name="ACE_splint";
				count=20;
			};
		};
	};
	class O_T_VTOL_02_infantry_dynamicLoadout_F;
	class TFV_D56I: O_T_VTOL_02_infantry_dynamicLoadout_F
	{
		crew="TFV_SU_Crewman";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AC_SU";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_D56I.jpg";
		side=0;
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		displayName="D56I-TC/AV Wyvern";
		typicalCargo[]=
		{
			"TFV_SU_Crewman"
		};
		class TextureSources
		{
			class Black
			{
				displayName="Black";
				author="Task Force Vargr Aux Team";
				factions[]=
				{
					"TFV_FactionClasses_URF"
				};
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_01_black_CO.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_02_black_CO.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_L_black_CO.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_R_black_CO.paa"
				};
			};
		};
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2",
			"Camo_3",
			"Camo_4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_01_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_02_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_L_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_R_black_CO.paa"
		};
	};
	class O_T_VTOL_02_vehicle_dynamicLoadout_F;
	class TFV_D56V: O_T_VTOL_02_vehicle_dynamicLoadout_F
	{
		crew="TFV_SU_Crewman";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AC_SU";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_D56I.jpg";
		side=0;
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		displayName="D56V-TC/AV Wyvern";
		typicalCargo[]=
		{
			"TFV_SU_Crewman"
		};
		class TextureSources
		{
			class Black
			{
				displayName="Black";
				author="Task Force Vargr Aux Team";
				factions[]=
				{
					"TFV_FactionClasses_URF"
				};
				textures[]=
				{
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_01_black_CO.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_02_black_CO.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_L_black_CO.paa",
					"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_R_black_CO.paa"
				};
			};
		};
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2",
			"Camo_3",
			"Camo_4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_01_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_02_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_L_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Wyvern\Wyvern_Frieden_03_R_black_CO.paa"
		};
	};
	class B_T_VTOL_01_armed_F;
	class TFV_D81LRTA_Base: B_T_VTOL_01_armed_F
	{
		scope=0;
		scopeCurator=0;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_D81LRTA.jpg";
		crew="TFV_SU_Crewman";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AC_SU";
		side=0;
		displayName="D81-LRTA Harpy";
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2",
			"Camo_3",
			"Camo_4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_01_black_co.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_02_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_03_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_04_black_CO.paa"
		};
		class ACE_Cargo
		{
			class Cargo
			{
				class TCF_Speedbag
				{
					type="TCF_Speedbag";
					amount=4;
				};
			};
		};
		class Turrets;
	};
	class TFV_D81LRTA_Base_F: TFV_D81LRTA_Base
	{
		class Turrets: Turrets
		{
			class CopilotTurret;
			class GunnerTurret_01;
			class GunnerTurret_02;
		};
	};
	class TFV_D81LRTA: TFV_D81LRTA_Base_F
	{
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		class Turrets: Turrets
		{
			class CopilotTurret: CopilotTurret
			{
			};
			class GunnerTurret_01: GunnerTurret_01
			{
				magazines[]=
				{
					"OPTRE_1Rnd_MAC_Rounds",
					"OPTRE_1Rnd_MAC_Rounds",
					"OPTRE_1Rnd_MAC_Rounds",
					"OPTRE_1Rnd_MAC_Rounds",
					"OPTRE_1Rnd_MAC_Rounds",
					"OPTRE_1Rnd_MAC_Rounds",
					"OPTRE_1Rnd_MAC_Rounds",
					"OPTRE_1Rnd_MAC_Rounds",
					"Laserbatteries"
				};
				weapons[]=
				{
					"OPTRE_MAC_Cannon",
					"Laserdesignator_mounted"
				};
			};
			class GunnerTurret_02: GunnerTurret_02
			{
				magazines[]=
				{
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_SpLaser_Battery",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP",
					"OPTRE_2000Rnd_20mm_HEIAP"
				};
				weapons[]=
				{
					"VES_M6_Laser",
					"OPTRE_GUA23A"
				};
			};
		};
	};
	class B_T_VTOL_01_infantry_F;
	class TFV_D81LRTI: B_T_VTOL_01_infantry_F
	{
		crew="TFV_SU_Crewman";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AC_SU";
		side=0;
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		displayName="D81-LRTI Harpy";
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2",
			"Camo_3",
			"Camo_4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_01_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_02_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_03_black_CO.paa",
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\Harpy\Harpy_Frieden_04_black_CO.paa"
		};
	};
	class B_Heli_Attack_01_Dynamicloadout_F;
	class TFV_UH101_FRI: B_Heli_Attack_01_Dynamicloadout_F
	{
		displayname="UH-101 Eagle";
		Side=0;
		crew="TFV_SU_Crewman";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_AC_SU";
		editorPreview="\TCF_MISC\EditorPreviews\LM_OPCAN_UH101_FRI.jpg";
		hiddenSelections[]=
		{
			"camo1"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\Opfor\United_Rebel_Front\Secessionist_Union\Vehicles\data\UH101.paa"
		};
	};
	class OPTRE_Static_M247H_Shielded_Tripod;
	class TFV_M247H_FRI: OPTRE_Static_M247H_Shielded_Tripod
	{
		scope=2;
		scopeCurator=2;
		side=0;
		crew="TFV_SU_Crewman";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_SU";
		editorSubcategory="TFV_Editor_Subcategory_Turrets_SU";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_M247H_FRI.jpg";
	};
};
class CfgWeapons
{
};
