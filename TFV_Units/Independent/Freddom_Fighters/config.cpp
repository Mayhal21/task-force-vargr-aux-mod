class CfgPatches
{
	class TCF_Units_Freedom_Fighters
	{
		author="Task Force Vargr Aux Team";
		name="TaskForceVargr";
		addonRootClass="TFV_Units";
		units[]={};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"TCF_OPFOR"
		};
	};
};
class CfgFactionClasses
{
	class TFV_FactionClasses_URF
    {
        displayName = "[TFV] United Rebel Front";
        priority=1;
		side=0;
		primaryLanguage="EN";
		backpack_tf_faction_radio_api="OPTRE_Como_pack_2";
    };
};
class CfgEditorCategories
{
	class TFV_Editor_Category_FF
	{
		displayName="[TFV] Freedom Fighters";
		scopeCurator=2;
		scopeeditor=2;
		scope=2;
	};
};
class CfgEditorSubcategories
{
	class TFV_Editor_Subcategory_AC_FF
    {
        displayName = "[TFV] Aircraft";
    };
    class TFV_Editor_Subcategory_AV_FF
    {
        displayName = "[TFV] Armored Vehicles";
    };
    class TFV_Editor_Subcategory_Inf_FF
    {
        displayName = "[TFV] Insurgents";
    };
    class TFV_Editor_Subcategory_Vehcles_FF
    {
        displayName = "[TFV] Light Vehicles";
    };
    class TFV_Editor_Subcategory_Turrets_FF
    {
        displayName = "[TFV] Turrets";
    };
};
