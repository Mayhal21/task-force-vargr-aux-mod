class CfgPatches
{
	class TFV_Units_Freedom_Fighters_Vehicles
	{
		author="Task Force Vargr Aux Team";
		name="TaskForceVargr";
		addonRootClass="TFV_Units_Freedom_fighters";
		units[]=
		{
			"TFV_ER_Silverback_FAV",
			"TFV_ER_Armoured_Silverback_FAV",
			"TFV_ER_Silverback_LRV",
			"TFV_ER_Silverback_Armoured_LRV",
			"TFV_ER_Silverback_TOW",
			"TFV_ER_Silverback_Armoured_TOW",
			"TFV_ER_Barricade_Hog",
			"TFV_ER_M247_Hog",
			"TFV_ER_LAU65_Hog",
			"TFV_ER_Nightingale",
			"TFV_ER_WDL_PelicanAV",
			"TFV_ER_WDL_Pelican",
			"TFV_ER_LAU",
			"TFV_unarmed_puma_02",
			"TFV_armed_puma_02",
			"TFV_AT_puma_02",
			"TFV_tarantula_02",
			"TFV_ferret_02",
			"TFV_salamander_02",
			"TFV_salamanderagl_02",
			"TFV_raven_02",
			"TFV_VTOLV_02",
			"TFV_VTOLI_02",
			"TFV_gladius_02",
			"TFV_gladius_02L"
		};
		weapons[]={};
		magazines[]={};
		ammo[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"TCF_OPFOR_Eridanus"
		};
	};
};
class CfgVehicles
{
	class TCF_Silverback_FAV;
	class TFV_ER_Silverback_FAV: TCF_Silverback_FAV
	{
		dlc="TCF";
		scope=2;
		scopeArsenal=2;
		scopeCurator=2;
		displayName="SB14 LRV Silverback";
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		crew="TFV_ER_Hacker";
		hiddenSelections[]=
		{
			"Camo1",
			"Camo2",
			"Camo3",
			"BL_Door",
			"BR_Door",
			"RF_Door",
			"LF_Door"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hood_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Trunk_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa"
		};
	};
	class TCF_Silverback_Armoured_FAV;
	class TFV_ER_Armoured_Silverback_FAV: TCF_Silverback_Armoured_FAV
	{
		displayName="SB14 LRV Silverback (Up-Armored)";
		scope=2;
		scopeCurator=2;
		scopeeditor=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		crew="TFV_ER_Hacker";
		hiddenSelections[]=
		{
			"Camo1",
			"Camo2",
			"Camo3",
			"BL_Door",
			"BR_Door",
			"RF_Door",
			"LF_Door"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hood_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Trunk_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa"
		};
	};
	class TCF_Silverback_LRV;
	class TFV_ER_Silverback_LRV: TCF_Silverback_LRV
	{
		dlc="TCF";
		scope=2;
		scopeArsenal=2;
		scopeCurator=2;
		displayName="SB14 LRV Silverback (MG)";
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		crew="TFV_ER_Hacker";
		transportSoldier=3;
		hiddenSelections[]=
		{
			"Camo1",
			"Camo2",
			"Camo3",
			"BL_Door",
			"BR_Door",
			"RF_Door",
			"LF_Door",
			"Turret",
			"Turret_Ring",
			"Ammo_belt"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hood_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Trunk_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Turret_MG_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Turret_ring_mg_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Ammo_Belt_co.paa"
		};
	};
	class TCF_Silverback_Armoured_LRV;
	class TFV_ER_Silverback_Armoured_LRV: TCF_Silverback_Armoured_LRV
	{
		displayName="SB14 LRV Silverback (Armoured MG)";
		scope=2;
		scopeCurator=2;
		scopeeditor=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		crew="TFV_ER_Hacker";
		hiddenSelections[]=
		{
			"Camo1",
			"Camo2",
			"Camo3",
			"BL_Door",
			"BR_Door",
			"RF_Door",
			"LF_Door",
			"Turret",
			"Turret_Ring",
			"Ammo_belt"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hood_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Trunk_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Turret_MG_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Turret_ring_mg_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Ammo_Belt_co.paa"
		};
	};
	class TCF_Silverback_TOW;
	class TFV_ER_Silverback_TOW: TCF_Silverback_TOW
	{
		dlc="TCF";
		scope=2;
		scopeArsenal=2;
		scopeCurator=2;
		displayName="SB14 LRV Silverback (TOW)";
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		crew="TFV_ER_Hacker";
		hiddenSelections[]=
		{
			"Camo1",
			"Camo2",
			"Camo3",
			"BL_Door",
			"BR_Door",
			"RF_Door",
			"LF_Door",
			"Main_Gun",
			"Turret_Ring"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hood_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Trunk_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_missile_luncher_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_missile_ring_co.paa"
		};
	};
	class TCF_Silverback_Armoured_TOW;
	class TFV_ER_Silverback_Armoured_TOW: TCF_Silverback_Armoured_TOW
	{
		scope=2;
		scopeCurator=2;
		scopeeditor=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		crew="TFV_ER_Hacker";
		displayName="SB14 LRV Silverback (Armoured TOW)";
		hiddenSelections[]=
		{
			"Camo1",
			"Camo2",
			"Camo3",
			"BL_Door",
			"BR_Door",
			"RF_Door",
			"LF_Door",
			"Main_Gun",
			"Turret_Ring"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Hood_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Trunk_Hull_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Rear_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Front_Door_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_missile_luncher_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_missile_ring_co.paa"
		};
	};
	class TCF_Barricade_Hog_Base;
	class TFV_ER_Barricade_Hog: TCF_Barricade_Hog_Base
	{
		side=0;
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		hiddenSelections[]=
		{
			"camo1",
			"camo2",
			"camo3"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extupper_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extunder_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12_turret_co.paa"
		};
		displayName="M12 Armadillo";
		crew="TFV_ER_Hacker";
		class textureSources
		{
			class scorp
			{
				displayName="Tauri camo";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extupper_co.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extunder_co.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12_turret_co.paa"
				};
				materials[]={};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class TCF_M247_Hog_Base;
	class TFV_ER_M247_Hog: TCF_M247_Hog_Base
	{
		side=0;
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extupper_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extunder_co.paa"
		};
		displayName="M12 Warthog FAV (M247)";
		crew="TFV_ER_Hacker";
		class textureSources
		{
			class scorp
			{
				displayName="Tauri camo";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extupper_co.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extunder_co.paa"
				};
				materials[]={};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class TCF_LAU65_Hog_Base;
	class TFV_ER_LAU65_Hog: TCF_LAU65_Hog_Base
	{
		side=0;
		scope=2;
		scopeCurator=2;
		scopeArsenal=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extupper_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_m12HogMaav_extunder_co.paa"
		};
		displayName="M12 Warthog FAV (LAU65)";
		crew="TFV_ER_Hacker";
		class textureSources
		{
			class scorp
			{
				displayName="Tauri camo";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_Er_m12HogMaav_extupper_co.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_Er_m12HogMaav_extunder_co.paa"
				};
				materials[]={};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class TCF_Nightingale_base;
	class TFV_ER_Nightingale: TCF_Nightingale_base
	{
		scope=2;
		scopeCurator=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		crew="TFV_ER_Hacker";
		displayName="EV-41 Nightingale";
		class textureSources
		{
			class TCF_Tau_Night
			{
				author="Baseplate";
				displayName="ER";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_nightingale_co.paa"
				};
			};
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_nightingale_co.paa"
		};
		textureList[]=
		{
			"TCF_Tau_Night",
			1
		};
		tf_range=25000;
		tf_isolatedAmount=0.40000001;
		tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel;";
		tf_hasLRradio=1;
		enableRadio=1;
	};
	class OPTRE_Pelican_armed;
	class TFV_ER_WDL_PelicanAV: OPTRE_Pelican_armed
	{
		scope=2;
		scopeCurator=2;
		scopeEditor=2;
		side=0;
		displayName="D77H-TCI/AV Pelican";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_ER_WDL_PelicanAV.jpg";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		crew="TFV_ER_Unarmed";
		class textureSources
		{
			class TCF_ER
			{
				displayName="Innie";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Pelican.paa"
				};
			};
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Pelican.paa"
		};
		textureList[]=
		{
			"TCF_ER",
			1
		};
	};
	class OPTRE_Pelican_unarmed;
	class TFV_ER_WDL_Pelican: OPTRE_Pelican_unarmed
	{
		scope=2;
		scopeCurator=2;
		scopeEditor=2;
		side=0;
		displayName="D77H-TCI Pelican";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_ER_WDL_Pelican.jpg";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		crew="TFV_ER_Unarmed";
		class textureSources
		{
			class TCF_ER
			{
				displayName="Innie";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Pelican.paa"
				};
			};
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\TCF_ER_Pelican.paa"
		};
		textureList[]=
		{
			"TCF_ER",
			1
		};
	};
	class StaticWeapon;
	class StaticAAWeapon: StaticWeapon
	{
		class Turrets;
	};
	class OPTRE_LAU65D_pod: StaticAAWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class TFV_ER_LAU: OPTRE_LAU65D_pod
	{
		scope=2;
		scopeCurator=2;
		side=0;
		crew="TFV_ER_Unarmed";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Turrets_FF";
		displayName="LAU-65D/SGM-151";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_ER_LAU.jpg";
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				gunnerType="TCF_ER_Unarmed";
			};
		};
	};
	class O_T_LSV_02_unarmed_F;
	class TFV_unarmed_puma_02: O_T_LSV_02_unarmed_F
	{
		displayName="M7 Unarmed Puma";
		scope=2;
		scopeCurator=2;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_unarmed_puma_02.jpg";
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		side=0;
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"camo1",
			"Camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Puma_02.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_02_ghex_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_03_ghex_co.paa"
		};
		textureList[]=
		{
			"OPTRE_puma_02",
			1
		};
		class textureSources1
		{
			class OPTRE_puma_02
			{
				displayName="Eridanus 1";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Puma_02.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_02_ghex_co.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_03_ghex_co.paa"
				};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class O_T_LSV_02_armed_F;
	class TFV_armed_puma_02: O_T_LSV_02_armed_F
	{
		displayName="M7 MG Puma";
		scope=2;
		scopeCurator=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		side=0;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_armed_puma_02.jpg";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"camo1",
			"Camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Puma_02.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_02_ghex_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_03_ghex_co.paa"
		};
		textureList[]=
		{
			"OPTRE_pumaarm_02",
			1
		};
		class textureSources
		{
			class OPTRE_pumaarm_02
			{
				displayName="MLA camo";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Puma_02.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_02_ghex_co.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_03_ghex_co.paa"
				};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class O_T_LSV_02_AT_F;
	class TFV_AT_puma_02: O_T_LSV_02_AT_F
	{
		displayName="M7A1 AT Puma";
		scope=2;
		scopeCurator=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		side=0;
		crew="TFV_ER_Unarmed";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_AT_puma_02.jpg";
		hiddenSelections[]=
		{
			"camo1",
			"Camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Puma_02.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_02_ghex_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_03_ghex_co.paa"
		};
		textureList[]=
		{
			"OPTRE_pumaat_02",
			1
		};
		class textureSources
		{
			class OPTRE_pumaat_02
			{
				displayName="MLA camo";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Puma_02.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_02_ghex_co.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\csat_lsv_03_ghex_co.paa"
				};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class B_APC_Tracked_01_AA_F;
	class TFV_tarantula_02: B_APC_Tracked_01_AA_F
	{
		displayName="M705AA Tarantula";
		scope=2;
		scopeCurator=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AV_FF";
		side=0;
		crew="TFV_ER_Unarmed";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_tarantula_02.jpg";
		hiddenSelections[]=
		{
			"Camo1",
			"Camo2",
			"Camo3"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_tarantula_02.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_tarantulaother_02.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_tarantulaturret_02.paa"
		};
	};
	class B_APC_Wheeled_01_cannon_F;
	class TFV_ferret_02: B_APC_Wheeled_01_cannon_F
	{
		displayName="M620 Ferret";
		scope=2;
		scopeCurator=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AV_FF";
		side=0;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_ferret_02.jpg";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"camo1",
			"Camo2",
			"Camo3"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_Ferret_body2.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_Ferret_other2.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_Ferret_turret2"
		};
	};
	class I_MRAP_03_F;
	class TFV_salamander_02: I_MRAP_03_F
	{
		displayName="M201 Salamander";
		scope=2;
		scopeCurator=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		side=0;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_salamander_02.jpg";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"camo1"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_Salamander_02.paa"
		};
	};
	class I_MRAP_03_gmg_F;
	class TFV_salamanderagl_02: I_MRAP_03_gmg_F
	{
		displayName="M201AGL Salamander";
		scope=2;
		scopeCurator=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_Vehcles_FF";
		side=0;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_salamanderagl_02.jpg";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_Salamander_02.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\OPTRE_GHMG_02.paa"
		};
	};
	class B_Heli_Attack_01_dynamicLoadout_F;
	class TFV_raven_02: B_Heli_Attack_01_dynamicLoadout_F
	{
		displayName="RAH-94 Raven";
		scope=2;
		scopeCurator=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_raven_02.jpg";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"camo1",
			"Camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Raven_02.paa"
		};
	};
	class O_T_VTOL_02_vehicle_dynamicLoadout_F;
	class TFV_VTOLV_02: O_T_VTOL_02_vehicle_dynamicLoadout_F
	{
		displayName="AVD-99VT Wyvern";
		scope=2;
		scopeCurator=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_VTOLV_02.jpg";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2",
			"Camo_3",
			"Camo_4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_F.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_B.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa"
		};
		textureList[]=
		{
			"OPTRE_wyvern_02",
			1
		};
		class textureSources
		{
			class OPTRE_wyvern_02
			{
				displayName="MLA camo";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_F.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_B.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa"
				};
				materials[]={};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class O_T_VTOL_02_infantry_dynamicLoadout_F;
	class TFV_VTOLI_02: O_T_VTOL_02_infantry_dynamicLoadout_F
	{
		displayName="AVD-99IT Wyvern";
		scope=2;
		scopeCurator=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_VTOLI_02.jpg";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2",
			"Camo_3",
			"Camo_4"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_F.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_B.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa"
		};
		textureList[]=
		{
			"OPTRE_wyvern2_02",
			1
		};
		class textureSources
		{
			class OPTRE_wyvern2_02
			{
				displayName="MLA camo";
				author="Nightovizard & Baseplate";
				textures[]=
				{
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_F.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_B.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa",
					"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\VTOL_02_RL.paa"
				};
				materials[]={};
				factions[]=
				{
					"OPF_F",
					"OPF_G_F"
				};
			};
		};
	};
	class B_Plane_CAS_01_F;
	class TFV_gladius_02: B_Plane_CAS_01_F
	{
		displayName="A-84 Gladius (Ins)";
		scope=2;
		scopeCurator=2;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		side=0;
		editorPreview="\TCF_MISC\EditorPreviews\TCF_gladius_02.jpg";
		vehicleClass="OPTRE_UNSC_Air_class";
		crew="TFV_ER_Unarmed";
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Gladius_02_fuselage_01_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Gladius_02_fuselage_02_co.paa"
		};
	};
	class TFV_gladius_02L: B_Plane_CAS_01_F
	{
		displayName="A-84L Gladius (Ins)";
		scope=2;
		scopeCurator=2;
		side=0;
		faction="TFV_FactionClasses_URF";
		editorCategory="TFV_Editor_Category_FF";
		editorSubCategory="TFV_Editor_Subcategory_AC_FF";
		editorPreview="\TCF_MISC\EditorPreviews\TCF_gladius_02L.jpg";
		vehicleClass="OPTRE_UNSC_Air_class";
		crew="TFV_ER_Unarmed";
		weapons[]=
		{
			"OPTRE_M6_Laser",
			"Laserdesignator_pilotCamera",
			"CMFlareLauncher"
		};
		magazines[]=
		{
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"OPTRE_SpLaser_Battery",
			"Laserbatteries",
			"120Rnd_CMFlare_Chaff_Magazine"
		};
		hiddenSelections[]=
		{
			"Camo_1",
			"Camo_2"
		};
		hiddenSelectionsTextures[]=
		{
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Gladius_02_fuselage_01_co.paa",
			"TFV_Units\United_Rebel_Front\Freddom_Fighters\Vehicles\data\Gladius_02_fuselage_02_co.paa"
		};
	};
};
