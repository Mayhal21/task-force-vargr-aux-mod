class cfgPatches
{
    class TFV_Units_Patch
    {
        Name = "TFV Aux Mod - Units";
        Author = "Task Force Vargr Aux Team";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};
