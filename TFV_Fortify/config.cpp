#define MACRO_FORTIFY_ADD(CLASSNAME,COST,LIMIT) class _xx_##CLASSNAME { \
    name = #CLASSNAME; \
    cost = COST; \
    limit = LIMIT; \
}
class CfgPatches {
    class TFV_Fortify {
        author = "Task Force Vargr Aux Team";
        name = "TFV Fortify Settings";
        requiredAddons[] = {"OPTRE_Core"};
        weapons[] = {};
        units[] = {};
        skipWhenMissingDependencies = 1;
    };
};

class CfgVehicles 
{
    class Man;
    class CAManBase: Man 
    {
        class ACE_SelfActions 
        {
            class MTI_Fortify 
            {
                class Fob_Parts
                {
                    displayName = "FOB Parts";
                    condition = "true";
                    statement = "";
                    showDisabled = 0;
                    insertChildren = "[_this select 0,'Fob_Parts'] call mti_fortify_fnc_addActions"; // IMPORTANT: also replace My_Category with your own classname here!
                };
                class Crewserved 
                {
                    displayName = "Crewserved Weapons";
                    condition = "true";
                    statement = "";
                    showDisabled = 0;
                    insertChildren = "[_this select 0,'Crewserved'] call mti_fortify_fnc_addActions"; // IMPORTANT: also replace My_Category with your own classname here!
                };
                class Fortifications 
                {
                    displayName = "Fortifications";
                    condition = "true";
                    statement = "";
                    showDisabled = 0;
                    insertChildren = "[_this select 0,'Fortifications'] call mti_fortify_fnc_addActions"; // IMPORTANT: also replace My_Category with your own classname here!
                };
            };
        };
    };
};

class mti_fortify_presets 
{
    class Default;
    class Heavy_Turret_Preset: Default 
    {
        scope = 1;
        displayName = "Heavy Turrets";
        category = "Crewserved";
        class Objects 
        {
            MACRO_FORTIFY_ADD(SAG6_Static_MLMS,20,-1);
            MACRO_FORTIFY_ADD(OPTRE_Static_Gauss,20,-1);
            MACRO_FORTIFY_ADD(OPTRE_Static_M41,20,-1);
            MACRO_FORTIFY_ADD(OPTRE_Static_ATGM,20,-1);
            MACRO_FORTIFY_ADD(OPTRE_Static_AA,20,-1);
        };
    };
    class M247_Preset: Default 
    {
        scope = 1;
        displayName = "Turrets";
        category = "Crewserved";
        class Objects 
        {
            MACRO_FORTIFY_ADD(OPTRE_Static_M247H_Shielded_Tripod,20,-1);
        };
    };
    class LAU65D_Preset: Default 
    {
        scope = 1;
        displayName = "Turrets";
        category = "Crewserved";
        class Objects 
        {
            MACRO_FORTIFY_ADD(OPTRE_LAU65D_pod,20,-1);
        };
    };
    class AU44_Preset: Default 
    {
        scope = 1;
        displayName = "Turrets";
        category = "Crewserved";
        class Objects 
        {
            MACRO_FORTIFY_ADD(OPTRE_AU_44_Mortar_Standalone,20,-1);
        };
    };
    class Sandbag_Barriers_Preset: Default 
    {
        scope = 1;
        displayName = "Sandbags & Barriers";
        category = "Fortifications ";
        class Objects 
        {
            MACRO_FORTIFY_ADD(Land_BagFence_Corner_F,20,-1);
            MACRO_FORTIFY_ADD(Land_BagFence_End_F,20,-1);
            MACRO_FORTIFY_ADD(Land_BagFence_Long_F,20,-1);
            MACRO_FORTIFY_ADD(Land_BagFence_Short_F,20,-1);
            MACRO_FORTIFY_ADD(Land_OPTRE_M72_barrierBlk,20,-1);
            MACRO_FORTIFY_ADD(Land_OPTRE_M72S_barrierBlk,20,-1);
        };
    };
};