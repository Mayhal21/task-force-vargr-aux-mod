class CfgPatches
{
    class TFV_Patch_Ammo_Shotguns
    {
        addonRootClass = "TFV_Patch_Weapons";
        requiredAddons[] = {};
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
        ammo[] =
        {
            "TFV_12Gauge_Slug",
            "TFV_12Gauge_Buckshot",
            "TFV_12Gauge_HEAP",
            "TFV_12Gauge_DragonsBreath"
        };
    };
};
class CfgAmmo
{
    class BulletBase;
    class TFV_12Gauge_Slug: BulletBase
    {
        caliber=4;
        hit=75;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=450;
        airFriction=-0.001;
        deflecting=10;
        cartridge = "FxCartridge_slug";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.5;
        tracerStartTime=0.0;
        tracerEndTime=1.5;
        nvgOnly=0;
        audibleFire=20;
        visibleFire=4;
        initspeed=450;
        visibleFireTime=3;
        dangerRadiusBulletClose=5;
        suppressionRadiusBulletClose=3;
        dangerRadiusHit=10;
        suppressionRadiusHit=7;
        ACE_caliber=18.5;
        ACE_bulletLength=40;
        ACE_bulletMass=35;
        ACE_ammoTempMuzzleVelocityShifts[]={-3.0,-2.8,-2.6,-2.4,-2.0,-1.5,-8.0,-1.2,0.8,1.6,3.0};
        ACE_ballisticCoefficients[]={0.1};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.15;
        ACE_muzzleVelocities[]={400,420,440,460,480};
        ACE_barrelLengths[]={200,250,300,350,400,450};
    };

    class TFV_12Gauge_Buckshot: BulletBase
    {
        caliber=2;
        hit=18;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=420;
        airFriction=-0.0012;
        deflecting=12;
        cartridge = "FxCartridge_pellets";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.2;
        tracerStartTime=0.0;
        tracerEndTime=1.5;
        nvgOnly=0;
        audibleFire=20;
        visibleFire=4;
        initspeed=420;
        visibleFireTime=2;
        dangerRadiusBulletClose=5;
        suppressionRadiusBulletClose=3;
        dangerRadiusHit=10;
        suppressionRadiusHit=6;
        ACE_caliber=18.5;
        ACE_bulletLength=8;
        ACE_bulletMass=4.0;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.8,-2.6,-2.4,-2.2,-2.0,-1.6,-7.5,-1.0,0.6,1.4,2.5};
        ACE_ballisticCoefficients[]={0.06};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.2;
        ACE_muzzleVelocities[]={400,420,440,460,480};
        ACE_barrelLengths[]={200,250,300,350,400,450};
        simulation = "shotSpread";
        dispersion = 0.07;
    };

    /*class TFV_12Gauge_HEAP: BulletBase
    {
    caliber=5;
    hit=90;
    indirectHit=25;
    indirectHitRange=1;
    explosive=0.8;
    typicalSpeed=420;
    airFriction=-0.0008;
    deflecting=5;
    cartridge = "FxCartridge_slug";
    model="\A3\Weapons_f\Data\bullettracer\tracer_yellow";
    tracerScale=2;
    tracerStartTime=0.0;
    tracerEndTime=1.8;
    nvgOnly=0;
    audibleFire=25;
    visibleFire=5;
    initspeed=420;
    visibleFireTime=3;
    dangerRadiusBulletClose=6;
    suppressionRadiusBulletClose=4;
    dangerRadiusHit=15;
    suppressionRadiusHit=10;
    ACE_caliber=18.5;
    ACE_bulletLength=50;
    ACE_bulletMass=45;
    ACE_ammoTempMuzzleVelocityShifts[]={-3.0,-2.8,-2.6,-2.4,-2.0,-1.5,-8.0,-1.2,0.8,1.6,3.0};
    ACE_ballisticCoefficients[]={0.1};
    ACE_velocityBoundaries[]={};
    ACE_standardAtmosphere="ICAO";
    ACE_dragModel=1;
    ACE_muzzleVelocityVariationSD=0.15;
    ACE_muzzleVelocities[]={380,400,420,440,460};
    ACE_barrelLengths[]={200,250,300,350,400,450};
    };

    class TFV_12Gauge_DragonsBreath: BulletBase
    {
    caliber=1;
    hit=8;
    indirectHit=5;
    indirectHitRange=2;
    explosive=0.5;
    typicalSpeed=380;
    airFriction=-0.002;
    deflecting=10;
    cartridge = "FxCartridge_pellets";
    model="\A3\Weapons_f\Data\bullettracer\tracer_orange";
    tracerScale=1.8;
    tracerStartTime=0.0;
    tracerEndTime=2.5;
    nvgOnly=0;
    audibleFire=22;
    visibleFire=6;
    initspeed=380;
    visibleFireTime=4;
    dangerRadiusBulletClose=6;
    suppressionRadiusBulletClose=3;
    dangerRadiusHit=12;
    suppressionRadiusHit=8;
    ACE_caliber=18.5;
    ACE_bulletLength=35;
    ACE_bulletMass=20;
    ACE_ammoTempMuzzleVelocityShifts[]={-3.0,-2.8,-2.6,-2.4,-2.0,-1.5,-8.0,-1.2,0.8,1.6,3.0};
    ACE_ballisticCoefficients[]={0.1};
    ACE_velocityBoundaries[]={};
    ACE_standardAtmosphere="ICAO";
    ACE_dragModel=1;
    ACE_muzzleVelocityVariationSD=0.2;
    ACE_muzzleVelocities[]={350,370,390,410,430};
    ACE_barrelLengths[]={200,250,300,350,400,450};
    };*/
};