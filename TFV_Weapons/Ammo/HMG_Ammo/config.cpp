class CfgPatches
{
    class TFV_Patch_Ammo_HMG
    {
        addonRootClass = "TFV_Patch_Weapons";
        requiredAddons[] = {};
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
        ammo[] =
        {
            "TFV_HMG_AP_Ammo",
            "TFV_HMG_JHP_Ammo",
        };
    };
};
class CfgAmmo
{
    class BulletBase;
    class TFV_HMG_AP_Ammo: BulletBase
    {
        caliber=4.5;
        hit=100;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=930;
        airFriction=-0.0003;
        deflecting=2;
        cartridge="FxCartridge_50BMG";
        model="\A3\Weapons_f\Data\bullettracer\tracer_yellow";
        tracerScale=1.8;
        tracerStartTime=0.0;
        tracerEndTime=3.2;
        nvgOnly=0;
        audibleFire=40;
        visibleFire=7;
        initspeed=930;
        visibleFireTime=4;
        dangerRadiusBulletClose=8;
        suppressionRadiusBulletClose=6;
        dangerRadiusHit=20;
        suppressionRadiusHit=14;
        ACE_caliber=12.7;
        ACE_bulletLength=55;
        ACE_bulletMass=48;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.8,-2.6,-2.4,-2.2,-1.8,-1.4,-7.2,-1.0,0.7,1.5,2.6};
        ACE_ballisticCoefficients[]={1.2};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.08;
        ACE_muzzleVelocities[]={880,900,920,940,960,980,1000};
        ACE_barrelLengths[]={500,550,600,650,700,750};
    };

    class TFV_HMG_JHP_Ammo: BulletBase
    {
        caliber=4.2;
        hit=120;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=900;
        airFriction=-0.0004;
        deflecting=3;
        cartridge="FxCartridge_50BMG";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.8;
        tracerStartTime=0.0;
        tracerEndTime=3.2;
        nvgOnly=0;
        audibleFire=38;
        visibleFire=6;
        initspeed=900;
        visibleFireTime=4;
        dangerRadiusBulletClose=7;
        suppressionRadiusBulletClose=5;
        dangerRadiusHit=18;
        suppressionRadiusHit=12;
        ACE_caliber=12.7;
        ACE_bulletLength=52;
        ACE_bulletMass=50;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.8,-2.6,-2.4,-2.2,-1.8,-1.4,-7.2,-1.0,0.7,1.5,2.6};
        ACE_ballisticCoefficients[]={1.1};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.1;
        ACE_muzzleVelocities[]={860,880,900,920,940,960,980};
        ACE_barrelLengths[]={500,550,600,650,700,750};
    };
};