class CfgPatches
{
    class TFV_Patch_Ammo_Rifles
    {
        addonRootClass = "TFV_Patch_Weapons";
        requiredAddons[] = {};
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
        ammo[] =
        {
            "TFV_Rifle_AP_Ammo",
            "TFV_Rifle_JHP_Ammo",
            "TFV_DMR_AP_Ammo",
            "TFV_DMR_JHP_Ammo"
        };
    };
};
class CfgAmmo
{
    class BulletBase;
    class TFV_Rifle_AP_Ammo: BulletBase
    {
        caliber=2.5;
        hit=50;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=850;
        airFriction=-0.0005;
        deflecting=3;
        cartridge="FxCartridge_762";
        model="\A3\Weapons_f\Data\bullettracer\tracer_white";
        tracerScale=1.2;
        tracerStartTime=0.0;
        tracerEndTime=2.0;
        nvgOnly=0;
        audibleFire=30;
        visibleFire=5;
        initspeed=850;
        visibleFireTime=3;
        dangerRadiusBulletClose=6;
        suppressionRadiusBulletClose=4;
        dangerRadiusHit=15;
        suppressionRadiusHit=10;
        ACE_caliber=7.62;
        ACE_bulletLength=30;
        ACE_bulletMass=10;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.2,-2.1,-2.0,-1.8,-1.5,-1.1,-7.0,-1.0,0.5,1.3,2.2};
        ACE_ballisticCoefficients[]={0.9};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.08;
        ACE_muzzleVelocities[]={780,800,820,840,850,870,890};
        ACE_barrelLengths[]={400,450,500,550,600,700,1000};
    };
    class TFV_Rifle_JHP_Ammo: BulletBase
    {
        caliber=1.8;
        hit=60;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=800;
        airFriction=-0.0007;
        deflecting=5;
        cartridge="FxCartridge_762";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.2;
        tracerStartTime=0.0;
        tracerEndTime=2.0;
        nvgOnly=0;
        audibleFire=25;
        visibleFire=4;
        initspeed=800;
        visibleFireTime=3;
        dangerRadiusBulletClose=5;
        suppressionRadiusBulletClose=3;
        dangerRadiusHit=12;
        suppressionRadiusHit=8;
        ACE_caliber=7.62;
        ACE_bulletLength=28;
        ACE_bulletMass=11;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.2,-2.1,-2.0,-1.8,-1.5,-1.1,-7.0,-1.0,0.5,1.3,2.2};
        ACE_ballisticCoefficients[]={0.75};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.1;
        ACE_muzzleVelocities[]={760,780,800,820,830,850,870};
        ACE_barrelLengths[]={400,450,500,550,600,700,1000};
    };
    //DMR Ammo
    class TFV_DMR_AP_Ammo: BulletBase
    {
        caliber=3.5;
        hit=85;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=900;
        airFriction=-0.0004;
        deflecting=2;
        cartridge="FxCartridge_338";
        model="\A3\Weapons_f\Data\bullettracer\tracer_white";
        tracerScale=1.3;
        tracerStartTime=0.0;
        tracerEndTime=2.5;
        nvgOnly=0;
        audibleFire=35;
        visibleFire=6;
        initspeed=900;
        visibleFireTime=3;
        dangerRadiusBulletClose=7;
        suppressionRadiusBulletClose=5;
        dangerRadiusHit=18;
        suppressionRadiusHit=12;
        ACE_caliber=8.6;
        ACE_bulletLength=35;
        ACE_bulletMass=16;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.5,-2.3,-2.1,-1.9,-1.5,-1.0,-6.8,-0.9,0.7,1.4,2.5};
        ACE_ballisticCoefficients[]={1.1};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.07;
        ACE_muzzleVelocities[]={850,870,890,910,930,950,970};
        ACE_barrelLengths[]={500,550,600,650,700,750,800};
    };
    class TFV_DMR_JHP_Ammo: BulletBase
    {
        caliber=2.8;
        hit=100;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=870;
        airFriction=-0.0006;
        deflecting=4;
        cartridge="FxCartridge_338";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.3;
        tracerStartTime=0.0;
        tracerEndTime=2.5;
        nvgOnly=0;
        audibleFire=32;
        visibleFire=5;
        initspeed=870;
        visibleFireTime=3;
        dangerRadiusBulletClose=6;
        suppressionRadiusBulletClose=4;
        dangerRadiusHit=15;
        suppressionRadiusHit=10;
        ACE_caliber=8.6;
        ACE_bulletLength=32;
        ACE_bulletMass=18;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.5,-2.3,-2.1,-1.9,-1.5,-1.0,-6.8,-0.9,0.7,1.4,2.5};
        ACE_ballisticCoefficients[]={0.9};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.09;
        ACE_muzzleVelocities[]={820,840,860,880,900,920,940};
        ACE_barrelLengths[]={500,550,600,650,700,750,800};
    };

};