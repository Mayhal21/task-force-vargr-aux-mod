class CfgPatches
{
    class TFV_Patch_Ammo_Pistols
    {
        addonRootClass = "TFV_Patch_Weapons";
        requiredAddons[] = {};
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
        ammo[] =
        {
            "TFV_Pistol_AP_Ammo",
            "TFV_Pistol_JHP_Ammo",
        };
    };
};
class CfgAmmo
{
    class BulletBase;
    class TFV_Pistol_AP_Ammo: BulletBase
    {
        caliber=1.2;
        hit=25;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=450;
        airFriction=-0.0006;
        deflecting=5;
        cartridge="FxCartridge_9mm";
        model="\A3\Weapons_f\Data\bullettracer\tracer_white";
        tracerScale=1.0;
        tracerStartTime=0.0;
        tracerEndTime=1.5;
        nvgOnly=0;
        audibleFire=15;
        visibleFire=3;
        initspeed=450;
        visibleFireTime=2;
        dangerRadiusBulletClose=4;
        suppressionRadiusBulletClose=2;
        dangerRadiusHit=8;
        suppressionRadiusHit=5;
        ACE_caliber=9.0;
        ACE_bulletLength=19;
        ACE_bulletMass=8;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.0,-1.8,-1.6,-1.4,-1.2,-1.0,-5.5,-0.8,0.4,1.2,2.0};
        ACE_ballisticCoefficients[]={0.14};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.05;
        ACE_muzzleVelocities[]={400,420,440,460,480};
        ACE_barrelLengths[]={100,150,200,250,300};
    };
    class TFV_Pistol_JHP_Ammo: BulletBase
    {
        caliber=1.0;
        hit=30;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=420;
        airFriction=-0.0008;
        deflecting=7;
        cartridge="FxCartridge_9mm";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.0;
        tracerStartTime=0.0;
        tracerEndTime=1.5;
        nvgOnly=0;
        audibleFire=12;
        visibleFire=2;
        initspeed=420;
        visibleFireTime=2;
        dangerRadiusBulletClose=3;
        suppressionRadiusBulletClose=2;
        dangerRadiusHit=7;
        suppressionRadiusHit=4;
        ACE_caliber=9.0;
        ACE_bulletLength=18;
        ACE_bulletMass=9;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.0,-1.8,-1.6,-1.4,-1.2,-1.0,-5.5,-0.8,0.4,1.2,2.0};
        ACE_ballisticCoefficients[]={0.12};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.07;
        ACE_muzzleVelocities[]={380,400,420,440,460};
        ACE_barrelLengths[]={100,150,200,250,300};
    };
};