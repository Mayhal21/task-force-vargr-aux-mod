class CfgPatches
{
    class TFV_Patch_Ammo_LMG
    {
        addonRootClass = "TFV_Patch_Weapons";
        requiredAddons[] = {};
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
        ammo[] =
        {
            "TFV_LMG_AP_Ammo",
            "TFV_LMG_JHP_Ammo",
        };
    };
};
class CfgAmmo
{
    class BulletBase;
    class TFV_LMG_AP_Ammo: BulletBase
    {
        caliber=2.5;
        hit=45;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=850;
        airFriction=-0.0005;
        deflecting=4;
        cartridge="FxCartridge_762";
        model="\A3\Weapons_f\Data\bullettracer\tracer_white";
        tracerScale=1.3;
        tracerStartTime=0.0;
        tracerEndTime=2.5;
        nvgOnly=0;
        audibleFire=30;
        visibleFire=6;
        initspeed=850;
        visibleFireTime=3;
        dangerRadiusBulletClose=6;
        suppressionRadiusBulletClose=4;
        dangerRadiusHit=15;
        suppressionRadiusHit=10;
        ACE_caliber=7.62;
        ACE_bulletLength=30;
        ACE_bulletMass=9.5;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.2,-2.1,-2.0,-1.8,-1.5,-1.1,-7.0,-1.0,0.5,1.3,2.2};
        ACE_ballisticCoefficients[]={0.85};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.1;
        ACE_muzzleVelocities[]={800,820,840,860,880};
        ACE_barrelLengths[]={400,450,500,550,600,700};
    };
    class TFV_LMG_JHP_Ammo: BulletBase
    {
        caliber=2.0;
        hit=55;
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=820;
        airFriction=-0.0007;
        deflecting=5;
        cartridge="FxCartridge_762";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.3;
        tracerStartTime=0.0;
        tracerEndTime=2.5;
        nvgOnly=0;
        audibleFire=28;
        visibleFire=5;
        initspeed=820;
        visibleFireTime=3;
        dangerRadiusBulletClose=5;
        suppressionRadiusBulletClose=3;
        dangerRadiusHit=12;
        suppressionRadiusHit=8;
        ACE_caliber=7.62;
        ACE_bulletLength=28;
        ACE_bulletMass=10;
        ACE_ammoTempMuzzleVelocityShifts[]={-2.2,-2.1,-2.0,-1.8,-1.5,-1.1,-7.0,-1.0,0.5,1.3,2.2};
        ACE_ballisticCoefficients[]={0.78};
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=1;
        ACE_muzzleVelocityVariationSD=0.12;
        ACE_muzzleVelocities[]={780,800,820,840,860};
        ACE_barrelLengths[]={400,450,500,550,600,700};
    };
};