class CfgPatches
{
    class TFV_Patch_Ammo_SRS
    {
        addonRootClass = "TFV_Patch_Weapons";
        requiredAddons[] = {};
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
        ammo[] =
        {
            "TFV_Sniper_Ammo",
            "TFV_Sniper_Ammo_HEAP"
        };
    };
};
class CfgAmmo
{
    class BulletBase;
    class TFV_Sniper_Ammo: BulletBase
    {
        caliber=20; // Increased caliber for better penetration
        hit=120; // Higher hit value for true APFSDS impact
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=1700; // Higher velocity for APFSDS performance
        airFriction=-0.00005; // Reduced drag for better long-range performance
        deflecting=0;
        cartridge = "FxCartridge_127";
        model="\A3\Weapons_f\Data\bullettracer\tracer_white"; // APFSDS rounds usually have minimal tracers
        tracerScale=2;
        tracerStartTime=0.0;
        tracerEndTime=3;
        nvgOnly=0;
        audibleFire=50; // APFSDS rounds are loud
        visibleFire=2;
        initspeed=1700;
        visibleFireTime=2;
        dangerRadiusBulletClose=10;
        suppressionRadiusBulletClose=6;
        dangerRadiusHit=25;
        suppressionRadiusHit=18;
        ACE_caliber=14.5;
        ACE_bulletLength=55;
        ACE_bulletMass=60; // Slightly adjusted for APFSDS characteristics
        ACE_ammoTempMuzzleVelocityShifts[]={-2.4,-2.3,-2.1,-2.0,-1.6,-1.2,-7.5,-1.2,0.6,1.5,2.5};
        ACE_ballisticCoefficients[]={1.2}; // Higher coefficient for better aerodynamic efficiency
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=7; // APFSDS rounds typically use drag model 7
        ACE_muzzleVelocityVariationSD=0.05; // Less variation in muzzle velocity
        ACE_muzzleVelocities[]={1500,1550,1600,1650,1700,1750,1800}; // Higher velocities for APFSDS
        ACE_barrelLengths[]={400,450,500,550,600,700,1000};
    };
    class TFV_Sniper_Ammo_HEAP: BulletBase
    {
        caliber=15; // Lower than APFSDS but still effective
        hit=100; // Balanced between penetration and explosive power
        indirectHit=20; // Added explosive effect
        indirectHitRange=1.5; // Small explosion radius
        typicalSpeed=1100; // Lower speed than APFSDS due to explosive filler
        airFriction=-0.0002; // Slightly higher drag than APFSDS
        deflecting=10; // More prone to deflection
        cartridge = "FxCartridge_127";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red"; // HEAP rounds have distinct tracers
        tracerScale=2.5;
        tracerStartTime=0.0;
        tracerEndTime=4;
        nvgOnly=0;
        audibleFire=45;
        visibleFire=4;
        initspeed=1100;
        visibleFireTime=4;
        dangerRadiusBulletClose=15;
        suppressionRadiusBulletClose=10;
        dangerRadiusHit=30;
        suppressionRadiusHit=25;
        ACE_caliber=14.5;
        ACE_bulletLength=50;
        ACE_bulletMass=75; // Heavier due to explosive filler
        ACE_ammoTempMuzzleVelocityShifts[]={-2.3,-2.2,-2.0,-1.8,-1.5,-1.1,-7.0,-1.0,0.5,1.3,2.2};
        ACE_ballisticCoefficients[]={0.8}; // Less aerodynamic efficiency than APFSDS
        ACE_velocityBoundaries[]={};
        ACE_standardAtmosphere="ICAO";
        ACE_dragModel=5; // HEAP rounds use a different drag model
        ACE_muzzleVelocityVariationSD=0.08;
        ACE_muzzleVelocities[]={1000,1050,1080,1100,1120,1150,1200};
        ACE_barrelLengths[]={400,450,500,550,600,700,1000};
    };
};