class CfgPatches
{
    class TFV_Patch_Ammo_SMG
    {
        addonRootClass = "TFV_Patch_Weapons";
        requiredAddons[] = {};
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
        ammo[] =
        {
            "TFV_SMG_AP_Ammo",
            "TFV_SMG_JHP_Ammo"
        };
    };
};
class CfgAmmo
{
    class BulletBase;
    class TFV_SMG_AP_Ammo: BulletBase
    {
        caliber=1.2; // Increased for penetration
        hit=12; // Higher damage for AP rounds
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=320;
        airFriction=-0.0003; // Less drag for better penetration
        deflecting=5;
        cartridge="FxCartridge_45ACP";
        model="\A3\Weapons_f\Data\bullettracer\tracer_white";
        tracerScale=1.5;
        tracerStartTime=0;
        tracerEndTime=1.5;
        nvgOnly=0;
        audibleFire=15;
        visibleFire=3;
        initspeed=320;
        visibleFireTime=2;
        dangerRadiusBulletClose=4;
        suppressionRadiusBulletClose=2;
        dangerRadiusHit=10;
        suppressionRadiusHit=6;
    };
    class TFV_SMG_JHP_Ammo: BulletBase
    {
        caliber=1.0; // Standard but optimized for soft targets
        hit=14; // Higher soft target damage
        indirectHit=0;
        indirectHitRange=0;
        typicalSpeed=280;
        airFriction=-0.0005; // More drag for reduced penetration
        deflecting=8;
        cartridge="FxCartridge_45ACP";
        model="\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale=1.5;
        tracerStartTime=0;
        tracerEndTime=1.5;
        nvgOnly=0;
        audibleFire=12;
        visibleFire=2;
        initspeed=280;
        visibleFireTime=2;
        dangerRadiusBulletClose=3;
        suppressionRadiusBulletClose=1;
        dangerRadiusHit=8;
        suppressionRadiusHit=5;
    };
};