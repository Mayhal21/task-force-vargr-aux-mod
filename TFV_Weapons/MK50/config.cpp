class CfgPatches
{
	class TFV_Weapons
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_MK50",
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{

	class arifle_Mk20_F;
	class Pistol_Base_F;
	class hgun_Pistol_heavy_01_F;
	class WeaponSlotsInfo;
	class UGL_F;
    class TFV_MK50: hgun_Pistol_heavy_01_F
	{
		muzzleEnd="konec_hlavne";
		muzzlePos="usti_hlavne";
		scope=2;
		scopeArsenal=2;
		ace_overheating_mrbs=3000000000;
		ACE_Overheating_SlowdownFactor=0;
		ACE_Overheating_JamChance=0;
		ACE_Overheating_Dispersion=0;
		ACE_overheating_allowSwapBarrel=0;
		author="Task Force Vargr Aux Team";
		model="MA_Weapons\data\Sidekick.p3d";
		displayName="[TFV] Mk50";
		descriptionShort="Mk50 handgun";
		baseWeapon="TFV_MK50";
		cursor="Pistol_Crosshair";
		recoil="MA_recoil_Sidekick";
		ace_clearJamAction="GestureReloadCTAR";
		reloadAction="GestureReloadPistol";
		reloadMagazineSound[]=
		{
			"A3\Sounds_F\arsenal\weapons\Pistols\4-Five\reload_4_five",
			1.5,
			1,
			100
		};
		magazineWell[]=
		{
			"TFV_UNSC_MK50_Magwell"
		};
		magazines[]=
        {
			"TFV_MK50_Mag",
			"TFV_Mk50_Mag_Tracer"            
        };
		modes[]=
		{
			"Single"
		};
		class Single: Mode_SemiAuto
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"MA_Weapons\data\Sidekick\Sidekick_Fire.ogg",
					1.5,
					1,
					2000
				};
				begin2[]=
				{
					"MA_Weapons\data\Sidekick\Sidekick_Fire.ogg",
					1.5,
					1.015,
					2000
				};
				begin3[]=
				{
					"MA_Weapons\data\Sidekick\Sidekick_Fire.ogg",
					1.5,
					0.985,
					2000
				};
				begin4[]=
				{
					"MA_Weapons\data\Sidekick\Sidekick_Fire.ogg",
					1.5,
					1.01,
					2000
				};
				begin5[]=
				{
					"MA_Weapons\data\Sidekick\Sidekick_Fire.ogg",
					1.5,
					0.995,
					2000
				};
				soundBegin[]=
				{
					"begin1",
					0.20,
					"begin2",
					0.20,
					"begin3",
					0.20,
					"begin4",
					0.20,
					"begin5",
					0.20,
				};
				beginwater1[]=
				{
					"MA_Weapons\data\Sidekick\Sidekick_Fire.ogg",
					1,
					1,
					400
				};
				soundBeginWater[]=
				{
					"beginwater1",
					1,
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
					0.79432821,
					1,
					400
				};
				begin2[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
					0.79432821,
					1,
					400
				};
				begin3[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
					0.79432821,
					1,
					400
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin1",
					0.34
				};
			};
			reloadTime=0.075000003;
			dispersion=0.00075000001;
			minRange=2;
			minRangeProbab=0.30000001;
			midRange=300;
			midRangeProbab=0.69999999;
			maxRange=600;
			maxRangeProbab=0.050000001;
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass=10;
			class MuzzleSlot: MuzzleSlot
			{
				ccompatibleitems[]={};
			};
			class CowsSlot: CowsSlot
			{
				compatibleitems[]={};
				iconPosition[]={0.60000002,0.27000001};
				iconScale=0.15000001;
			};
			class PointerSlot: PointerSlot
			{
				compatibleitems[]={};
			};
			class UnderBarrelSlot: UnderBarrelSlot
			{
				compatibleitems[]={};
			};
		};
	};
};
class cfgMagazineWells
{
	class TFV_UNSC_MK50_Magwell 
	{
		UNSC_127x40_12rnd[] = 
		{
			"TFV_MK50_Mag",
			"TFV_Mk50_Mag_Tracer"
		};
	};
};

class CfgMagazines
{
    class CA_Magazine;
	class TFV_MK50_Mag: CA_Magazine
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Team";
		displayName="MK50 Magazine";
		count=24;
		displaynameshort="MK50 Magazine";
		descriptionshort="MK50 Magazine";
		mass=7;
		ammo="TFV_MK50_Base";
		tracersEvery=0;
	};
    class TFV_MK50_Mag_Tracer: TFV_MK50_Mag
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Team";
		displayName="MK50 Magazine (Tracer)";
		count=12;
		displaynameshort="MK50 Magazine (Tracer)";
		descriptionshort="MK50 Magazine (Tracer)";
		mass=7;
		ammo="TFV_MK50_Base";
		tracersEvery=1;
	};
};
class CfgAmmo
{
    class BulletBase;
    class TFV_MK50_Base: BulletBase
	{
		caliber=4;
		hit=15;
		indirectHit=0;
		indirectHitRange=1;
		typicalSpeed=1000;
		airFriction=-0.0005;
		deflecting=0;
		cartridge="FxCartridge_762";
		model="\A3\Weapons_f\Data\bullettracer\tracer_yellow";
		tracerScale=.8;
		tracerStartTime=0.0;
		tracerEndTime=8;
		nvgOnly=0;
		audibleFire=40;
		visibleFire=3;
		initspeed=1000;
		visibleFireTime=3;
		dangerRadiusBulletClose=8;
		suppressionRadiusBulletClose=6;
		dangerRadiusHit=12;
		suppressionRadiusHit=8;
		ACE_caliber=7.62;
		ACE_bulletLength=30;
		ACE_bulletMass=12.6;
		ACE_ammoTempMuzzleVelocityShifts[]={-2.55,-2.47,-2.25,-2.1199999,-1.6799999,-1.28,-7.6399999,-1.3,0.58999997,1.51,2.6099999};
		ACE_ballisticCoefficients[]={0.43};
		ACE_velocityBoundaries[]={};
		ACE_standardAtmosphere="ICAO";
		ACE_dragModel=1;
		ACE_muzzleVelocityVariationSD=0.1;
		ACE_muzzleVelocities[]={935,977,998,1021,1040,1055,1350};
		ACE_barrelLengths[]={400,450,500,550,600,700,800};
	};
};
class cfgMods
{
	author="Task Force Vargr Aux Team";
	timepacked="";
};
