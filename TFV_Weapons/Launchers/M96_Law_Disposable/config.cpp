#define _ARMA_

class CfgPatches
{
	class TFV_Weapons_LAW
	{
		addonRootClass="TFV_Weapons";
		requiredAddons[]=
		{
			"TFV_Weapons",
			"A3_Weapons_F",
			"A3_Weapons_F_Exp",
			"cba_main",
			"Extended_EventHandlers",
			"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_LAW",
			"ACE_TFV_LAW_ready_F",
			"ACE_TFV_LAW_used_F",						
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class CBA_DisposableLaunchers
{
	ACE_TFV_LAW_ready_F[] = {"TFV_LAW","ACE_TFV_LAW_used_F"};
};
class SlotInfo;
class MuzzleSlot;
class PointerSlot;
class CfgWeapons
{
	class Launcher;
	class Launcher_Base_F:Launcher
	{
		class WeaponSlotsInfo;
	};
	class TFV_LAW:Launcher_Base_F
	{
		author = "Task Force Vargr Aux Team";
		scope = 2;
		scopeArsenal = 2;
		baseWeapon = "TFV_LAW";
		displayName = "[TFV] M96 LAW (Disposable)";
		model = "DMNS\Weapons\Launchers\DMNS_M96_LAW_loaded.p3d";
		picture = "\DMNS\Weapons\Launchers\Data\Law_Icon.paa";
		UiPicture = "\DMNS\Weapons\Launchers\Data\Law_Icon.paa";
		handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_Exp\Launchers\RPG7\Data\Anim\RPG7V.rtm"};
		reloadAction = "GestureReloadRPG7";
		recoil = "recoil_rpg";
		manualControl = 0;
		maxZeroing = 500;
		shotPos = "muzzlePos2";
		shotEnd = "muzzleEnd2";
		modes[] = {"Single"};
		class Single: Mode_SemiAuto
		{
			sounds[] = {"StandardSound"};
			class BaseSoundModeType{};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\nlaw",1.9952624,1,1800};
				soundBegin[] = {"begin1",1};
			};
			recoil = "recoil_rpg";
			aiRateOfFire = 7;
			aiRateOfFireDistance = 500;
			minRange = 25;
			minRangeProbab = 0.8;
			midRange = 50;
			midRangeProbab = 0.8;
			maxRange = 790;
			maxRangeProbab = 0.8;
		};
		class GunParticles
		{
			class effect1
			{
				positionName = "muzzleEnd2";
				directionName = "muzzlePos2";
				effectName = "RocketBackEffectsNLAWNT";
			};
		};
		drySound[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\Dry_NLAW",0.17782794,1,15};
		reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\Reload_NLAW",1,1,10};
		lockingTargetSound[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\locking_NLAW",0.31622776,1};
		lockedTargetSound[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\locked_NLAW",0.31622776,2.5};
		changeFiremodeSound[] = {"A3\sounds_f\dummysound",0.56234133,1,20};
		modelOptics="\A3\Weapons_F_Beta\acc\reticle_titan.p3d";
		class OpticsModes
		{
			class StepScope
			{
				opticsID=1;
				useModelOptics=1;
				opticsPPEffects[]=
				{
					"OpticsCHAbera1",
					"OpticsBlur1"
				};
				opticsFlare=0;
				opticsZoomInit="0.25/1";
				opticsZoomMax="0.25/1";
				opticsZoomMin="0.25/6";
				distanceZoomMin=300;
				distanceZoomMax=300;
				memoryPointCamera="eye";
				cameraDir="look";
				visionMode[]=
				{
					"Normal",
					"NVG",
					"Ti"
				};
				thermalMode[]={0};
				opticsDisablePeripherialVision=1;
				discretefov[]=
				{
					"0.25/1",
					"0.25/3",
					"0.25/6"
				};
				discreteInitIndex=0;
			};
		cameraDir = "look";
		value = 20;
		weaponInfoType = "RscWeaponZeroing";
		canLock = 2;
		ballisticsComputer = 1;
		allowTabLock = 1;
		lockAcquire = 1;
		weaponLockSystem = 12;
		cmImmunity = 0.1;
		magazines[]=
		{
			"TFV_M96_AT_Ammo"
		};
		magazineWell[] = {""};
		magazineReloadTime = 0.1;
		inertia = 1;
		aimTransitionSpeed = 0.5;
		dexterity = 1;
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 145;
		};
		class ItemInfo
		{
			priority = 3;
		};
	};
	};
		class ACE_TFV_LAW_ready_F:TFV_LAW
	{
		author = "Task Force Vargr Aux Team";
		scope = 1;
		scopeArsenal = 1;
		baseWeapon = "TFV_LAW";
		magazines[]=
		{
			"TFV_M96_AT_Ammo",
		};
		class EventHandlers
		{
			fired = "_this call CBA_fnc_firedDisposable";
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 100;
		};
	};
	class ACE_TFV_LAW_used_F: TFV_LAW 
	{
		author = "Task Force Vargr Aux Team";
		scope = 1;
		scopeArsenal = 1;
		baseWeapon = "TFV_LAW";
		model = "DMNS\Weapons\Launchers\DMNS_M96_LAW_Used.p3d";
		displayName = "[TFV] M96 (Used)";
		magazines[] = {"CBA_FakeLauncherMagazine"};
		class WeaponSlotsInfo: WeaponSlotsInfo
			{
			mass = 100;
			};
		};
};
class CfgMagazines
{
	class Titan_AT;
	class TFV_M96_AT_Ammo: Titan_AT
	{
		scope=2;
		scopearsenal=2;
		displayName="[TFV] M96 AT Rocket";
		author="Task Force Vargr Aux Team";
		ammo="TFV_M96_AT_Rocket";
		picture="\a3\Weapons_F_Tank\Launchers\MRAWS\Data\UI\icon_rocket_MRAWS_HEAT_F_ca.paa";
		model="\a3\Weapons_F_Tank\Launchers\MRAWS\rocket_MRAWS_HEAT_F_item.p3d";
		type="2 *   256";
		count=1;
		initSpeed=60;
		maxLeadSpeed=60;
		mass=70;
	};
};
class CfgAmmo
{
	class M_Titan_AT;
	class TFV_M96_AT_Rocket: M_Titan_AT
	{
		aiAmmoUsageFlags="128 + 256 +512";
		hit=500;
		thrust=130;
		thrustTime=3.5;
		timeToLive=20;
		indirectHit=20;
		indirectHitRange=2;
		lockType = 2;
    	manualControl = 1;
		maxControlRange = 2000;
	};
};