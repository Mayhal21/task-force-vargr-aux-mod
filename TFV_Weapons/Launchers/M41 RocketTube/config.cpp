class CfgPatches
{
	class TFV_Weapons_M41
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
            "TFV_M41",
			"TFV_M41_SSR_G",
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SensorTemplateIR;
class CfgWeapons
{
	class OPTRE_UnguidedLauncher_Base;
	class OPTRE_GuidedATLauncher_Base;
	class OPTRE_GuidedAALauncher_Base;
	class TFV_M41: OPTRE_UnguidedLauncher_Base
	{
		recoil = ""; 
		author = "Task Force Vargr Aux Team";
        scope  = 2;
        scopeArsenal = 2;
		displayname = "[TFV] M41 Spanker";
		descriptionshort = "Surface-to-Surface Rocket Medium Anti-Vehicle/Assault Weapon";
		pictureMjolnirHud = "\OPTRE_Suit_Scripts\textures\weaponIcons\ExplosiveWeapons\Launcher_icon.paa";
		magazines[] = 
		{
			"TFV_M19_HEAT_SACLOS",			
		};
        magazineWell[] 	= 
		{
			"TFV_M41"
		};
		picture="\OPTRE_weapons\rockets\icons\launcher.paa";
		model="\OPTRE_Weapons\Rockets\M41_launcher_loaded.p3d";
		handAnim[] =
		{
			"OFP2_ManSkeleton",
			"\OPTRE_Weapons\Rockets\Data\Anim\m41_hand_anim.rtm",
			"Spartan_ManSkeleton",
			"\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\m41_hand_anim_Spartan.rtm"
		};
		modelOptics[] 	= 
		{
			"\OPTRE_Weapons\Rockets\M41_Optic_2x.p3d",
			"\OPTRE_Weapons\Rockets\M41_Optic_4x.p3d",
			"\OPTRE_Weapons\Rockets\M41_Optic_10x.p3d"
		};
		baseWeapon="TFV_M41";

		cursor="OPTRE_M41R";
		pictureWire="\OPTRE_Weapons\data\Pictures\WireWeaponIcons\Launchers\Double.paa";
		ODST_1="OPTRE_ODST_HUD_AmmoCount_RL";
		Glasses="OPTRE_GLASS_HUD_AmmoCount_RL";
		Eye="OPTRE_EYE_HUD_AmmoCount_RL";
		HUD_BulletInARows=2;
		HUD_TotalPosibleBullet=2;
		hiddenSelections[]= {
			"camo",
			"camo_tubes",
			"camo_details"
		};
		hiddenSelectionsTextures[] = {
			"optre_weapons\rockets\data\launcher_co.paa",
			"optre_weapons\rockets\data\tubes_co.paa",
			"optre_weapons\rockets\data\logos_ca.paa"
		};
		class WeaponSlotsInfo
		{
			mass=125;
		};
		class GunParticles
		{
			class effect1
			{
				positionName="muzzleEnd2";
				directionName="muzzlePos2";
				effectName="RocketBackEffectsNLAWNT";
			};
		};
		class OpticsModes
		{
			class StepScope
			{
				opticsID=1;
				useModelOptics=1;
				opticsPPEffects[]=
				{
					"OpticsCHAbera1",
					"OpticsBlur1"
				};
				opticsFlare=0;
				opticsZoomMin=0.125;
				opticsZoomMax=0.042;
				opticsZoomInit=0.125;
				distanceZoomMin=300;
				distanceZoomMax=300;
				memoryPointCamera= "eye";
				cameraDir="look";
				visionMode[]=
				{
					"Normal",
					"NVG",
					"Ti"
				};
				thermalMode[]={0,1};
				opticsDisablePeripherialVision=1;
				discretefov[]={0.125,0.0525,0.042};
				discreteInitIndex=0;
			};
		};
		modes[]=
		{
			"Direct", 
			"TopDown"
		};
		class Direct: Mode_SemiAuto
		{
			displayName="Direct seek";
			sounds[]=
			{
				"StandardSound"
			};
			class BaseSoundModeType
			{
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"OPTRE_Weapons\Rockets\data\sounds\rocket_1.wss",
					2.5,
					1,
					1500
				};
				soundBegin[]=
				{
					"begin1",
					1
				};
			};
			recoil="recoil_single_law";
			reloadtime=1;
			aiRateOfFire=7;
			aiRateOfFireDistance=600;
			minRange=10;
			minRangeProbab=0.30000001;
			midRange=400;
			midRangeProbab=0.80000001;
			maxRange=600;
			maxRangeProbab=0.1;
		};
		class TopDown: Direct
		{
			displayName="Top-down seek";
			textureType="topDown";
			ace_missileGuidance_attackProfile="JAV_TOP";
		};
		canLock=2;
		weaponLockDelay=2;
		weaponLockSystem="2 + 16";
		cmImmunity=0.25;
		lockAcquire=1;
		lockingTargetSound[]=
		{
			"A3\Sounds_F\arsenal\weapons\Launchers\Titan\locking_Titan",
			0.31622776,
			1
		};
		lockedTargetSound[]=
		{
			"A3\Sounds_F\arsenal\weapons\Launchers\Titan\locked_Titan",
			0.31622776,
			2.5
		};
	};
	class TFV_M41_SSR_G: TFV_M41
	{
		scope=2;
		dlc="OPTRE";
		author="Article 2 Studios";
		displayname="[TFV] M41 Spanker (Guided)";
		descriptionshort="Surface-to-Surface Rocket Medium Anti-Vehicle/Assault Weapon (Guided)"; 
	};
};
class cfgMagazineWells
{
	class TFV_M41 
    {
        TFV_Mag[] = 
        {
			"TFV_M19_HEAT",
			"TFV_M19_HEAT_SACLOS",
        };
    };
};
class CfgMagazines
{
	class RPG32_HE_F;
	class TFV_M19_HEAT_SACLOS: RPG32_HE_F
	{
		dlc="OPTRE";
		displayname="[TFV] M19 HEAT Rocket (SACLOS)";
		displaynameshort="SACLOS";
		descriptionshort="High Explosive Anti Tank<br/>Semi-automatic command to line-of-sight (SACLOS)";
		ammo="TFV_M19_Rocket_Guided";
		picture="\OPTRE_Weapons\Rockets\icons\magazine\opaex\heat_saclos.paa";
		model="\OPTRE_Weapons\Rockets\M41_tube.p3d";
		modelSpecial="\OPTRE_Weapons\Rockets\M41_tube.p3d";
		modelSpecialIsProxy=1;
		hiddenSelections[]=
		{
			"camo_tubes",
			"camo_details"
		};
		hiddenSelectionsTextures[]=
		{
			"\OPTRE_Weapons\Rockets\data\mag_types\heat.paa",
			"\optre_weapons\rockets\data\logos_ca.paa"
		};
		count=2;
		mass=100;
		initSpeed=250;
		allowedSlots[]={901,701};
		maxLeadSpeed=270;
	};
};
class SensorTemplatePassiveRadar;
class SensorTemplateAntiRadiation;
class SensorTemplateActiveRadar;
class SensorTemplateVisual;
class SensorTemplateMan;
class SensorTemplateLaser;
class SensorTemplateNV;
class SensorTemplateDataLink;
class CfgAmmo
{
	class M_NLAW_AT_F;
	class M_Titan_AT;
	class M_Titan_AA;
	class TFV_M19_Rocket_Guided: M_Titan_AT
	{
		warheadName="TandemHEAT";
		submunitionAmmo="ammo_Penetrator_Vorona";
		effectsMissile="missile3";
		hit=900;
		indirectHit=25;
		proximityExplosionDistance=0;
		indirectHitRange=4;
		explosive=0.80000001;
		irLock=1;
		airLock=1;
		laserLock=0;
		nvLock=0;
		cmImmunity=0.34999999;
		simulationStep=0.0020000001;
		airFriction=0.064999998;
		sideAirFriction=0.30000001;
		manualControl=1;
		maneuvrability=24;
		maxControlRange=10000;
		missileKeepLockedCone=75;
		missileLockCone=10;
		missileLockMaxDistance=5000;
		missileLockMinDistance=50;
		missileLockMaxSpeed=111;
		trackOversteer=0.89999998;
		trackLead=0.80000001;
		weaponLockSystem=2;
		initTime=0;
		thrustTime=6;
		thrust=70;
		maxSpeed=210;
		class Components
		{
			class SensorsManagerComponent
			{
				class Components
				{
					class IRSensorComponent: SensorTemplateIR
					{
						class AirTarget
						{
							minRange=500;
							maxRange=4000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						class GroundTarget
						{
							minRange=500;
							maxRange=4000;
							objectDistanceLimitCoef=1;
							viewDistanceLimitCoef=1;
						};
						maxTrackableSpeed=100;
						angleRangeHorizontal=7;
						angleRangeVertical=4.5;
						maxTrackableATL=500;
					};
				};
			};
		};
		class ace_missileguidance
		{
			enabled=0;
			minDeflection=0.00025000001;
			maxDeflection=0.001;
			incDeflection=0.00050000002;
			canVanillaLock=1;
			defaultSeekerType="SACLOS";
			seekerTypes[]=
			{
				"SALH",
				"SACLOS"
			};
			defaultSeekerLockMode="LOAL";
			seekerLockModes[]=
			{
				"LOAL",
				"LOBL"
			};
			seekerAngle=90;
			seekerAccuracy=1;
			seekerMinRange=1;
			seekerMaxRange=2500;
			defaultAttackProfile="LIN";
			attackProfiles[]=
			{
				"LIN",
				"DIR"
			};
		};
	};
};
class cfgMods
{
	author="Task Force Vargr Aux Team";
	timepacked="";
};
