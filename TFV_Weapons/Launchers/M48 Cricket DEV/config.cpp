class CfgPatches
{
	class TFV_Cricket_dev
	{
		addonRootClass="TFV_Weapons";
		requiredAddons[]=
		{
			"OPTRE_Core",         
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_Cricket_dev",
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{
	class Launcher_Base_F;
	class TFV_launcher_base: Launcher_Base_F
	{
		class Single;
	};
	class TFV_Cricket_dev: TFV_launcher_base
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		scopearsenal=2;
		displayName="[TFV] M48 Cricket(DEV)";
		baseWeapon="TFV_Cricket_dev";
		picture="\TCF_WEAPONRY\Weapons\Icons\Pilum_icon.paa"; 
		model="\TCF_WEAPONRY\Weapons\Launchers\M48\M48B_Cricket.p3d";
		weaponInfoType="RscOpticsRangeFinderMRAWS";
		nameSound="aalauncher";
		handAnim[]= {"OFP2_ManSkeleton","\OPTRE_Weapons\Rockets\Data\Anim\m41_hand_anim.rtm"};
		reloadAction="ReloadRPG";
		recoil="recoil_rpg";
		maxZeroing=600;
		magazines[]=
		{
			"TFV_M48_HEAT_MAG",
			"TFV_M48_PEN_R_MAG",
			"TFV_M48_THERMO_MAG"
		};
		magazineWell[]={};
		class GunParticles
		{
			class effect1
			{
				positionName = "konec hlavne";
				directionName = "usti hlavne";
				effectName = "RocketBackEffectsRPGNT";
			};
		};
		class Single: Single
		{
			sounds[]=
			{
				"StandardSound"
			};
			class BaseSoundModeType
			{
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\nlaw",
					1.99526,
					1,
					1800
				};
				soundBegin[]=
				{
					"begin1",
					1
				};
				soundSetShot[]=
				{
					"Launcher_RPG7_Shot_SoundSet",
					"Launcher_RPG7_Tail_SoundSet"
				};
			};
		};
		modelOptics = "\a3\Weapons_F_Tank\acc\reticle_MRAWSNew.p3d";
		class OpticsModes
		{
			class optic
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsZoomMin = 0.0875;
				opticsZoomMax = 0.0875;
				opticsZoomInit = 0.0875;
				distanceZoomMin = 300;
				distanceZoomMax = 300;
				memoryPointCamera = "eye";
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				cameraDir = "look";
				visionMode[] = 
				{
					"Normal",
					"NVG",
					"Ti"
				};
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
			};

		};
	};
};
class CfgMagazines
{
	class TFV_M48_HEAT_MAG
	{
		scope=2;
		scopearsenal=2;
		displayName="[TFV] M48 HEAT Rocket";
		displayNameShort= "HEAT"
		author="Task Force Vargr Aux Team";
		ammo="TFV_M48_HEAT";
		picture="\a3\Weapons_F_Tank\Launchers\MRAWS\Data\UI\icon_rocket_MRAWS_HEAT_F_ca.paa";
		type="2 *   256";
		count=1;
		mass=75;
		nameSound="launcher";
		model="\OPTRE_Weapons\Ammo\boxAmmo.p3d";
	};
	class TFV_M48_PEN_R_MAG
	{
		scope=2;
		scopearsenal=2;
		displayName="[TFV] M48 Penetrator Rocket";
		displayNameShort= "Penetrator";
		author="Task Force Vargr Aux Team";
		ammo="TFV_M48_PEN_R";
		picture="\a3\Weapons_F_Tank\Launchers\MRAWS\Data\UI\icon_rocket_MRAWS_HEAT_F_ca.paa";
		type="2 *   256";
		count=1;                               /////CHANGE/////
		initSpeed=999;
		mass=100;
		nameSound="launcher";
		model="\OPTRE_Weapons\Ammo\boxAmmo.p3d";
	};
	class TFV_M48_THERMO_MAG
	{
		scope=2;
		scopearsenal=2;
		displayName="[TFV] M48 Thermobaric Rocket";
		displayNameShort= "Thermobaric"
		author="Task Force Vargr Aux Team";
		ammo="TFV_M48_THERMO";
		type="2 *  256";
		picture="\A3\Weapons_F_beta\Launchers\titan\Data\UI\gear_titan_missile_atl_CA.paa";
		mass=100;
		count=1;
		nameSound="launcher";
		model="\OPTRE_Weapons\Ammo\boxAmmo.p3d";
	};
};
class CfgAmmo
{
	class ammo_Penetrator_Base;
	class R_PG32V_F;
	class M_Titan_AT;
	class TCF_50x137_PEN;
	class TFV_pen:ammo_Penetrator_Base
	{
		caliber = 60;
		warheadName = "TandemHEAT";
		hit = 600;
	};
	class TCF_maxpen:ammo_Penetrator_Base
	{
		caliber = 60;
		hit = 800;
	};
class TFV_M48_HEAT:M_Titan_AT
	{
		model = "\A3\weapons_f\launchers\nlaw\nlaw_rocket";
		submunitionAmmo = "TFV_pen";
		submunitionDirectionType = "SubmunitionModelDirection";
		submunitionInitSpeed = 999;
		submunitionParentSpeedCoef = 0;
		submunitionInitialOffset[] = {0,0,-0.2};
		warheadName = "HEAT";
		cost = 500;
		explosive = 0.8;
		timeToLive = 20;
		aiAmmoUsageFlags = "128 + 512";
		allowAgainstInfantry = 0;
		hit = 250;
		indirectHit = 13;
		indirectHitRange = 3;
		fuseDistance = 10;
		irLock = 0;
		airLock = 0;
		lockType = 0;
		laserLock = 0;
		nvLock = 0;
		manualControl = 0;
		weaponLockSystem = "2 + 16";
		airFriction = 0.05000001;
		sideAirFriction = 0.3;
		maneuvrability = 10;
		coefGravity = 2;
		initTime = 0;
		thrustTime = 10;
		thrust = 125;
		maxSpeed = 350;
		flightProfiles[] = {};
		class Direct{};
		class Components{};
		class ace_missileguidance
		{
			enabled = 0;
		};
	};
	class TFV_M48_PEN_R:TCF_50x137_PEN
	{
		submunitionAmmo = "TCF_max_pen";
		hit = 350;
		indirectHit=5555;
		indirectHitRange=1;
		submunitionCount = 1;
		warheadName = "AP";
		explosive = 0;
		cost = 100;
		airFriction = 0.01;
		sideAirFriction = 0.0001;
		maxSpeed = 999;
		initTime = 0;
		thrustTime = 0.1;
		thrust = 1400;
		fuseDistance = 10;
		passThrough = 1;
		CraterEffects = "ATMissileCrater";
		explosionEffects = "ATMissileExplosion";
		timeToLive = 20;
	};
		class TFV_HE:TFV_M48_HEAT
	{
		submunitionAmmo = "";
		submunitionDirectionType = "";
		submunitionInitSpeed = 0;
		submunitionParentSpeedCoef = 0;
		submunitionInitialOffset[] = {0,0,0};
		triggerOnImpact = 0;
		hit = 200;
		indirectHit = 50;
		indirectHitRange = 6;
		warheadName = "HE";
		explosive = 1;
		cost = 200;
		CraterEffects = "ArtyShellCrater";
		ExplosionEffects = "MortarExplosion";
		effectsMissileInit = "";
		effectsMissile = "EmptyEffect";
		allowAgainstInfantry = 1;
	};
	class TFV_M48_THERMO:TFV_HE
	{
		hit = 500;
		indirectHit = 250;
		indirectHitRange = 6;
		cost = 1000;
		warheadName = "Thermobaric";
		ace_frag_enabled = 0;
	};
};