class CfgPatches
{
	class TFV_Weapons
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_M319",
		};
	};
};

class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class MuzzleSlot;
class CowsSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{
	class mk20_base_F;
	class arifle_Mk20_F: mk20_base_F
	{
		class WeaponSlotsInfo;
	};
	class TFV_Rifle_Base: arifle_Mk20_F
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class PointerSlot;
		};
	};
	class UGL_F;
	class TFV_M319: TFV_Rifle_Base
	{
		author = "Task Force Vargr Aux Team";
		scope = 2;
		scopeArsenal = 2;
		cursor = "EmptyCursor";
		cursorAim = "OPTRE_GRNDLNCH";
		handAnim[] = {"OFP2_ManSkeleton","\OPTRE_Weapons\gl\data\anim\OPTRE_Handanim_M319.rtm","Spartan_ManSkeleton","\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\m319_Spartan_HandAnim.rtm"};
		model = "\OPTRE_Weapons\GL\M319.p3d";
		displayName = "[TFV] M319 Grenade Launcher";
		descriptionShort = "Grenade Launcher";
		magazineWell[] = {"TFV_M319"};
		baseWeapon = "TFV_M319";
		muzzlePos = "usti hlavne";
		muzzleEnd = "konec hlavne";
		useModelOptics = 0;
		useExternalOptic = 0;
		memoryPointCamera = "OP_eye";
		cameraDir = "eye_gl_look";
		discreteDistance[] = {50,75,100,150,200,250,300,350,400};
		discreteDistanceCameraPoint[] = {"OP_eye_50","OP_eye_75","OP_eye_100","OP_eye_150","OP_eye_200","OP_eye_250","OP_eye_300","OP_eye_350","OP_eye_400"};
		discreteDistanceInitIndex = 0;
		maxZeroing = 400;
		picture = "\OPTRE_Weapons\gl\icons\m319_icon.paa";
		pictureMjolnirHud = "\OPTRE_Suit_Scripts\textures\weaponIcons\ExplosiveWeapons\M319_icon.paa";
		reloadAction = "WBK_HaloGL_Reload";
		changeFiremodeSound[] = {"A3\Sounds_F\arsenal\weapons\UGL\Firemode_ugl",0.31622776,1,5};
		reloadMagazineSound[] = {"\OPTRE_Wbk_WeaponImprovements\reload\gl_reload.ogg",2,1,25};
		drySound[] = {"A3\Sounds_F\arsenal\weapons\UGL\Dry_ugl",0.56234133,1,10};
		modes[] = {"Single"};
		hiddenSelections[] = {"camoBody","camoSight","camoLogo","camoreticle"};
		hiddenSelectionsTextures[] = {"\optre_weapons\gl\data\gl_co.paa","\optre_weapons\br\data\gl\sight_co.paA","\optre_weapons\gl\data\logos_ca.paa","\optre_weapons\br\data\gl\ubgl_reticle.paa"};
		class Single: Mode_SemiAuto
		{
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"A3\Sounds_F\arsenal\weapons\UGL\Closure_UGL",1,1,10};
				soundClosure[] = {"closure1",1};
			};
			class StandardSound: BaseSoundModeType
			{
				soundSetShot[] = {"Msbs65_01_Ugl_Shot_SoundSet","Msbs65_01_Ugl_Tail_SoundSet","Msbs65_01_Ugl_InteriorTail_SoundSet"};
				begin1[] = {"A3\Sounds_F\arsenal\weapons\UGL\UGL_01",0.70794576,1,200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons\UGL\UGL_02",0.70794576,1,200};
				soundBegin[] = {"begin1",0.5,"begin2",0.5};
			};
			recoil = "recoil_single_gm6";
			recoilProne = "recoil_single_prone_gm6";
			minRange = 30;
			minRangeProbab = 0.1;
			midRange = 200;
			midRangeProbab = 0.7;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		recoil = "recoil_gm6";
		magazineReloadTime = 1;
		reloadTime = 0.1;
		optics = 1;
		modelOptics = "-";
		magazines[] = {			
            "TFV_M319_6rnd_HEAT",
			"TFV_M319_6rnd_Smoke",
			"TFV_M319_6rnd_Smoke_Blue",
			"TFV_M319_6rnd_Smoke_Purple",
			"TFV_M319_6rnd_Smoke_Green",
			"TFV_M319_6rnd_Smoke_Red",
			"TFV_Flare_Red",
			"TFV_Flare_Orange",
			"TFV_Flare_Yellow",
			"TFV_Flare_Green",
			"TFV_Flare_Blue",
			"TFV_Flare_Purple",
			"TFV_Flare_White",
			"ACE_HuntIR_M203",
            };
		class GunParticles{};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 50;
			class MuzzleSlot{};
			class CowsSlot{};
			class PointerSlot: PointerSlot{};
			class UnderBarrelSlot{};
		};
		initSpeed = -1;
	};
};
class cfgMagazineWells
{
	class TFV_M319
	{
		TFV_M319_6rnd_HEAT[] = 
		{
			"TFV_M319_6rnd_HEAT",
			"TFV_M319_6rnd_Smoke",
			"TFV_M319_6rnd_Smoke_Blue",
			"TFV_M319_6rnd_Smoke_Purple",
			"TFV_M319_6rnd_Smoke_Green",
			"TFV_M319_6rnd_Smoke_Red",
			"TFV_Flare_Red",
			"TFV_Flare_Orange",
			"TFV_Flare_Yellow",
			"TFV_Flare_Green",
			"TFV_Flare_Blue",
			"TFV_Flare_Purple",
			"TFV_Flare_White",
		};
	};
};
class CfgMagazines
{
    class 1Rnd_HE_Grenade_shell;
	class 1Rnd_SmokeRed_Grenade_shell;
	class TFV_M319_6rnd_HEAT: 1Rnd_HE_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_HEAT";
		displayName="TFV 6Rnd HEAT";
		displayNameShort="HEAT";
		descriptionshort="6 Round High Explosive Dual Purpose Grenades";
		count=6;
		mass=10;
	};
	class TFV_M319_6rnd_Smoke: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_Smoke";
		displayName="TFV 6Rnd Smoke (White)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=6;
		mass=10;
	};
	class TFV_M319_6rnd_Smoke_Blue: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokeBlue";
		displayName="TFV 6Rnd Smoke (Blue)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=6;
		mass=10;
	};
	class TFV_M319_6rnd_Smoke_Purple: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokePurple";
		displayName="TFV 6Rnd Smoke (Purple)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=6;
		mass=10;
	};
	class TFV_M319_6rnd_Smoke_Green: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokeGreen";
		displayName="TFV 6Rnd Smoke (Green)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=6;
		mass=10;
	};
	class TFV_M319_6rnd_Smoke_Red: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokeRed";
		displayName="TFV 6Rnd Smoke (Red)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=6;
		mass=10;
	};
	class TFV_Flare_Red: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_Red_Flare_Smoke";
		displayName = "TFV 3Rnd Flare (Red)";
		displaynameshort = "Red Flare";
		descriptionshort = "Red Flare";
		count=3;
		mass=10;
	};
	class TFV_Flare_Orange: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_Orange_Flare_Smoke";
		displayName = "TFV 3Rnd Flare (Orange)";
		displaynameshort = "Orange Flare";
		descriptionshort = "Orange Flare";
		count=3;
		mass=10;
	};
	class TFV_Flare_Yellow: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_Yellow_Flare_Smoke";
		displayName = "TFV 3Rnd Flare (Yellow)";
		displaynameshort = "Yellow Flare";
		descriptionshort = "Yellow Flare";
		count=3;
		mass=10;
	};
	class TFV_Flare_Green: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_Green_Flare_Smoke";
		displayName = "TFV 3Rnd Flare (Green)";
		displaynameshort = "Green Flare";
		descriptionshort = "Green Flare";
		count=3;
		mass=10;
	};
	class TFV_Flare_Blue: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_Blue_Flare_Smoke";
		displayName = "TFV 3Rnd Flare (Blue)";
		displaynameshort = "Blue Flare";
		descriptionshort = "Blue Flare";
		count=3;
		mass=10;
	};
	class TFV_Flare_Purple: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_Purple_Flare_Smoke";
		displayName = "TFV 3Rnd Flare (Purple)";
		displaynameshort = "Purple Flare";
		descriptionshort = "Purple Flare";
		count=3;
		mass=10;
	};
	class TFV_Flare_White: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_White_Flare_Smoke";
		displayName = "TFV 3Rnd Flare (White)";
		displaynameshort = "White Flare";
		descriptionshort = "White Flare";
		count=3;
		mass=10;
	};
};
class CfgAmmo 
{
	class M319_HEAT;
	class G_40mm_Smoke;
	class G_40mm_SmokeBlue;
	class G_40mm_SmokePurple;
	class G_40mm_SmokeRed;
	class G_40mm_SmokeGreen;
	class TFV_HEAT: M319_HEAT 
	{
    	model = "OPTRE_weapons\gl\mag_hedp.p3d";
		ACE_damageType="explosive";
		ace_frag_enabled=1;
		ace_frag_classes[]=
		{
			"ace_frag_medium_HD"
		};
		ace_frag_metal=2000;
		ace_frag_charge=450;
		ace_frag_gurney_c=2830;
		ace_frag_gurney_k="1/2";
		ace_vehicle_damage_incendiary=0.80000001;
		hit=450;
		indirectHit=200;
		explosive=0.60000001;
		indirectHitRange=3;
		caliber=6;
		warheadName="TandemHEAT";
		ace_frag_force=1;
		ace_rearm_caliber=39;
		class CamShakeHit
		{
			power=40;
			duration=0.40000001;
			frequency=20;
			distance=2;
		};
  	};
	class TFV_40mm_Smoke:G_40mm_Smoke
	{
		deflecting=5;
		explosive=1;
		simulation="shotShell";
		explosionTime=0;
		fuseDistance=0;
		explosionEffects="OPTRE_Effect_GL_White";
	};
	class TFV_40mm_SmokeBlue: G_40mm_SmokeBlue
	{
		deflecting=5;
		explosive=1;
		simulation="shotShell";
		explosionTime=0;
		fuseDistance=0;
		explosionEffects="OPTRE_Effect_GL_Blue";
	};
	class TFV_40mm_SmokePurple: G_40mm_SmokePurple
	{
		deflecting=5;
		explosive=1;
		simulation="shotShell";
		explosionTime=0;
		fuseDistance=0;
		explosionEffects="OPTRE_Effect_GL_Purple";
	};
	class TFV_40mm_SmokeRed: G_40mm_SmokeRed
	{
		deflecting=5;
		explosive=1;
		simulation="shotShell";
		explosionTime=0;
		fuseDistance=0;
		explosionEffects="OPTRE_Effect_GL_Red";
	};
	class TFV_40mm_SmokeGreen: G_40mm_SmokeGreen
	{
		deflecting=5;
		explosive=1;
		simulation="shotShell";
		explosionTime=0;
		fuseDistance=0;
		explosionEffects="OPTRE_Effect_GL_Green";
	};
	class TFV_Red_Flare:G_40mm_SmokeRed
	{
		model = "\A3\weapons_f\Ammo\UGL_Flare";
		airFriction = 0;
		sideairFriction = 0;
		triggerTime = 1e-08;
		coefGravity = 0.1;
		thrust = 50;
		simulation = "shotIlluminating";
		effectFlare = "TFV_Flare_Light";
		explosionTime = 0.1;
		timetolive = 120;
		initspeed = 25;
		size = 5;
		typicalSpeed = 25;
		flare = 1;
		lightColor[] = {0.8438,0.1383,0.1353,1};
		smokeColor[] = {0.8438,0.1383,0.1353,1};
		brightness = 1;
		intensity = 1;
		flareSize = 1;
		flareAirFriction = 0;
	};
	class TFV_Red_Flare_Smoke:TFV_Red_Flare
	{
		model = "\A3\weapons_f\Ammo\UGL_Flare";
		simulation = "shotSmoke";
		effectsSmoke = "SmokeShellRedEffect";
		fuseDistance = 0;
		submunitionAmmo = "TFV_Red_Flare";
		deleteParentWhenTriggered = 0;
		initspeed = 25;
		typicalSpeed = 25;
		submunitionConeType[] = {"randomCenter",2};
		submunitionConeAngle = 360;
		ACE_muzzleVelocities[] = {25};
		triggerTime = 1;
		smokeColor[] = {0.8438,0.1383,0.1353,1};
	};
	class TFV_Orange_Flare:TFV_Red_Flare
	{
		lightColor[] = {0.6697,0.2275,0.10053,1};
		smokeColor[] = {0.6697,0.2275,0.10053,1};
	};
	class TFV_Orange_Flare_Smoke:TFV_Red_Flare_Smoke
	{
		submunitionAmmo = "TFV_Orange_Flare";
		smokeColor[] = {0.6697,0.2275,0.10053,1};
	};
	class TFV_Yellow_Flare:TFV_Red_Flare
	{
		lightColor[] = {0.9883,0.8606,0.0719,1};
		smokeColor[] = {0.9883,0.8606,0.0719,1};
	};
	class TFV_Yellow_Flare_Smoke:TFV_Red_Flare_Smoke
	{
		submunitionAmmo = "TFV_Yellow_Flare";
		smokeColor[] = {0.9883,0.8606,0.0719,1};
	};
	class TFV_Green_Flare:TFV_Red_Flare
	{
		lightColor[] = {0.25,0.5,0.25,1};
		smokeColor[] = {0.25,0.5,0.25,1};
	};
	class TFV_Green_Flare_Smoke:TFV_Red_Flare_Smoke
	{
		submunitionAmmo = "TFV_Green_Flare";
		smokeColor[] = {0.25,0.5,0.25,1};
	};
	class TFV_Blue_Flare:TFV_Red_Flare
	{
		lightColor[] = {0.1183,0.1867,1,1};
		smokeColor[] = {0.1183,0.1867,1,1};
	};
	class TFV_Blue_Flare_Smoke:TFV_Red_Flare_Smoke
	{
		submunitionAmmo = "TFV_Blue_Flare";
		smokeColor[] = {0.1183,0.1867,1,1};
	};
	class TFV_Purple_Flare:TFV_Red_Flare
	{
		lightColor[] = {0.4341,0.1388,0.4144,1};
		smokeColor[] = {0.4341,0.1388,0.4144,1};
	};
	class TFV_Purple_Flare_Smoke:TFV_Red_Flare_Smoke
	{
		submunitionAmmo = "TFV_Purple_Flare";
		smokeColor[] = {0.4341,0.1388,0.4144,1};
	};
	class TFV_White_Flare:TFV_Red_Flare
	{
		lightColor[] = {1,1,1,1};
		smokeColor[] = {1,1,1,1};
	};
	class TFV_White_Flare_Smoke:TFV_Red_Flare_Smoke
	{
		submunitionAmmo = "TFV_White_Flare";
		smokeColor[] = {1,1,1,1};
	};
};
class TFV_Flare_Light
{
	class Light1
	{
		intensity = 1;
		interval = 1;
		position[] = {0,0,0};
		simulation = "light";
		type = "FlareLight";
	};
};
