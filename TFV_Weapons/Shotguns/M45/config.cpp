class CfgPatches
{
	class TFV_Weapons_M45
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_M45_Tac",
		};
	};
};
class OPTRE_CowsSlot_Rail;
class OPTRE_Pointers;
class OPTRE_UnderBarrelSlot_rail;
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{
	class arifle_Mk20_F;
	class Pistol_Base_F;
	class WeaponSlotsInfo;
	class UGL_F;
	
	enum
	{
		destructengine = 2,
		destructdefault = 6,
		destructwreck = 7,
		destructtree = 3,
		destructtent = 4,
		stabilizedinaxisx = 1,
		stabilizedinaxesxyz = 4,
		stabilizedinaxisy = 2,
		stabilizedinaxesboth = 3,
		destructno = 0,
		stabilizedinaxesnone = 0,
		destructman = 5,
		destructbuilding = 1
	};
	class TFV_arifle_UNSC_M45_Base_F:arifle_Mk20_F
	{
		author="Task Force Vargr Aux Mod";
		scope=1;
		_generalMacro="TFV_arifle_UNSC_M45_Base_F";
		magazines[]=
		{
			"TFV_12Gauge_Buck_Round",
			"TFV_12Gauge_Slug_Round",
			"TFV_12Gauge_HEAP_Round",
			"TFV_12Gauge_Dragon_Round",
		};
		magazineWell[]=
		{
			"TFV_M45_Magwell"
		};

		ODST_1 = "OPTRE_ODST_HUD_AmmoCount_Shotgun";
		Glasses = "OPTRE_GLASS_HUD_AmmoCount_Shotgun";
		Eye = "OPTRE_EYE_HUD_AmmoCount_Shotgun";
		HUD_BulletInARows=1;
		HUD_TotalPosibleBullet=12;
		cursor="OPTRE_M45";
		pictureWire = "\OPTRE_Weapons\data\Pictures\WireWeaponIcons\Prime\Shotgun\Shotgun.paa";

		ACE_barrelTwist=177.8;
		ACE_barrelLength=807;
		ACE_overheating_mrbs=99999;
		ACE_overheating_jamchance=0;
		ACE_overheating_slowdownFactor=0;
		ACE_overheating_allowSwapBarrel=0;
		ACE_overheating_dispersion=0;
		ACE_overheating_closedbolt=0;
		ACE_arsenal_hide=0;
		ACE_twistDirection=1;
		ACE_clearJamAction="GestureReload";
		ACE_checkTemperatureAction="Gear";
		magazineReloadSwitchPhase=0.5;
		class Library
		{
			libTextDesc="$STR_A3_CfgWeapons_arifle_XMX_Library0";
		};
		//reloadAction="GestureReloadTRG";
		reloadAction = "WBK_HaloShotgun_Reload";
		reloadMagazineSound[] = {"\OPTRE_Wbk_WeaponImprovements\reload\shotgun_reload.ogg",2,1,25};
		recoil="recoil_gm6";
		maxZeroing=100;
		class WeaponSlotsInfo:WeaponSlotsInfo
			{
			class CowsSlot:OPTRE_CowsSlot_Rail
				{
					compatibleitems[] = {"OPTRE_M7_Sight","Optre_Evo_Sight","Optre_Evo_Sight_Riser"};					
				};
			class UnderBarrelSlot:OPTRE_UnderBarrelSlot_rail
				{
					compatibleitems[] = {};
				};
			class MuzzleSlot
				{
                compatibleItems[] = {};
            	};
			};
		distanceZoomMin=0;
		distanceZoomMax=100;
		descriptionShort="M45 Tactical Shotgun";
		handAnim[] =
		{
		"OFP2_ManSkeleton",
		"\OPTRE_Weapons\shotgun\data\anim\OPTRE_M45_handanim.rtm",
		"Spartan_ManSkeleton",
		"\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\benelli_Spartan.rtm"
		};
		muzzles[]=
		{
			"this"
		};
		aiDispersionCoefY=2;
		aiDispersionCoefX=3;
	};
	class TFV_M45_TAC:TFV_arifle_UNSC_M45_Base_F
	{
		author="Task Force Vargr Aux Mod";
		_generalMacro="TFV_M45_TAC";
		baseWeapon="TFV_M45_TAC";
		scope=2;
		displayName="[Vargr] M45 Tactical Shotgun";
		model="\OPTRE_Weapons\Shotgun\shotguntac.p3d";
		mass=60;
		reloadAction = "WBK_HaloShotgun_Reload";
		reloadMagazineSound[] = {"\OPTRE_Wbk_WeaponImprovements\reload\shotgun_reload.ogg",2,1,25};
		//reloadAction="GestureReloadBR55";
		picture="\OPTRE_weapons\shotgun\icons\M452.paa";
		pictureMjolnirHud = "\OPTRE_Suit_Scripts\textures\weaponIcons\Shotguns\M451_icon.paa";
		handAnim[] =
		{
		"OFP2_ManSkeleton",
		"\OPTRE_Weapons\Shotgun\data\anim\optre_shotgun_idle.rtm",
		"Spartan_ManSkeleton",
		"\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\benelli_Spartan.rtm"
		};
		muzzles[]=
		{
			"this"
		};
		class FlashLight
		{
			color[] = {180,160,130};
			ambient[] = {0.9,0.8,0.7};
			intensity = 100;
			size = 1;
			innerAngle = 5;
			outerAngle = 100;
			coneFadeCoef = 10;
			position = "flash dir";
			direction = "flash";
			useFlare = 1;
			flareSize = 1.5;
			flareMaxDistance = 100;
			dayLight = 1;
			class Attenuation
			{
				start = 0;
				constant = 0.5;
				linear = 0.1;
				quadratic = 0.2;
				hardLimitStart = 27;
				hardLimitEnd = 34;
			};
			scale[] = {0};
		};
				class EventHandlers

		{

			fired = "_this call CBA_fnc_weaponEvents";

		};
		class CBA_weaponEvents
		{
			handAction = "WBK_HaloShotgun_Pump";
			sound = "optre_shotgun_pump";
			soundLocation = "LeftHandMiddle1";
			delay = 0;
			onEmpty = 0;
			hasOptic = 1;
			soundEmpty = "";
			soundLocationEmpty = "";
		};
		inertia=0.7;
		aimTransitionSpeed=0.69999999;
		dexterity=2;
		recoil="recoil_gm6";
		maxZeroing=100;
		class ItemInfo
		{
			priority=1;
		};
		modes[] = {"Single","close","short","medium","far"};
		class Single: Mode_SemiAuto
		{
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "DefaultRifle";
				closure1[] = {"",1.0,1,200};
				closure2[] = {"",1.0,1,200};
				soundClosure[] = {"closure1",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				soundsetshot[] = {"OPTRE_Shotgun_SoundSet","M320_Tail_SoundSet","M320_InteriorTail_SoundSet"};
			};
			reloadTime = 0.5;
			dispersion = 0.001;
			recoil = "recoil_single_ksg";
			recoilProne = "recoil_single_prone_ksg";
			minRange = 1;
			minRangeProbab = 0.02;
			midRange = 50;
			midRangeProbab = 0.03;
			maxRange = 100;
			maxRangeProbab = 0.04;
		};
		class close: Single
		{
			burst = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 35;
			maxRangeProbab = 0.04;
			showToPlayer = 0;
		};
		class short: close
		{
			burst = 1;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 300;
			minRange = 35;
			minRangeProbab = 0.05;
			midRange = 55;
			midRangeProbab = 0.7;
			maxRange = 75;
			maxRangeProbab = 0.04;
		};
		class medium: close
		{
			burst = 1;
			aiRateOfFire = 3;
			aiRateOfFireDistance = 600;
			minRange = 75;
			minRangeProbab = 0.05;
			midRange = 100;
			midRangeProbab = 0.6;
			maxRange = 125;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			burst = 1;
			aiRateOfFire = 6;
			aiRateOfFireDistance = 700;
			minRange = 125;
			minRangeProbab = 0.04;
			midRange = 150;
			midRangeProbab = 0.5;
			maxRange = 200;
			maxRangeProbab = 0.01;
		};
		aiDispersionCoefY = 2;
		aiDispersionCoefX = 3;
	};
};
class cfgMagazineWells
{
	class TFV_M45_Magwell 
	{
		TFV_Shotgun_Ammo[] = 
		{
			"TFV_12Gauge_Buck_Round",
			"TFV_12Gauge_Slug_Round",
			"TFV_12Gauge_HEAP_Round",
			"TFV_12Gauge_Dragon_Round",
		};
	};
};
class CfgMagazines
{
    class CA_Magazine;
	class TFV_12Gauge_Buck_Round: CA_Magazine
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[Vargr] Buckshot";
		ammo="TFV_12Gauge_Buckshot";
		count=12;
		initSpeed=400;
		tracersEvery=1;
		descriptionShort="12 Gauge Buckshot";
		mass=5;
		ace_isbelt=0;
		ace_attachable=0;
		ace_placeable=0;
		ace_setupobject=0;
		ace_delaytime=0;
		ace_triggers=0;
		ace_magazines_forcemagazinemuzzlevelocity=1;
		model="\OPTRE_Weapons\Shotgun\Shell_mag_S.p3d";
		picture="\a3\weapons_F\data\ui\m_12gauge_ca.paa";
		lastRoundsTracer = 12;
	};
    class TFV_12Gauge_Slug_Round: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] Slug";
	ammo="TFV_12Gauge_Slug";
	count=12;
	initSpeed=450;
	descriptionShort="12 Gauge Slug";
	};
	/*class TFV_12Gauge_HEAP_Round: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] HEAP Round";
	ammo="TFV_12Gauge_HEAP";
	count=12;
	initSpeed=420;
	descriptionShort="12 Gauge HEAP Round";
	};
	class TFV_12Gauge_Dragon_Round: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] Dragons Breath";
	ammo="TFV_12Gauge_DragonsBreath";
	count=12;
	initSpeed=380;
	descriptionShort="12 Gauge Dragons Breath";
	};*/
};