class CfgPatches
{
	class TFV_Weapons_M90
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_M90",
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{
	class arifle_Mk20_F;
	class Pistol_Base_F;
	class WeaponSlotsInfo;
	class UGL_F;
	
	enum
	{
		destructengine = 2,
		destructdefault = 6,
		destructwreck = 7,
		destructtree = 3,
		destructtent = 4,
		stabilizedinaxisx = 1,
		stabilizedinaxesxyz = 4,
		stabilizedinaxisy = 2,
		stabilizedinaxesboth = 3,
		destructno = 0,
		stabilizedinaxesnone = 0,
		destructman = 5,
		destructbuilding = 1
	};
	class TFV_arifle_UNSC_M90_Base_F: arifle_Mk20_F
	{
		author="Task Force Vargr Aux Mod";
		scope=1;
		_generalMacro="TFV_arifle_UNSC_M90_Base_F";
		magazines[]=
		{
			"TFV_12Gauge_Buck_Round",
			"TFV_12Gauge_Slug_Round",
			"TFV_12Gauge_HEAP_Round",
			"TFV_12Gauge_Dragon_Round",
		};
		magazineWell[]=
		{
			"TFV_M90_Magwell"
		};

		ODST_1 = "OPTRE_ODST_HUD_AmmoCount_Shotgun";
		Glasses = "OPTRE_GLASS_HUD_AmmoCount_Shotgun";
		Eye = "OPTRE_EYE_HUD_AmmoCount_Shotgun";
		HUD_BulletInARows=1;
		HUD_TotalPosibleBullet=12;
		cursor="OPTRE_M45";
		pictureWire = "\OPTRE_Weapons\data\Pictures\WireWeaponIcons\Prime\Shotgun\Shotgun.paa";

		ACE_barrelTwist=177.8;
		ACE_barrelLength=807;
		ACE_overheating_mrbs=99999;
		ACE_overheating_jamchance=0;
		ACE_overheating_slowdownFactor=0;
		ACE_overheating_allowSwapBarrel=0;
		ACE_overheating_dispersion=0;
		ACE_overheating_closedbolt=0;
		ACE_arsenal_hide=0;
		ACE_twistDirection=1;
		ACE_clearJamAction="GestureReload";
		ACE_checkTemperatureAction="Gear";
		magazineReloadSwitchPhase=0.5;
		class Library
		{
			libTextDesc="$STR_A3_CfgWeapons_arifle_XMX_Library0";
		};
		//reloadAction="GestureReloadTRG";
		reloadAction = "WBK_HaloShotgun_Reload";
		reloadMagazineSound[] = {"\OPTRE_Wbk_WeaponImprovements\reload\shotgun_reload.ogg",2,1,25};
		recoil="recoil_gm6";
		maxZeroing=100;

		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
				displayName="Muzzle Slot";
				compatibleItems[]=
				{

					//"optre_MA5Suppressor"
				};
				iconPosition[]={0,0.44999999};
				iconScale=0.2;
			};
			class CowsSlot: CowsSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
				displayName="$STR_A3_CowsSlot0";
				compatibleitems[]=
				{
				//	"OPTRE_M7_Sight",
				//	"UNSC_br_scope"
				};
				iconPosition[]={0.5,0.34999999};
				iconScale=0.2;
			};
			class PointerSlot: PointerSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\SIDE";
				displayName="$STR_A3_PointerSlot0";
				compatibleItems[]=
				{
				//	"optre_bmr_laser",
					//"acc_flashlight"
				};
				iconPosition[]={0.2,0.44999999};
				iconScale=0.25;
			};
		};
		distanceZoomMin=0;
		distanceZoomMax=100;
		descriptionShort="Standard Issue M90";
		handAnim[] =
		{
		"OFP2_ManSkeleton",
		"\UNSC_F_Weapons\weapons\animations\m90_caws_standing.rtm",
		"Spartan_ManSkeleton",
		"\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\benelli_Spartan.rtm"
		};
		muzzles[]=
		{
			"this"
		};
		aiDispersionCoefY=2;
		aiDispersionCoefX=3;
	};
	class TFV_M90: TFV_arifle_UNSC_M90_Base_F
	{
		author="Task Force Vargr Aux Mod";
		_generalMacro="TFV_M90";
		baseWeapon="TFV_M90";
		scope=2;
		displayName="[Vargr] M90 Tactical Shotgun";
		model="\UNSC_F_Weapons\weapons\UNSC_M90.p3d";
		mass=60;
		reloadAction = "WBK_HaloShotgun_Reload";
		reloadMagazineSound[] = {"\OPTRE_Wbk_WeaponImprovements\reload\shotgun_reload.ogg",2,1,25};
		//reloadAction="GestureReloadBR55";
		picture="\UNSC_F_Weapons\weapons\UI\M90_UI.paa";
		UiPicture="\UNSC_F_Weapons\weapons\UI\M90_UI.paa";
		handAnim[] =
		{
		"OFP2_ManSkeleton",
		"\UNSC_F_Weapons\weapons\animations\m90_caws_standing.rtm",
		"Spartan_ManSkeleton",
		"\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\benelli_Spartan.rtm"
		};
		muzzles[]=
		{
			"this"
		};
		class FlashLight
		{
			color[] = {180,160,130};
			ambient[] = {0.9,0.8,0.7};
			intensity = 100;
			size = 1;
			innerAngle = 5;
			outerAngle = 100;
			coneFadeCoef = 10;
			position = "flash dir";
			direction = "flash";
			useFlare = 1;
			flareSize = 1.5;
			flareMaxDistance = 100;
			dayLight = 1;
			class Attenuation
			{
				start = 0;
				constant = 0.5;
				linear = 0.1;
				quadratic = 0.2;
				hardLimitStart = 27;
				hardLimitEnd = 34;
			};
			scale[] = {0};
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass=60;
			class MuzzleSlot: MuzzleSlot
			{
				inkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
				displayName="Muzzle Slot";
				compatibleItems[]=
				{
				//	"optre_MA5Suppressor"
				};
				iconPosition[]={0,0.40000001};
			};
			class CowsSlot: CowsSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
				displayName="$STR_A3_CowsSlot0";
				compatibleitems[]=
				{
				//	"UNSC_MA5A_Smartlink",
				};
				iconPosition[]={0.5,0.30000001};
			};
			class PointerSlot: PointerSlot
			{
				iconPosition[]={0.2,0.40000001};
			};
		};
		inertia=0.7;
		aimTransitionSpeed=0.69999999;
		dexterity=2;
		recoil="recoil_gm6";
		maxZeroing=100;
		class ItemInfo
		{
			priority=1;
		};
		descriptionShort="UNSC Standard issue M90 Shotgun";
		modes[] = {"Single","close","short","medium","far"};
		class Single: Mode_SemiAuto
		{
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "DefaultRifle";
				closure1[] = {"",1.0,1,200};
				closure2[] = {"",1.0,1,200};
				soundClosure[] = {"closure1",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				soundsetshot[] = {"OPTRE_Shotgun_SoundSet","M320_Tail_SoundSet","M320_InteriorTail_SoundSet"};
			};
			reloadTime = 0.5;
			dispersion = 0.001;
			recoil = "recoil_single_ksg";
			recoilProne = "recoil_single_prone_ksg";
			minRange = 1;
			minRangeProbab = 0.02;
			midRange = 50;
			midRangeProbab = 0.03;
			maxRange = 100;
			maxRangeProbab = 0.04;
		};
		class close: Single
		{
			burst = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 35;
			maxRangeProbab = 0.04;
			showToPlayer = 0;
		};
		class short: close
		{
			burst = 1;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 300;
			minRange = 35;
			minRangeProbab = 0.05;
			midRange = 55;
			midRangeProbab = 0.7;
			maxRange = 75;
			maxRangeProbab = 0.04;
		};
		class medium: close
		{
			burst = 1;
			aiRateOfFire = 3;
			aiRateOfFireDistance = 600;
			minRange = 75;
			minRangeProbab = 0.05;
			midRange = 100;
			midRangeProbab = 0.6;
			maxRange = 125;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			burst = 1;
			aiRateOfFire = 6;
			aiRateOfFireDistance = 700;
			minRange = 125;
			minRangeProbab = 0.04;
			midRange = 150;
			midRangeProbab = 0.5;
			maxRange = 200;
			maxRangeProbab = 0.01;
		};
		aiDispersionCoefY = 2;
		aiDispersionCoefX = 3;
	};
};
class cfgMagazineWells
{
		class TFV_M90_Magwell 
	{
		TFV_Shotgun_Ammo[] = 
		{
			"TFV_12Gauge_Buck_Round",
			"TFV_12Gauge_Slug_Round",
			"TFV_12Gauge_HEAP_Round",
			"TFV_12Gauge_Dragon_Round",
		};
	};
};
class CfgMagazines
{
    class CA_Magazine;
	class TFV_12Gauge_Buck_Round: CA_Magazine
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[Vargr] Buckshot";
		ammo="TFV_12Gauge_Buckshot";
		count=12;
		initSpeed=400;
		tracersEvery=1;
		descriptionShort="12 Gauge Buckshot";
		mass=5;
		ace_isbelt=0;
		ace_attachable=0;
		ace_placeable=0;
		ace_setupobject=0;
		ace_delaytime=0;
		ace_triggers=0;
		ace_magazines_forcemagazinemuzzlevelocity=1;
		model="\OPTRE_Weapons\Shotgun\Shell_mag_S.p3d";
		picture="\a3\weapons_F\data\ui\m_12gauge_ca.paa";
		lastRoundsTracer = 12;
	};
    class TFV_12Gauge_Slug_Round: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] Slug";
	ammo="TFV_12Gauge_Slug";
	count=12;
	initSpeed=450;
	descriptionShort="12 Gauge Slug";
	};
	/*class TFV_12Gauge_HEAP_Round: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] HEAP Round";
	ammo="TFV_12Gauge_HEAP";
	count=12;
	initSpeed=420;
	descriptionShort="12 Gauge HEAP Round";
	};
	class TFV_12Gauge_Dragon_Round: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] Dragons Breath";
	ammo="TFV_12Gauge_DragonsBreath";
	count=12;
	initSpeed=380;
	descriptionShort="12 Gauge Dragons Breath";
	};*/
};
