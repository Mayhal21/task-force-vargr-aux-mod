class CfgPatches
{
	class TFV_Weapons_Bulldog
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
            "TFV_Bulldog",
		};
	};
};
class MuzzleSlot;
class CowsSlot;
class PointerSlot;
class UnderBarrelSlot;
class Mode_SemiAuto;
class OPTRE_MuzzleSlot;
class OPTRE_Pointers;
class OPTRE_CowsSlot_Rail;
class OPTRE_UnderBarrelSlot_rail;
class CfgWeapons
{
	class mk20_base_F;
	class arifle_Mk20_F: mk20_base_F
	{
		class Single;
		class single_far_optics2;
		class single_medium_optics1;
		class FullAuto;
		class fullauto_medium;
	};
	class TFV_Bulldog: arifle_Mk20_F
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		baseWeapon="TFV_Bulldog";
		displayName="[Vargr] Bulldog";
		descriptionShort="Combat Shotgun";
		description="The CQS48 Bulldog is a pump-action combat shotgun which fires 12-gauge shells from an 7-round rotating drum";
		pictureWire="\OPTRE_Weapons\bulldog\data\wire\bulldog.paa";
		pictureMjolnirHud="\OPTRE_Suit_Scripts\textures\weaponIcons\Shotguns\Bulldog_icon.paa";
		model="\OPTRE_Weapons\bulldog\bulldog_auto.p3d";
		cursor="OPTRE_M45";
		hiddenSelections[]=
		{
			"camoBody",
			"camoDecal"
		};
		hiddenSelectionsTextures[]=
		{
			"\OPTRE_Weapons\bulldog\data\Body_Black.paa",
			"\OPTRE_Weapons\bulldog\data\decal\cqs48a\DecalSheet_co.paa"
		};
		hiddenSelectionsMaterials[]=
		{
			"\OPTRE_Weapons\bulldog\data\body.rvmat",
			"\OPTRE_Weapons\bulldog\data\decal\cqs48a\decal.rvmat"
		};
		magazines[]=
		{
			"TFV_Bulldog_Buckshot_Magazine",
			"TFV_Bulldog_Slug_Magazine",
			"TFV_Bulldog_HEAP_Magazine",
			"TFV_Bulldog_Dragon_Magazine",
		};
		magazineWell[]=
		{
			"TFV_Bulldog_Magwell",
		};
		reloadAction="WBK_Commando_Reload";
		picture="\OPTRE_Weapons\MA37K\icons\ma37k_icon.paa";
		handAnim[]=
		{
			"OFP2_ManSkeleton",
			"\OPTRE_Weapons\bulldog\data\anim\bulldog.rtm",
			"Spartan_ManSkeleton",
			"\OPTRE_Weapons\bulldog\data\anim\bulldog_spartan.rtm"
		};
		HUD_BulletInARows=2;
		HUD_TotalPosibleBullet=24;
		recoil="OPTRE_Recoil_Bulldog";
		class WeaponSlotsInfo
		{
			mass=40;
			class MuzzleSlot
			{
				compatibleitems[]={};
			};
			class CowsSlot: OPTRE_CowsSlot_Rail
			{
				compatibleitems[]={"OPTRE_M7_Sight","Optre_Evo_Sight","Optre_Evo_Sight_Riser"};
			};
			class PointerSlot:OPTRE_Pointers
			{
				compatibleitems[]=
					{					

					};
			};
			class UnderBarrelSlot:OPTRE_UnderBarrelSlot_rail
			{
				compatibleitems[]={};
			};
		};
		modes[]=
		{
			"Single",
			"Single_Medium",
			"FullAuto",
			"fullauto_medium",
			"fullauto_far"
		};
		class Single: Single
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]=
				{
					"",
					1,
					1,
					200
				};
				closure2[]=
				{
					"",
					1,
					1,
					200
				};
				soundClosure[]=
				{
					"closure1",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"OPTRE_Shotgun_SoundSet",
					"M320_Tail_SoundSet",
					"M320_InteriorTail_SoundSet"
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[]=
				{
					"DMR05_silencerShot_SoundSet",
					"DMR05_silencerTail_SoundSet",
					"DMR05_silencerInteriorTail_SoundSet"
				};
			};
			reloadTime="60/350";
			aiDispersionCoefX=1.5;
			aiDispersionCoefY=2.5;
			aiRateOfFire=1;
			aiRateOfFireDispersion=0.75;
			aiRateOfFireDistance=215;
			maxRange=300;
			maxRangeProbab=0.15000001;
			midRange=215;
			midRangeProbab=0.80000001;
			minRange=175;
			minRangeProbab=0.1;
		};
		class Single_Medium: Single
		{
			showToPlayer=0;
			aiDispersionCoefX=1;
			aiDispersionCoefY=1.5;
			aiRateOfFire=1.25;
			aiRateOfFireDistance=350;
			maxRange=425;
			maxRangeProbab=0.15000001;
			midRange=350;
			midRangeProbab=0.80000001;
			minRange=215;
			minRangeProbab=0.1;
			burst="1 + round random 1";
		};
		class FullAuto: FullAuto
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]=
				{
					"",
					1,
					1,
					200
				};
				closure2[]=
				{
					"",
					1,
					1,
					200
				};
				soundClosure[]=
				{
					"closure1",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"OPTRE_Shotgun_SoundSet",
					"M320_Tail_SoundSet",
					"M320_InteriorTail_SoundSet"
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[]=
				{
					"DMR05_silencerShot_SoundSet",
					"DMR05_silencerTail_SoundSet",
					"DMR05_silencerInteriorTail_SoundSet"
				};
			};
			reloadTime="60/350";
			aiRateOfFireDistance=50;
			aiRateOfFire=0.5;
			maxRange=20;
			maxRangeProbab=0.15000001;
			midRange=10;
			midRangeProbab=0.80000001;
			minRange=0;
			minRangeProbab=0.1;
		};
		class fullauto_medium: FullAuto
		{
			showToPlayer=0;
			aiRateOfFire=0.55000001;
			aiRateOfFireDistance=75;
			maxRange=0;
			maxRangeProbab=0.44999999;
			midRange=25;
			midRangeProbab=0.94999999;
			minRange=75;
			minRangeProbab=0.44999999;
			burst="2 + round random 3";
		};
		class fullauto_far: FullAuto
		{
			showToPlayer=0;
			aiRateOfFire=0.75;
			aiRateOfFireDistance=120;
			maxRange=175;
			maxRangeProbab=0.44999999;
			midRange=115;
			midRangeProbab=0.94999999;
			minRange=55;
			minRangeProbab=0.25;
			burst="3 + round random 3";
		};
		class GunParticles
		{
			class EffectShotCloud
			{
				positionName="Nabojnicestart";
				directionName="Nabojniceend";
				effectName="CaselessAmmoCloud";
			};
		};
	};
};
class cfgMagazineWells
{
	class TFV_Bulldog_Magwell 
	{
		TFV_Shotgun_Ammo[] = 
		{
			"TFV_Bulldog_Buckshot_Magazine",
			"TFV_Bulldog_Slug_Magazine",
			"TFV_Bulldog_HEAP_Magazine",
			"TFV_Bulldog_Dragon_Magazine",
		};
	};
};
class CfgMagazines
{
    class 20Rnd_762x51_Mag;
	class TFV_Bulldog_Buckshot_Magazine: 20Rnd_762x51_Mag
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		displayName="[Vargr] Buckshot";
		ammo="TFV_12Gauge_Buckshot";
		count=24;
		initSpeed=400;
		tracersEvery=1;
		descriptionShort="12 Gauge Buckshot";
		mass=5;
		ace_isbelt=0;
		ace_attachable=0;
		ace_placeable=0;
		ace_setupobject=0;
		ace_delaytime=0;
		ace_triggers=0;
		ace_magazines_forcemagazinemuzzlevelocity=1;
		dlc="OPTRE";
		model="\OPTRE_Weapons\bulldog\drum.p3d";
		modelSpecial="\OPTRE_Weapons\bulldog\drum.p3d";
		picture="\a3\weapons_F\data\ui\m_12gauge_ca.paa";
		modelSpecialIsProxy=1;
		hiddenSelections[]=
		{
			"camoBody"
		};
		hiddenSelectionsTextures[]=
		{
			"\OPTRE_Weapons\bulldog\data\Body_co.paa"
		};
		lastRoundsTracer = 12;
	};
    class TFV_Bulldog_Slug_Magazine: TFV_Bulldog_Buckshot_Magazine 
    {
	displayName="[Vargr] Slug";
	ammo="TFV_12Gauge_Slug";
	count=24;
	initSpeed=450;
	descriptionShort="12 Gauge Slug";
	};
	/*class TFV_Bulldog_HEAP_Magazine: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] HEAP Round";
	ammo="TFV_12Gauge_HEAP";
	count=24;
	initSpeed=420;
	descriptionShort="12 Gauge HEAP Round";
	};
	class TFV_Bulldog_Dragon_Magazine: TFV_12Gauge_Buck_Round 
    {
	displayName="[Vargr] Dragons Breath";
	ammo="TFV_12Gauge_DragonsBreath";
	count=24;
	initSpeed=380;
	descriptionShort="12 Gauge Dragons Breath";
	};*/
};
