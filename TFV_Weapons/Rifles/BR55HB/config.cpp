class CfgPatches
{
	class TFV_Weapons
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_BR55_SB_GL",
			"TFV_BR55_SB",
			"TFV_BR55_HB",
			"TFV_BR55_HB_GL"
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{
	class arifle_Mk20_F;
	class Pistol_Base_F;
	class WeaponSlotsInfo;
	class UGL_F;
	class TFV_arifle_UNSC_br55_gl_Base_F: arifle_Mk20_F
	{
		author="Task Force Vargr Aux Team";
		scope=1;
		_generalMacro="TFV_arifle_UNSC_br55_gl_Base_F";
		magazines[]=
		{
			"TFV_BR55_AP_Magazine",
			"TFV_BR55_JHP_Magazine",
		};
		magazineWell[]=
		{
			"TFV_BR55_Magwell"
		};

		ODST_1 = "OPTRE_ODST_HUD_AmmoCount_AR";
		Glasses = "OPTRE_GLASS_HUD_AmmoCount_AR";
		Eye = "OPTRE_EYE_HUD_AmmoCount_AR";
		HUD_BulletInARows=2;
		HUD_TotalPosibleBullet=36;
		cursor="OPTRE_BR55";
		pictureWire="\OPTRE_Weapons\data\Pictures\WireWeaponIcons\Prime\BattleRifle\BR_SCOPE.paa";

		ACE_barrelTwist=177.8;
		ACE_barrelLength=607;
		ACE_overheating_mrbs=99999;
		ACE_overheating_jamchance=0;
		ACE_overheating_slowdownFactor=0;
		ACE_overheating_allowSwapBarrel=0;
		ACE_overheating_dispersion=0;
		ACE_overheating_closedbolt=0;
		ACE_arsenal_hide=0;
		ACE_twistDirection=1;
		ACE_clearJamAction="GestureReload";
		ACE_checkTemperatureAction="Gear";
		magazineReloadSwitchPhase=0.5;
		class Library
		{
			libTextDesc="$STR_A3_CfgWeapons_arifle_XMX_Library0";
		};
		//reloadAction="GestureReloadTRG";
		reloadAction="GestureReloadBR55";
		recoil="recoil_trg20";
		maxZeroing=1000;

		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
				displayName="Muzzle Slot";
				compatibleItems[]=
				{

					//"optre_MA5Suppressor",
					"19_UNSC_br55_Suppressor",
					"19_UNSC_br55L_Suppressor"
				};
				iconPosition[]={0,0.44999999};
				iconScale=0.2;
			};
			class CowsSlot: CowsSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
				displayName="$STR_A3_CowsSlot0";
				compatibleitems[]=
				{
					//"optre_M7_sight",
					"19_UNSC_br_scope"
				};
				iconPosition[]={0.5,0.34999999};
				iconScale=0.2;
			};
			class PointerSlot: PointerSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\SIDE";
				displayName="$STR_A3_PointerSlot0";
				compatibleItems[]=
				{
					//"optre_bmr_laser",
					//"acc_flashlight"
					"19_UNSC_br55_LAM"
				};
				iconPosition[]={0.2,0.44999999};
				iconScale=0.25;
			};
		};
		distanceZoomMin=0;
		distanceZoomMax=1000;
		descriptionShort="Battle Riffle - 55 with UGL";
		handAnim[] = 
		{
			"OFP2_ManSkeleton",
			"\UNSC_F_Weapons\weapons\animations\br55_gl_standing.rtm",
			"Spartan_ManSkeleton","\OPTRE_FC_Weapons\ConcussionRifle\data\anim\Spartan_ConcRifleHandAnim.rtm"
		};
		muzzles[]=
		{
			"this"
		};
		modes[]=
		{
			"Single",
			"Burst"
		};
		class GL_3GL_F: UGL_F
		{
			displayName="$STR_A3_cfgweapons_gl0";
			descriptionShort="$STR_A3_cfgweapons_gl1";
			useModelOptics=true;
			useExternalOptic=false;
			magazines[]=
			{
				"TFV_GRL45_3rnd_HEAT",
				"TFV_GRL45_3rnd_Smoke",
				"TFV_GRL45_3rnd_Smoke_Blue",
				"TFV_GRL45_3rnd_Smoke_Purple",
				"TFV_GRL45_3rnd_Smoke_Green",
				"TFV_GRL45_3rnd_Smoke_Red",
				"ACE_HuntIR_M203",
				"TFV_Flare_Red",
				"TFV_Flare_Orange",
				"TFV_Flare_Yellow",
				"TFV_Flare_Green",
				"TFV_Flare_Blue",
				"TFV_Flare_Purple",
				"TFV_Flare_White",
			};
			magazineWell[]=
			{
				"TFV_GL_3Rnd"
			};
			cameraDir="OP_look";
			discreteDistance[]={100,150,200,250,300,350};
			discreteDistanceCameraPoint[]=
			{
				"OP_eye2",
				"OP_eye3",
				"OP_eye4",
				"OP_eye5",
				"OP_eye6",
				"OP_eye8"
			};
			discreteDistanceInitIndex=1;
			reloadAction="GestureReloadMXUGL";
			reloadMagazineSound[]=
			{
				"A3\Sounds_F\arsenal\weapons\Rifles\MX\Mx_UGL_reload",
				1,
				1,
				10
			};
		};
		aiDispersionCoefY=1;
		aiDispersionCoefX=2;
	};
	class TFV_BR55_SB_GL: TFV_arifle_UNSC_br55_gl_Base_F
	{
		author="Task Force Vargr Aux Team";
		_generalMacro="TFV_BR55_SB_GL";
		baseWeapon="TFV_BR55_SB_GL";
		scope=2;
		displayName="[Vargr] Battle Rifle Short w/UGL";
		model="\UNSC_F_Weapons\weapons\UNSC_br55_gl.p3d";
		mass=85;
		reloadAction="GestureReloadBR55";
		picture="\UNSC_F_Weapons\weapons\UI\BR55_gl_UI.paa";
		UiPicture="\UNSC_F_Weapons\weapons\UI\BR55_gl_UI.paa";
		handAnim[] = 
		{
			"OFP2_ManSkeleton",
			"\UNSC_F_Weapons\weapons\animations\br55_gl_standing.rtm",
			"Spartan_ManSkeleton","\OPTRE_FC_Weapons\ConcussionRifle\data\anim\Spartan_ConcRifleHandAnim.rtm"
		};
		muzzles[]=
		{
			"this",
			"GL_3GL_F"
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass=85;
			class MuzzleSlot: MuzzleSlot
			{
				inkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
				displayName="Muzzle Slot";
				compatibleItems[]=
				{
					//"muzzle_snds_b",
					//"optre_MA5Suppressor",
					"19_UNSC_br55_Suppressor",
					"19_UNSC_br55L_Suppressor"
				};
				iconPosition[]={0,0.40000001};
			};
			class CowsSlot: CowsSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
				displayName="$STR_A3_CowsSlot0";
				compatibleitems[]=
				{
					"optre_M7_sight",
					"19_UNSC_br_scope",
					"Optre_Evo_Sight",
					"Optre_Evo_Sight_Riser"
				};
				iconPosition[]={0.5,0.30000001};
			};
			class PointerSlot: PointerSlot
			{
				iconPosition[]={0.2,0.40000001};
			};
		};
		inertia=0.5;
		aimTransitionSpeed=0.85;
		dexterity=2;
		recoil="recoil_trg20";
		maxZeroing=1000;

		class ItemInfo
		{
			priority=1;
		};
		descriptionShort="BR 55, chambered in the Expirimental 9.5 x 40mm cartridge";
		class Single: Mode_SemiAuto
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
					1.2,
					1,
					2000
				};
				begin2[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
					1.2,
					1,
					2000
				};
				begin3[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
					1.2,
					1,
					2000
				};
			begin4[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
				1.2,
				1,
				2000
			};
				soundBegin[]=
				{
					"begin1",
					0.1,
					"begin2",
					0.1,
					"begin3",
					0.1,
					"begin4",
					0.1
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
							2.2387211,
							1,
							1800
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
					0.79432821,
					1,
					400
				};
				begin2[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
					0.79432821,
					1,
					400
				};
				begin3[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
					0.79432821,
					1,
					400
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin1",
					0.34
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
							1,
							1,
							400
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime=0.1;
			recoil="recoil_single_mx";
			recoilProne="recoil_single_prone_mx";
			dispersion=0.00050000002;
			minRange=0;
			minRangeProbab=0.80000001;
			midRange=500;
			midRangeProbab=0.89999998;
			maxRange=1000;
			maxRangeProbab=0.60000002;
			aiRateOfFire=2;
			aiRateOfFireDistance=20;
		};
		class Burst: Mode_Burst
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
					1.2,
					1,
					2000
				};
				begin2[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
					1.2,
					1,
					2000
				};
				begin3[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
					1.2,
					1,
					2000
				};
			begin4[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
				1.2,
				1,
				2000
			};
				soundBegin[]=
				{
					"begin1",
					0.1,
					"begin2",
					0.1,
					"begin3",
					0.1,
					"begin4",
					0.1
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
							2.2387211,
							1,
							1800
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
					0.79432821,
					1,
					400
				};
				begin2[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
					0.79432821,
					1,
					400
				};
				begin3[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
					0.79432821,
					1,
					400
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin1",
					0.34
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
							1,
							1,
							400
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			soundBurst=0;
			burst=3;
			reloadTime=0.08;
			recoil="recoil_auto_mx";
			recoilProne="recoil_auto_prone_mx";
			dispersion=0.0005;
			minRange=1;
			minRangeProbab=0.80000001;
			midRange=500;
			midRangeProbab=0.89999998;
			maxRange=1000;
			maxRangeProbab=0.60000002;
			aiRateOfFire=1;
			aiRateOfFireDistance=10;
		};
	};
	class TFV_BR55_SB: TFV_BR55_SB_GL
	{
		author="Task Force Vargr Aux Team";
		_generalMacro="TFV_BR55_SB";
		baseWeapon="TFV_BR55_SB";
		scope=2;
		displayName="[Vargr] Battle Rifle Short";
		model="\UNSC_F_Weapons\weapons\UNSC_br55.p3d";
		mass=60;
		reloadAction="GestureReloadBR55";
		picture="\UNSC_F_Weapons\weapons\UI\BR55_UI.paa";
		UiPicture="\UNSC_F_Weapons\weapons\UI\BR55_UI.paa";
		handAnim[] = 
		{
			"OFP2_ManSkeleton",
			"\UNSC_F_Weapons\weapons\animations\br55_standing.rtm",
			"Spartan_ManSkeleton",
			"\OPTRE_Weapons\BR\data\anim\optre_BR_handanim_new_SPARTAN.rtm"
		};
		muzzles[]=
		{
			"this"
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass=60;
			class MuzzleSlot: MuzzleSlot
			{
				inkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
				displayName="Muzzle Slot";
				compatibleItems[]=
				{
					//"optre_MA5Suppressor",
					"19_UNSC_br55_Suppressor",
					"19_UNSC_br55L_Suppressor"
				};
				iconPosition[]={0,0.40000001};
			};
			class CowsSlot: CowsSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
				displayName="$STR_A3_CowsSlot0";
				compatibleitems[]=
				{
					"optre_M7_sight",
					"19_UNSC_br_scope",
					"Optre_Evo_Sight",
					"Optre_Evo_Sight_Riser"
				};
				iconPosition[]={0.5,0.30000001};
			};
			class PointerSlot: PointerSlot
			{
				iconPosition[]={0.2,0.40000001};
			};
		};
		inertia=0.5;
		aimTransitionSpeed=0.85;
		dexterity=2;
		recoil="recoil_trg20";
		maxZeroing=1000;
		class ItemInfo
		{
			priority=1;
		};
		descriptionShort="BR 55, chambered in the Expirimental 9.5 x 40mm cartridge";
		class Single: Mode_SemiAuto
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
					1.2,
					1,
					2000
				};
				begin2[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
					1.2,
					1,
					2000
				};
				begin3[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
					1.2,
					1,
					2000
				};
			begin4[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
				1.2,
				1,
				2000
			};
				soundBegin[]=
				{
					"begin1",
					0.1,
					"begin2",
					0.1,
					"begin3",
					0.1,
					"begin4",
					0.1
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
							2.2387211,
							1,
							1800
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
					0.79432821,
					1,
					400
				};
				begin2[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
					0.79432821,
					1,
					400
				};
				begin3[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
					0.79432821,
					1,
					400
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin1",
					0.34
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
							1,
							1,
							400
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime=0.1;
			recoil="recoil_single_mx";
			recoilProne="recoil_single_prone_mx";
			dispersion=0.00050000002;
			minRange=0;
			minRangeProbab=0.80000001;
			midRange=500;
			midRangeProbab=0.89999998;
			maxRange=1000;
			maxRangeProbab=0.60000002;
			aiRateOfFire=1;
			aiRateOfFireDistance=20;
		};
		class Burst: Mode_Burst
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
					1.2,
					1,
					2000
				};
				begin2[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
					1.2,
					1,
					2000
				};
				begin3[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
					1.2,
					1,
					2000
				};
			begin4[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
				1.2,
				1,
				2000
			};
				soundBegin[]=
				{
					"begin1",
					0.1,
					"begin2",
					0.1,
					"begin3",
					0.1,
					"begin4",
					0.1
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
							2.2387211,
							1,
							1800
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
					0.79432821,
					1,
					400
				};
				begin2[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
					0.79432821,
					1,
					400
				};
				begin3[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
					0.79432821,
					1,
					400
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin1",
					0.34
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
							1,
							1,
							400
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			soundBurst=0;
			burst=3;
			reloadTime=0.08;
			recoil="recoil_auto_mx";
			recoilProne="recoil_auto_prone_mx";
			dispersion=0.0005;
			minRange=1;
			minRangeProbab=0.80000001;
			midRange=500;
			midRangeProbab=0.89999998;
			maxRange=1000;
			maxRangeProbab=0.60000002;
			aiRateOfFire=1;
			aiRateOfFireDistance=10;
		};
	};
	class TFV_BR55_HB: TFV_BR55_SB_GL
	{
		author="Task Force Vargr Aux Team";
		_generalMacro="TFV_BR55_HB";
		baseWeapon="TFV_BR55_HB";
		scope=2;
		displayName="[Vargr] Battle Rifle Long";
		model="\UNSC_F_Weapons\weapons\UNSC_br55_HB.p3d";
		mass=80;
		reloadAction="GestureReloadBR55";
		picture="\UNSC_F_Weapons\weapons\UI\BR55_HB_UI.paa";
		UiPicture="\UNSC_F_Weapons\weapons\UI\BR55_HB_UI.paa";
		ACE_barrelLength=866;
		handAnim[] = 
		{
			"OFP2_ManSkeleton",
			"\UNSC_F_Weapons\weapons\animations\br55_standing.rtm",
			"Spartan_ManSkeleton",
			"\OPTRE_Weapons\BR\data\anim\optre_BR_handanim_new_SPARTAN.rtm"
		};
		magazines[]=
		{
			"TFV_BR55_AP_Magazine",
			"TFV_BR55_JHP_Magazine",
		};
		magazineWell[]=
		{
			"TFV_BR55_Magwell"
		};
		muzzles[]=
		{
			"this"
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass=80;
			class MuzzleSlot: MuzzleSlot
			{
				inkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
				displayName="Muzzle Slot";
				compatibleItems[]=
				{
					//"optre_MA5Suppressor",
					"19_UNSC_br55_Suppressor",
					"19_UNSC_br55L_Suppressor"
				};
				iconPosition[]={0,0.40000001};
			};
			class CowsSlot: CowsSlot
			{
				linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
				displayName="$STR_A3_CowsSlot0";
				compatibleitems[]=
				{
					"optre_M7_sight",
					"19_UNSC_br_scope",
					"Optre_Evo_Sight",
					"Optre_Evo_Sight_Riser"
				};
				iconPosition[]={0.5,0.30000001};
			};
			class PointerSlot: PointerSlot
			{
				iconPosition[]={0.2,0.40000001};
			};
		};
		inertia=0.5;
		aimTransitionSpeed=0.85;
		dexterity=2;
		recoil="recoil_trg20";
		maxZeroing=1000;
		class ItemInfo
		{
			priority=1;
		};
		descriptionShort="BR 55, chambered in the Expirimental 9.5 x 40mm cartridge";
		class Single: Mode_SemiAuto
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
					1.2,
					1,
					2000
				};
				begin2[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
					1.2,
					1,
					2000
				};
				begin3[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
					1.2,
					1,
					2000
				};
			begin4[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
				1.2,
				1,
				2000
			};
				soundBegin[]=
				{
					"begin1",
					0.1,
					"begin2",
					0.1,
					"begin3",
					0.1,
					"begin4",
					0.1
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
							2.2387211,
							1,
							1800
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
					0.79432821,
					1,
					400
				};
				begin2[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
					0.79432821,
					1,
					400
				};
				begin3[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
					0.79432821,
					1,
					400
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin1",
					0.34
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
							1,
							1,
							400
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime=0.1;
			recoil="recoil_single_mx";
			recoilProne="recoil_single_prone_mx";
			dispersion=0.0002;
			minRange=0;
			minRangeProbab=0.9;
			midRange=500;
			midRangeProbab=0.95;
			maxRange=1000;
			maxRangeProbab=0.8;
			aiRateOfFire=1;
			aiRateOfFireDistance=15;
		};
		class Burst: Mode_Burst
		{
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
					1.2,
					1,
					2000
				};
				begin2[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
					1.2,
					1,
					2000
				};
				begin3[]=
				{
					"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
					1.2,
					1,
					2000
				};
			begin4[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
				1.2,
				1,
				2000
			};
				soundBegin[]=
				{
					"begin1",
					0.1,
					"begin2",
					0.1,
					"begin3",
					0.1,
					"begin4",
					0.1
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
							2.2387211,
							1,
							1800
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
							1,
							1,
							1800
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				begin1[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
					0.79432821,
					1,
					400
				};
				begin2[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
					0.79432821,
					1,
					400
				};
				begin3[]=
				{
					"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
					0.79432821,
					1,
					400
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin1",
					0.34
				};
				class SoundTails
				{
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
							1,
							1,
							400
						};
						frequency=1;
						volume="interior";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
							1,
							1,
							400
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
				};
			};
			soundBurst=0;
			burst=3;
			reloadTime=0.1;
			recoil="recoil_auto_mx";
			recoilProne="recoil_auto_prone_mx";
			dispersion=0.0002;
			minRange=1;
			minRangeProbab=0.8;
			midRange=500;
			midRangeProbab=0.9;
			maxRange=1000;
			maxRangeProbab=0.6;
			aiRateOfFire=1;
			aiRateOfFireDistance=10;
		};
	};
	class TFV_BR55_HB_GL: TFV_BR55_SB_GL
{
	author="19th Fleet Mod Team - Split Jaw";
	_generalMacro="TFV_BR55_HB_GL";
	baseWeapon="TFV_BR55_HB_GL";
	scope=2;
	displayName="[Vargr] Battle Rifle Long w/UGL";
	model="\UNSC_F_Weapons\weapons\UNSC_br55_HB_gl.p3d";
	mass=100;
	//reloadAction="GestureReloadTRG";
	reloadAction="GestureReloadBR55";
	picture="\UNSC_F_Weapons\weapons\UI\BR55_HB_gl_UI.paa";
	UiPicture="\UNSC_F_Weapons\weapons\UI\BR55_HB_gl_UI.paa";
		ACE_barrelLength=866;
		handAnim[] = {
		"OFP2_ManSkeleton",
		"\UNSC_F_Weapons\weapons\animations\br55_gl_standing.rtm",
		"Spartan_ManSkeleton","\OPTRE_FC_Weapons\ConcussionRifle\data\anim\Spartan_ConcRifleHandAnim.rtm"
		};
	magazines[]=
	{
		"TFV_BR55_AP_Magazine",
		"TFV_BR55_JHP_Magazine",
	};
	magazineWell[]=
	{
		"TFV_BR55_Magwell";
	};
	muzzles[]=
	{
		"this",
		"GL_3GL_F"
	};
	class WeaponSlotsInfo: WeaponSlotsInfo
	{
		mass=100;
		class MuzzleSlot: MuzzleSlot
		{
			inkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
			displayName="Muzzle Slot";
			compatibleItems[]=
			{
				//"optre_MA5Suppressor",
				"19_UNSC_br55_Suppressor",
				"19_UNSC_br55L_Suppressor"
			};
			iconPosition[]={0,0.40000001};
		};
		class CowsSlot: CowsSlot
		{
			linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
			displayName="$STR_A3_CowsSlot0";
			compatibleitems[]=
			{
				//"optre_M7_sight",
				"optre_M7_sight",
				"19_UNSC_br_scope",
				"Optre_Evo_Sight",
				"Optre_Evo_Sight_Riser"
			};
			iconPosition[]={0.5,0.30000001};
		};
		class PointerSlot: PointerSlot
		{
			iconPosition[]={0.2,0.40000001};
		};
	};
	inertia=0.5;
	aimTransitionSpeed=0.85;
	dexterity=2;
	recoil="recoil_trg20";
	maxZeroing=1000;

	class ItemInfo
	{
		priority=1;
	};
	descriptionShort="BR 55, chambered in the Expirimental 9.5 x 40mm cartridge";
	class Single: Mode_SemiAuto
	{
		sounds[]=
		{
			"StandardSound",
			"SilencedSound"
		};
		class BaseSoundModeType
		{
			weaponSoundEffect="DefaultRifle";
			closure1[]={};
			closure2[]={};
			soundClosure[]=
			{
				"closure1",
				0.5,
				"closure2",
				0.5
			};
		};
		class StandardSound: BaseSoundModeType
		{
			begin1[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
				1.2,
				1,
				2000
			};
			begin2[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
				1.2,
				1,
				2000
			};
			begin3[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
				1.2,
				1,
				2000
			};
		begin4[]=
		{
			"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
			1.2,
			1,
			2000
		};
			soundBegin[]=
			{
				"begin1",
				0.1,
				"begin2",
				0.1,
				"begin3",
				0.1,
				"begin4",
				0.1
			};
			class SoundTails
			{
				class TailInterior
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
						2.2387211,
						1,
						1800
					};
					frequency=1;
					volume="interior";
				};
				class TailTrees
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*trees";
				};
				class TailForest
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*forest";
				};
				class TailMeadows
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*(meadows/2 max sea/2)";
				};
				class TailHouses
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*houses";
				};
			};
		};
		class SilencedSound: BaseSoundModeType
		{
			begin1[]=
			{
				"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
				0.79432821,
				1,
				400
			};
			begin2[]=
			{
				"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
				0.79432821,
				1,
				400
			};
			begin3[]=
			{
				"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
				0.79432821,
				1,
				400
			};
			soundBegin[]=
			{
				"begin1",
				0.33000001,
				"begin2",
				0.33000001,
				"begin1",
				0.34
			};
			class SoundTails
			{
				class TailInterior
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
						1,
						1,
						400
					};
					frequency=1;
					volume="interior";
				};
				class TailTrees
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*trees";
				};
				class TailForest
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*forest";
				};
				class TailMeadows
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*(meadows/2 max sea/2)";
				};
				class TailHouses
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*houses";
				};
			};
		};
		reloadTime=0.1;
		recoil="recoil_single_mx";
		recoilProne="recoil_single_prone_mx";
		dispersion=0.0002;
		minRange=0;
		minRangeProbab=0.9;
		midRange=500;
		midRangeProbab=0.95;
		maxRange=1000;
		maxRangeProbab=0.8;
		aiRateOfFire=1;
		aiRateOfFireDistance=15;
	};
	class Burst: Mode_Burst
	{
		sounds[]=
		{
			"StandardSound",
			"SilencedSound"
		};
		class BaseSoundModeType
		{
			weaponSoundEffect="DefaultRifle";
			closure1[]={};
			closure2[]={};
			soundClosure[]=
			{
				"closure1",
				0.5,
				"closure2",
				0.5
			};
		};
		class StandardSound: BaseSoundModeType
		{
			begin1[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_1.wss",
				1.2,
				1,
				2000
			};
			begin2[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_2.wss",
				1.2,
				1,
				2000
			};
			begin3[]=
			{
				"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_3.wss",
				1.2,
				1,
				2000
			};
		begin4[]=
		{
			"\UNSC_F_Weapons\weapons\data\Sounds\UNSC_br55_4.wss",
			1.2,
			1,
			2000
		};
			soundBegin[]=
			{
				"begin1",
				0.1,
				"begin2",
				0.1,
				"begin3",
				0.1,
				"begin4",
				0.1
			};
			class SoundTails
			{
				class TailInterior
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_interior",
						2.2387211,
						1,
						1800
					};
					frequency=1;
					volume="interior";
				};
				class TailTrees
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_trees",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*trees";
				};
				class TailForest
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_forest",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*forest";
				};
				class TailMeadows
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_meadows",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*(meadows/2 max sea/2)";
				};
				class TailHouses
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\mx_tail_houses",
						1,
						1,
						1800
					};
					frequency=1;
					volume="(1-interior/1.4)*houses";
				};
			};
		};
		class SilencedSound: BaseSoundModeType
		{
			begin1[]=
			{
				"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_01",
				0.79432821,
				1,
				400
			};
			begin2[]=
			{
				"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_02",
				0.79432821,
				1,
				400
			};
			begin3[]=
			{
				"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_short_03",
				0.79432821,
				1,
				400
			};
			soundBegin[]=
			{
				"begin1",
				0.33000001,
				"begin2",
				0.33000001,
				"begin1",
				0.34
			};
			class SoundTails
			{
				class TailInterior
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_Mx_tail_interior",
						1,
						1,
						400
					};
					frequency=1;
					volume="interior";
				};
				class TailTrees
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_trees",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*trees";
				};
				class TailForest
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_forest",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*forest";
				};
				class TailMeadows
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_meadows",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*(meadows/2 max sea/2)";
				};
				class TailHouses
				{
					sound[]=
					{
						"A3\Sounds_F\arsenal\weapons\Rifles\MX\Silencer_mx_tail_houses",
						1,
						1,
						400
					};
					frequency=1;
					volume="(1-interior/1.4)*houses";
				};
			};
		};
		soundBurst=0;
		burst=3;
		reloadTime=0.1;
		recoil="recoil_auto_mx";
		recoilProne="recoil_auto_prone_mx";
		dispersion=0.0005;
		minRange=1;
		minRangeProbab=0.8;
		midRange=500;
		midRangeProbab=0.9;
		maxRange=1000;
		maxRangeProbab=0.6;
		aiRateOfFire=1;
		aiRateOfFireDistance=10;
	};
};
};
class cfgMagazineWells
{
	class TFV_BR55_Magwell 
	{
		TFV_Rifle_Ammo[] = 
		{
			"TFV_BR55_AP_Magazine",
			"TFV_BR55_JHP_Magazine",
		};
	};
	class TFV_GL_3Rnd 
	{
		TFV_GL_Ammo[] = 
		{
			"TFV_GRL45_3rnd_HEAT",
			"TFV_GRL45_3rnd_Smoke",
			"TFV_GRL45_3rnd_Smoke_Blue",
			"TFV_GRL45_3rnd_Smoke_Purple",
			"TFV_GRL45_3rnd_Smoke_Green",
			"TFV_GRL45_3rnd_Smoke_Red",
			"ACE_HuntIR_M203",
			"TFV_Flare_Red",
			"TFV_Flare_Orange",
			"TFV_Flare_Yellow",
			"TFV_Flare_Green",
			"TFV_Flare_Blue",
			"TFV_Flare_Purple",
			"TFV_Flare_White",
		};
	};
};
class CfgMagazines
{
	class TFV_Flare_Red;
	class TFV_Flare_Orange;
	class TFV_Flare_Yellow;
	class TFV_Flare_Green;
	class TFV_Flare_Blue;
	class TFV_Flare_Purple;
	class TFV_Flare_White;
    class CA_Magazine;
	class TFV_BR55_AP_Magazine: CA_Magazine
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr]36Rnd 7.62x51mm AP Magazine";
	    ammo="TFV_Rifle_AP_Ammo";
	    count=36;
	    initSpeed=850;
	    tracersEvery=1;
	    descriptionShort="36-round magazine loaded with 7.62x51mm Armor-Piercing rounds.";
	    mass=5;
	    ace_isbelt=0;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
	    model="\OPTRE_Weapons\Ammo\BR55Ammo.p3d";
	    picture="\OPTRE_weapons\br\icons\magazine.paa";
	    lastRoundsTracer = 36;
	};

	class TFV_BR55_JHP_Magazine: CA_Magazine
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr]36Rnd 7.62x51mm JHP Magazine";
	    ammo="TFV_Rifle_JHP_Ammo";
	    count=36;
	    initSpeed=800;
	    tracersEvery=1;
	    descriptionShort="36-round magazine loaded with 7.62x51mm Jacketed Hollow Point rounds.";
	    mass=5;
	    ace_isbelt=0;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
	    model="\OPTRE_Weapons\Ammo\BR55Ammo.p3d";
	    picture="\OPTRE_weapons\br\icons\magazine.paa";
	    lastRoundsTracer = 36;
	};

	//GL's
	class 1Rnd_HE_Grenade_shell;
	class 1Rnd_SmokeRed_Grenade_shell;
	class TFV_GRL45_3rnd_HEAT: 1Rnd_HE_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_HEAT";
		displayName="TFV 3Rnd HEAT";
		displayNameShort="HEAT";
		descriptionshort="6 Round High Explosive Dual Purpose Grenades";
		count=3;
		mass=10;
	};
	class TFV_GRL45_3rnd_Smoke: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_Smoke";
		displayName="TFV 3Rnd Smoke (White)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=3;
		mass=10;
	};
	class TFV_GRL45_3rnd_Smoke_Blue: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokeBlue";
		displayName="TFV 3Rnd Smoke (Blue)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=3;
		mass=10;
	};
	class TFV_GRL45_3rnd_Smoke_Purple: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokePurple";
		displayName="TFV 3Rnd Smoke (Purple)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=3;
		mass=10;
	};
	class TFV_GRL45_3rnd_Smoke_Green: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokeGreen";
		displayName="TFV 3Rnd Smoke (Green)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=3;
		mass=10;
	};
	class TFV_GRL45_3rnd_Smoke_Red: 1Rnd_SmokeRed_Grenade_shell
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Mod";
		ammo="TFV_40mm_SmokeRed";
		displayName="TFV 3Rnd Smoke (Red)";
		displayNameShort="SMOKE";
		descriptionshort="6 Round Smoke Grenades";
		count=3;
		mass=10;
	};
};