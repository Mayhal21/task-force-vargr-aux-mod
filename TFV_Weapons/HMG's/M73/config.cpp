#define _ARMA_
//(13 Enums)
enum {
	destructengine = 2,
	destructdefault = 6,
	destructwreck = 7,
	destructtree = 3,
	destructtent = 4,
	stabilizedinaxisx = 1,
	stabilizedinaxesxyz = 4,
	stabilizedinaxisy = 2,
	stabilizedinaxesboth = 3,
	destructno = 0,
	stabilizedinaxesnone = 0,
	destructman = 5,
	destructbuilding = 1
};
class CfgPatches
{
	class TFV_Weapons_M73
	{
		units[] = {};
		weapons[] = 
		{
			"TFV_M73_HMG"
		};
		requiredVersion = 0.1;
		requiredAddons[] = {"OPTRE_Weapons"};
		addonRootClass = "TFV_Weapons";
		author = "Task Force Vargr Aux Team";
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class OPTRE_MuzzleSlot;
class OPTRE_CowsSlot_Rail;
class OPTRE_Pointers;
class OPTRE_UnderBarrelSlot_rail;
class CfgWeapons
{
	class LMG_Mk200_F;
	class TFV_MachineGun_Base:LMG_Mk200_F
	{
		class WeaponSlotsInfo;
	};
	class TFV_M73_HMG:TFV_MachineGun_Base
	{
		author = "Task Force Vargr Aux Team";
		scope = 2;
		scopeArsenal = 2;
		handAnim[] = {"OFP2_ManSkeleton","\OPTRE_Weapons\MG\data\anim\OPTRE_M73_handanim.rtm","Spartan_ManSkeleton","\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\M73_1_Spartan.rtm"};
		model = "\OPTRE_Weapons\MG\M73.p3d";
		displayName = "[Vargr] M73 Heavy Machine Gun";
		picture = "\OPTRE_weapons\MG\icons\M73_1.paa";
		pictureMjolnirHud = "\OPTRE_Suit_Scripts\textures\weaponIcons\MachineGuns\M73_icon.paa";
		magazines[] = 
		{
			"TFV_M73_AP_Magazine",
			"TFV_M73_JHP_Magazine", 
		};
		magazineWell[] = 
		{
			"TFV_M73_Magwell"
		};
		reloadAction = "GestureReloadM200";
		recoil = "recoil_lim";
		baseWeapon = "TFV_M73";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"OPTRE_Weapons\MG\data\M73_co.paa"};
		cursor = "TFV_M73_HMG";
		pictureWire = "\OPTRE_Weapons\data\Pictures\WireWeaponIcons\Prime\MachineGun\MG_IRON.paa";
		ODST_1 = "OPTRE_ODST_HUD_AmmoCount_LMG";
		Glasses = "OPTRE_GLASS_HUD_AmmoCount_LMG";
		Eye = "OPTRE_EYE_HUD_AmmoCount_LMG";
		HUD_BulletInARows = 4;
		HUD_TotalPosibleBullet = 200;
		class GunParticles
		{
			class EffectShotCloud
			{
				positionName = "Nabojnicestart";
				directionName = "Nabojniceend";
				effectName = "CaselessAmmoCloud";
			};
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 100;
			class MuzzleSlot:OPTRE_MuzzleSlot{};
			class CowsSlot:OPTRE_CowsSlot_Rail
				{
					compatibleitems[] = 
					{
						"optre_br55hb_scope",
						"OPTRE_M7_Sight",
						"OPTRE_Evo_Sight",
						"OPTRE_M73_SmartLink",
					};					
				};
			class PointerSlot:OPTRE_Pointers{};
			class UnderBarrelSlot:OPTRE_UnderBarrelSlot_rail{};
		};
		modes[] = {"FullAuto","Single","close","short","medium","far"};
		class Single: Mode_SemiAuto
		{
			sounds[] = {"StandardSound","SilencedSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "DefaultRifle";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				soundSetShot[] = {"OPTRE_LMG_SoundSet","SMGVermin_Tail_SoundSet","Zafir_InteriorTail_SoundSet"};
				begin3[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-03.ogg","db8",1,2000};
				begin4[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-04.ogg","db8",1,2000};
				begin5[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-05.ogg","db8",1,2000};
				begin6[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-06.ogg","db8",1,2000};
				begin7[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-07.ogg","db8",1,2000};
				begin8[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-08.ogg","db8",1,2000};
				begin9[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-09.ogg","db8",1,2000};
				begin10[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-10.ogg","db8",1,2000};
				soundBegin[] = {"begin3",0.2,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1,"begin10",0.1};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_trees",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_forest",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_meadows",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_houses",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[] = {"SyndikatLMG_silencerShot_SoundSet","SyndikatLMG_silencerTail_SoundSet","SyndikatLMG_silencerInteriorTail_SoundSet"};
				begin1[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_01",0.8912509,1,200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_02",0.8912509,1,200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_03",0.8912509,1,200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin1",0.34};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_trees",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_forest",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_meadows",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_houses",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime = 0.0789;
			dispersion = 0.00075;
			minRange = 2;
			minRangeProbab = 0.01;
			midRange = 200;
			midRangeProbab = 0.01;
			maxRange = 400;
			maxRangeProbab = 0.01;
		};
		class FullAuto: Mode_FullAuto
		{
			sounds[] = {"StandardSound","SilencedSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "DefaultRifle";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				soundSetShot[] = {"OPTRE_LMG_SoundSet","SMGVermin_Tail_SoundSet","Zafir_InteriorTail_SoundSet"};
				begin3[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-03.ogg","db8",1,2000};
				begin4[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-04.ogg","db8",1,2000};
				begin5[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-05.ogg","db8",1,2000};
				begin6[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-06.ogg","db8",1,2000};
				begin7[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-07.ogg","db8",1,2000};
				begin8[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-08.ogg","db8",1,2000};
				begin9[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-09.ogg","db8",1,2000};
    			begin10[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-10.ogg","db8",1,2000};
				soundBegin[] = {"begin3",0.1,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1,"begin10",0.1};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_trees",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_forest",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_meadows",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_houses",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
       		};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[] = {"SyndikatLMG_silencerShot_SoundSet","SyndikatLMG_silencerTail_SoundSet","SyndikatLMG_silencerInteriorTail_SoundSet"};
				begin1[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_01",0.8912509,1,200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_02",0.8912509,1,200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_03",0.8912509,1,200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin1",0.34};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_trees",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_forest",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_meadows",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
    					sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_houses",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime = 0.0789;
			dispersion = 0.00075;
			minRange = 2;
			minRangeProbab = 0.01;
			midRange = 200;
			midRangeProbab = 0.01;
			maxRange = 400;
			maxRangeProbab = 0.01;
		};
		class close: FullAuto
		{
			burst = 10;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
    		minRange = 0;
			minRangeProbab = 0.05;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.04;
			showToPlayer = 0;
		};
		class short: close
		{
			burst = 10;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 300;
			minRange = 50;
			minRangeProbab = 0.05;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 300;
			maxRangeProbab = 0.04;
		};
		class medium: close
		{
			burst = 10;
			aiRateOfFire = 4;
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.05;
			midRange = 400;
			midRangeProbab = 0.6;
			maxRange = 600;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			burst = 10;
			aiRateOfFire = 6;
			aiRateOfFireDistance = 700;
			minRange = 350;
			minRangeProbab = 0.04;
			midRange = 550;
			midRangeProbab = 0.5;
			maxRange = 700;
			maxRangeProbab = 0.01;
		};
       	aiDispersionCoefY = 10;
		aiDispersionCoefX = 10;
	};
};
class cfgMagazineWells
{
	class TFV_M73_Magwell
	{
		TFV_LMG_Ammo[] = 
		{
			"TFV_M73_AP_Magazine",
			"TFV_M73_JHP_Magazine", 
		};
	};
};

class cfgMagazines
{
    class 150Rnd_762x51_Box;
	class TFV_M73_AP_Magazine:150Rnd_762x51_Box
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr]250Rnd 7.62x51mm JHP Magazine";
	    ammo="TFV_HMG_AP_Ammo";
	    count=250;
	    initSpeed=930;
	    tracersEvery=1;
	    descriptionShort="250-round magazine loaded with .50 caliber Armor-Piercing rounds.";
	    mass=5;
	    ace_isbelt=1;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
	    model="\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
	    picture="\OPTRE_Weapons\MG\icons\magazine.paa";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	    lastRoundsTracer = 250;
	};
	class TFV_M73_JHP_Magazine:150Rnd_762x51_Box
	{
		author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr]250Rnd 7.62x51mm JHP Magazine";
	    ammo="TFV_HMG_JHP_Ammo";
	    count=250
	    initSpeed=900;
	    tracersEvery=1;
	    descriptionShort="250-round magazine loaded with .50 caliber Jacketed Hollow Point rounds.";
	    mass=5;
	    ace_isbelt=1;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
	    model="\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
	    picture="\OPTRE_Weapons\MG\icons\magazine.paa";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	    lastRoundsTracer = 250;
	};
};
