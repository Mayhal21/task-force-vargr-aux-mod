class CfgPatches
{
	class TFV_Weapons
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
            "TFV_Spartan_Laser",
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class CfgWeapons
{
	class OPTRE_UnguidedLauncher_Base;
	class TFV_Spartan_Laser: OPTRE_UnguidedLauncher_Base
	{
		author="Task Force Vargr Aux Team";
		scope=2;
		scopeArsenal=2;
		displayname="[TFV] Spartan Laser";
		descriptionshort="M6V Grindel/Galilean Non-Linear Rifle";
		magazines[]=
		{
			"TFV_Laser_Mag"
		};
		magazineWell[]={};
		picture="\OPTRE_Weapons\M6GGNR\data\ui\splaser.paa";
		model="\OPTRE_Weapons\M6GGNR\data\spartan_laser.p3d";
		handAnim[]=
		{
			"OFP2_ManSkeleton",
			"\OPTRE_Weapons\M6GGNR\data\spartan_laser.rtm",
			"Spartan_ManSkeleton",
			"\OPTRE_Weapons\M6GGNR\data\anim\handanim_splaser.rtm"
		};
		opticsZoomMin=0.125;
		opticsZoomMax=0.025;
		opticsZoomInit=0.125;
		discretefov[]=
		{
			"2 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')",
			"4 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')",
			"10 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')"
		};
		modelOptics[]=
		{
			"\OPTRE_Weapons\M6GGNR\data\ui\M6V_Optic_2x.p3d",
			"\OPTRE_Weapons\M6GGNR\data\ui\M6V_Optic_4x.p3d",
			"\OPTRE_Weapons\M6GGNR\data\ui\M6V_Optic_10x.p3d"
		};
		baseWeapon="TFV_Spartan_Laser";
		cursor="OPTRE_Splaser";
		pictureWire="\OPTRE_Weapons\M6GGNR\data\ui\splaserwire.paa";
		ODST_1="OPTRE_ODST_HUD_AmmoCount_AR";
		Glasses="OPTRE_ODST_HUD_AmmoCount_AR";
		Eye="OPTRE_ODST_HUD_AmmoCount_AR";
		HUD_BulletInARows=4;
		HUD_TotalPosibleBullet=4;
		ace_overpressure_angle=0;
		ace_overpressure_range=0;
		ace_overpressure_damage=0;
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"OPTRE_Weapons\M6GGNR\data\Spartan_Laser_Camo1_co.paa",
			"OPTRE_Weapons\M6GGNR\data\Spartan_Laser_Camo2_co.paa"
		};
		class WeaponSlotsInfo
		{
			mass=150;
		};
		class GunParticles
		{
		};
		class OpticsModes
		{
			class StepScope
			{
				opticsID=1;
				useModelOptics=1;
				opticsPPEffects[]=
				{
					"OpticsCHAbera1",
					"OpticsBlur1"
				};
				opticsFlare=0;
				opticsZoomMin=0.125;
				opticsZoomMax=0.025;
				opticsZoomInit=0.125;
				distanceZoomMin=300;
				distanceZoomMax=300;
				memoryPointCamera="eye";
				cameraDir="look";
				visionMode[]=
				{
					"Normal",
					"NVG",
					"Ti"
				};
				thermalMode[]={0};
				opticsDisablePeripherialVision=1;
				discretefov[]=
				{
					"2 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')",
					"4 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')",
					"10 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')"
				};
				discreteInitIndex=0;
			};
		};
		modes[]=
		{
			"Single"
		};
		recoil="Empty";
		class Single: Mode_SemiAuto
		{
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"OPTRE_Weapons\Vehicle\data\sounds\Laser_1.wss",
					2.5,
					1,
					1500
				};
				soundBegin[]=
				{
					"begin1",
					1
				};
			};
			soundContinuous=0;
			aiRateOfFire=1;
			reloadTime=10;
			magazineReloadTime=15;
			autoReload=0;
			reloadAction="";
			minRange=50;
			minRangeProbab=0.40000001;
			midRange=1000;
			midRangeProbab=0.40000001;
			maxRange=1500;
			maxRangeProbab=0.2;
		};
		canLock=0;
		weaponLockDelay=2;
		weaponLockSystem="2 + 16";
		cmImmunity=0.25;
		lockAcquire=1;
		lockingTargetSound[]=
		{
			"A3\Sounds_F\arsenal\weapons\Launchers\Titan\locking_Titan",
			0.31622776,
			1
		};
		lockedTargetSound[]=
		{
			"A3\Sounds_F\arsenal\weapons\Launchers\Titan\locked_Titan",
			0.31622776,
			2.5
		};
	};
};
class CfgMagazines
{
	class CA_Magazine;
	class TFV_Laser_Mag: CA_Magazine
	{
		scope=2;
		scopeArsenal=2;
		displayName="[TFV] Spartan Laser";
		displayNameShort="Laser";
		picture="\OPTRE_Weapons\M6GGNR\data\splaser_mag.paa";
		ammo="TFV_Laser_Ammo";
		initSpeed=2000;
		count=12;
		muzzleImpulseFactor[]={0,0};
		mass=1500;
		model="\OPTRE_Weapons\Ammo\boxAmmo.p3d";
		modelSpecial="\OPTRE_Weapons\Ammo\boxAmmo.p3d";
		modelSpecialIsProxy=1;
		hiddenSelections[]=
		{
			"camo"
		};
		hiddenSelectionsTextures[]=
		{
			"\OPTRE_Weapons\Ammo\data\SpartanLaser_Ammo_CO.paa"
		};
	};
};
class CfgAmmo
{
	class OPTRE_M41_Rocket_HEAT;
	class TFV_Laser_Ammo: OPTRE_M41_Rocket_HEAT
	{
		coefGravity=0;
		scope=2;
		scopeArsenal=2;
		caliber=1;
		hit=2000;
		indirectHit=300;
		explosive=0.80000001;
		indirectHitRange=4;
		fuseDistance=1;
		typicalSpeed=1500;
		maxSpeed=2000;
		model="OPTRE_Weapons\data\spartan_laser.p3d";
		airFriction=0;
		sideAirFriction=0;
	};
};
class cfgMods
{
	author="Task Force Vargr Aux Team";
	timepacked="";
};
