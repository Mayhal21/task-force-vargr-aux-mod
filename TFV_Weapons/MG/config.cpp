#define _ARMA_
//(13 Enums)
enum {
	destructengine = 2,
	destructdefault = 6,
	destructwreck = 7,
	destructtree = 3,
	destructtent = 4,
	stabilizedinaxisx = 1,
	stabilizedinaxesxyz = 4,
	stabilizedinaxisy = 2,
	stabilizedinaxesboth = 3,
	destructno = 0,
	stabilizedinaxesnone = 0,
	destructman = 5,
	destructbuilding = 1
};
class CfgPatches
{
class TFV_Weapons_MG
	{
		units[] = {};
		weapons[] = {"OPTRE_M73_SmartLink","OPTRE_M73","OPTRE_M247"};
		requiredVersion = 0.1;
		requiredAddons[] = {"OPTRE_Weapons"};
		addonRootClass = "TFV_Weapons";
		author = "Task Force Vargr Aux Team";
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class OPTRE_MuzzleSlot;
class OPTRE_CowsSlot_Rail;
class OPTRE_Pointers;
class OPTRE_UnderBarrelSlot_rail;
class CfgWeapons
{
	class optic_Aco;
	class InventoryOpticsItem_Base_F;
	class LMG_Mk200_F;
	class TFV_MachineGun_Base:LMG_Mk200_F
	{
		class WeaponSlotsInfo;
	};
	class TFV_M73_SL:optic_Aco
	{
		author = "Task Force Vargr Aux Team";
		scope = 2;
		displayName = "[TFV] M73 Smart Link Scope";
		picture = "\OPTRE_weapons\MG\icons\scope.paa";
		model = "\OPTRE_Weapons\MG\M73_SmartLink.p3d";
		weaponInfoType = "RscWeaponZeroing";
		inertia = 0.1;
		class ItemInfo: InventoryOpticsItem_Base_F
		{
			mass = 10;
			opticType = 1;
			optics = 1;
			modelOptics = "\OPTRE_Weapons\MG\M73_SmartLink.p3d";
			class OpticsModes
			{
				class M73_IS
				{
					opticsID = 1;
					useModelOptics = 0;
					opticsPPEffects[] = {""};
					opticsFlare = 0;
					opticsDisablePeripherialVision = 0;
					opticsZoomMin = 0.375;
					opticsZoomMax = 0.55;
					opticsZoomInit = 0.55;
					memoryPointCamera = "opticView";
					visionMode[] = {"Normal","NVG"};
					distanceZoomMin = 300;
					distanceZoomMax = 300;
				};
				class M73_Scope:M73_IS
				{
					opticsID = 2;
					useModelOptics = 1;
					opticsZoomMin = 0.125;
					opticsZoomMax = 0.0525;
    				opticsZoomInit = 0.125;
					discretefov[] = {0.125,0.0525};
					discreteinitIndex = 0;
					discreteDistance[] = {100,300,400,500,600,700,800,900,1000};
					discreteDistanceInitIndex = 1;
					distanceZoomMin = 100;
					distanceZoomMax = 1000;
					memoryPointCamera = "opticView2";
					modelOptics[] = {"\OPTRE_Weapons\MG\MG_Optic_2x.p3d","\OPTRE_Weapons\MG\MG_Optic_4x.p3d"};
					visionMode[] = {"Normal","NVG"};
				};
			};
		};
	};
	class TFV_M73:TFV_MachineGun_Base
	{
		author = "Task Force Vargr Aux Team";
		scope = 2;
		scopeArsenal = 2;
		handAnim[] = {"OFP2_ManSkeleton","\OPTRE_Weapons\MG\data\anim\OPTRE_M73_handanim.rtm","Spartan_ManSkeleton","\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\M73_1_Spartan.rtm"};
		model = "\OPTRE_Weapons\MG\M73.p3d";
		displayName = "[TFV] M73 Light Machine Gun";
		picture = "\OPTRE_weapons\MG\icons\M73_1.paa";
		pictureMjolnirHud = "\OPTRE_Suit_Scripts\textures\weaponIcons\MachineGuns\M73_icon.paa";
		magazines[] = {"TFV_100Rnd_95x40_Box","TFV_200Rnd_95x40_Box"};
		magazineWell[] = {"TFV_Magwell_M73"};
		reloadAction = "GestureReloadM200";
		recoil = "recoil_lim";
		baseWeapon = "TFV_M73";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"OPTRE_Weapons\MG\data\M73_co.paa"};
		cursor = "TFV_M73";
		pictureWire = "\OPTRE_Weapons\data\Pictures\WireWeaponIcons\Prime\MachineGun\MG_IRON.paa";
		ODST_1 = "OPTRE_ODST_HUD_AmmoCount_LMG";
		Glasses = "OPTRE_GLASS_HUD_AmmoCount_LMG";
		Eye = "OPTRE_EYE_HUD_AmmoCount_LMG";
		HUD_BulletInARows = 4;
		HUD_TotalPosibleBullet = 200;
		class GunParticles
		{
			class EffectShotCloud
			{
				positionName = "Nabojnicestart";
				directionName = "Nabojniceend";
				effectName = "CaselessAmmoCloud";
			};
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 100;
			class MuzzleSlot:OPTRE_MuzzleSlot{};
			class CowsSlot:OPTRE_CowsSlot_Rail
				{
					compatibleitems[] = {"OPTRE_M73_SmartLink","Optre_Evo_Sight","Optre_Evo_Sight_Riser"};					
				};
			class PointerSlot:OPTRE_Pointers{};
			class UnderBarrelSlot:OPTRE_UnderBarrelSlot_rail{};
		};
		modes[] = {"FullAuto","Single","close","short","medium","far"};
		class Single: Mode_SemiAuto
		{
			sounds[] = {"StandardSound","SilencedSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "DefaultRifle";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				soundSetShot[] = {"OPTRE_LMG_SoundSet","SMGVermin_Tail_SoundSet","Zafir_InteriorTail_SoundSet"};
				begin3[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-03.ogg","db8",1,2000};
				begin4[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-04.ogg","db8",1,2000};
				begin5[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-05.ogg","db8",1,2000};
				begin6[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-06.ogg","db8",1,2000};
				begin7[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-07.ogg","db8",1,2000};
				begin8[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-08.ogg","db8",1,2000};
				begin9[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-09.ogg","db8",1,2000};
				begin10[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-10.ogg","db8",1,2000};
				soundBegin[] = {"begin3",0.2,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1,"begin10",0.1};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_trees",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_forest",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_meadows",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_houses",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[] = {"SyndikatLMG_silencerShot_SoundSet","SyndikatLMG_silencerTail_SoundSet","SyndikatLMG_silencerInteriorTail_SoundSet"};
				begin1[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_01",0.8912509,1,200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_02",0.8912509,1,200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_03",0.8912509,1,200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin1",0.34};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_trees",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_forest",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_meadows",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_houses",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime = 0.0789;
			dispersion = 0.00075;
			minRange = 2;
			minRangeProbab = 0.01;
			midRange = 200;
			midRangeProbab = 0.01;
			maxRange = 400;
			maxRangeProbab = 0.01;
		};
		class FullAuto: Mode_FullAuto
		{
			sounds[] = {"StandardSound","SilencedSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "DefaultRifle";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				soundSetShot[] = {"OPTRE_LMG_SoundSet","SMGVermin_Tail_SoundSet","Zafir_InteriorTail_SoundSet"};
				begin3[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-03.ogg","db8",1,2000};
				begin4[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-04.ogg","db8",1,2000};
				begin5[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-05.ogg","db8",1,2000};
				begin6[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-06.ogg","db8",1,2000};
				begin7[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-07.ogg","db8",1,2000};
				begin8[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-08.ogg","db8",1,2000};
				begin9[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-09.ogg","db8",1,2000};
    			begin10[] = {"\OPTRE_Weapons\MG\data\sounds\M73LMG-10.ogg","db8",1,2000};
				soundBegin[] = {"begin3",0.1,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1,"begin10",0.1};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_trees",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_forest",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_meadows",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_houses",1,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
       		};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[] = {"SyndikatLMG_silencerShot_SoundSet","SyndikatLMG_silencerTail_SoundSet","SyndikatLMG_silencerInteriorTail_SoundSet"};
				begin1[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_01",0.8912509,1,200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_02",0.8912509,1,200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_short_03",0.8912509,1,200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin1",0.34};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_trees",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_forest",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_meadows",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
    					sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\Silencer_Mk200_tail_houses",1,1,1800};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime = 0.0789;
			dispersion = 0.00075;
			minRange = 2;
			minRangeProbab = 0.01;
			midRange = 200;
			midRangeProbab = 0.01;
			maxRange = 400;
			maxRangeProbab = 0.01;
		};
		class close: FullAuto
		{
			burst = 10;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
    		minRange = 0;
			minRangeProbab = 0.05;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.04;
			showToPlayer = 0;
		};
		class short: close
		{
			burst = 10;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 300;
			minRange = 50;
			minRangeProbab = 0.05;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 300;
			maxRangeProbab = 0.04;
		};
		class medium: close
		{
			burst = 10;
			aiRateOfFire = 4;
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.05;
			midRange = 400;
			midRangeProbab = 0.6;
			maxRange = 600;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			burst = 10;
			aiRateOfFire = 6;
			aiRateOfFireDistance = 700;
			minRange = 350;
			minRangeProbab = 0.04;
			midRange = 550;
			midRangeProbab = 0.5;
			maxRange = 700;
			maxRangeProbab = 0.01;
		};
       	aiDispersionCoefY = 10;
		aiDispersionCoefX = 10;
	};
	class TFV_M247:TFV_M73
	{
		author = "Task Force Vargr Aux Team";
		scope = 2;
		scopeArsenal = 2;
		handAnim[] = {"OFP2_ManSkeleton","\OPTRE_Weapons\MG\data\anim\OPTRE_M247_handanim.rtm","Spartan_ManSkeleton","\OPTRE_MJOLNIR\data\anims\OPTRE_anims\Weapons\M247_Spartan.rtm"};
		model = "\OPTRE_Weapons\MG\M247.p3d";
		displayName = "[TFV] M247 Heavy Machine Gun";
		magazines[] = {"TFV_100Rnd_M247_Ball","TFV_200Rnd_M247_Ball"};
		magazineWell[] = {"TFV_Magwell_M247"};
		recoil = "recoil_mk200";
		baseWeapon = "TFV_M247";
		picture = "\OPTRE_Weapons\MG\icons\m247_icon.paa";
		pictureMjolnirHud = "\OPTRE_Suit_Scripts\textures\weaponIcons\MachineGuns\M247_icon.paa";
		hiddenSelections[] = {"mainbody1","mainbody2","magazine","stock","details","barrel_sights"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M247_mainbody1_CO.paa","\OPTRE_Weapons\MG\data\M247_mainbody2_CO.paa","\OPTRE_Weapons\MG\data\M247_magazine_CO.paa","\OPTRE_Weapons\MG\data\M247_stock_CO.paa","\OPTRE_Weapons\MG\data\M247_details_CO.paa","\OPTRE_Weapons\MG\data\M247_barrel_sights_CO.paa"};
		HUD_BulletInARows = 2;
		HUD_TotalPosibleBullet = 100;
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 150;
		};
		reloadMagazineSound[] = {"\OPTRE_Wbk_WeaponImprovements\reload\lmg_rightSide.ogg",2,1,25};
		reloadAction = "WBK_LMG_Mag_RightSide_reload";
		class Single: Single
		{
			class StandardSound: StandardSound
			{
				soundSetShot[] = {"OPTRE_LMG_SoundSet","SMGVermin_Tail_SoundSet","Zafir_InteriorTail_SoundSet"};
				begin1[] = {"\OPTRE_Weapons\MG\data\sounds\M247_1.wss",1,1,2000};
				begin2[] = {"\OPTRE_Weapons\MG\data\sounds\M247_2.wss",1,1,2000};
			};
			reloadTime = 0.125;
			dispersion = 0.00075;
		};
		class FullAuto: FullAuto
		{
			class StandardSound: StandardSound
			{
				soundSetShot[] = {"OPTRE_LMG_SoundSet","SMGVermin_Tail_SoundSet","Zafir_InteriorTail_SoundSet"};
				begin1[] = {"\OPTRE_Weapons\MG\data\sounds\M247_1.wss",1,1,2000};
				begin2[] = {"\OPTRE_Weapons\MG\data\sounds\M247_2.wss",1,1,2000};
			};
			reloadTime = 0.125;
			dispersion = 0.00075;
    	};
	};
	class TFV_M73_AC:TFV_M73
	{
		author = "Article 2 Studios";
		scope = 1;
		scopeArsenal = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				slot = "CowsSlot";
				item = "OPTRE_M73_SmartLink";
			};
    		class LinkedItemsUnder
			{
				slot = "UnderBarrelSlot";
				item = "bipod_01_F_blk";
			};
		};
	};
	class TFV_M247_AC:TFV_M247
	{
		dlc = "OPTRE";
		author = "Article 2 Studios";
		scope = 1;
		scopeArsenal = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				slot = "CowsSlot";
				item = "OPTRE_M393_ACOG";
			};
			class LinkedItemsUnder
			{
				slot = "UnderBarrelSlot";
    			item = "bipod_01_F_blk";
			};
		};
	};
};
class cfgMagazineWells
{
	class TFV_Magwell_M73
	{
		UNSC_762x51_32rnd[] = 
		{
			"TFV_100Rnd_95x40_Box",
			"TFV_200Rnd_95x40_Box", 
			"TFV_100Rnd_95x40_Box_Tracer",
			"TFV_200Rnd_95x40_Box_Tracer"
		};
	};
	class TFV_Magwell_M247
	{
		UNSC_762x51_32rnd[] = 
		{
			"TFV_100Rnd_M247_Ball",
			"TFV_200Rnd_M247_Ball", 
			"TFV_100Rnd_M247_Ball_Tracer", 
			"TFV_200Rnd_M247_Ball_Tracer"
		};
	};
};

class cfgMagazines
{
    class 150Rnd_762x51_Box;
	class OPTRE_100Rnd_762x51_Box;
    class OPTRE_100Rnd_95x40_Box;
	class TFV_100Rnd_95x40_Box:150Rnd_762x51_Box
	{
		scope=2;
		scopearsenal=2;
		dlc = "OPTRE";
		displayname = "[TFV] 100Rnd 9.5x40mm Box Magazine";
<<<<<<< HEAD
=======
		displaynameshort = "9.5x40mm";
		ammo = "TFV_B_95x40_Ball";
		count = 100;
		initspeed = 925;
		picture = "\OPTRE_Weapons\MG\icons\magazine.paa";
		descriptionshort = "100 Round Box Magazine<br/>9.5x40mm";
		mass = 35;
		tracersEvery = 0;
		lastRoundsTracer = 100;
		model = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	};
	class TFV_100Rnd_95x40_Box_Tracer:150Rnd_762x51_Box
	{
		scope=0;
		scopearsenal=0;
		dlc = "OPTRE";
		displayname = "[TFV] 100Rnd 9.5x40mm Box Magazine [Tracer]";
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		displaynameshort = "9.5x40mm";
		ammo = "TFV_B_95x40_Ball";
		count = 100;
		initspeed = 925;
		picture = "\OPTRE_Weapons\MG\icons\magazine.paa";
		descriptionshort = "100 Round Box Magazine<br/>9.5x40mm";
		mass = 20;
		tracersEvery = 0;
		lastRoundsTracer = 100;
		model = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	};
	class TFV_100Rnd_95x40_Box_Tracer:150Rnd_762x51_Box
	{
		scope=2;
		scopearsenal=2;
		dlc = "OPTRE";
		displayname = "[TFV] 100Rnd 9.5x40mm Box Magazine [Tracer]";
		displaynameshort = "9.5x40mm";
		ammo = "TFV_B_95x40_Ball";
		count = 100;
		initspeed = 925;
		picture = "\OPTRE_Weapons\MG\icons\magazine.paa";
		descriptionshort = "100 Round Box Magazine<br/>9.5x40mm";
		mass = 20;
		tracersEvery = 1;
		lastRoundsTracer = 100;
		model = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	};
	class TFV_200Rnd_95x40_Box:150Rnd_762x51_Box
	{
		scope=2;
		scopearsenal=2;
		dlc = "OPTRE";
		displayname = "[TFV] 200Rnd 9.5x40mm Box Magazine";
<<<<<<< HEAD
=======
		displaynameshort = "9.5x40mm";
		ammo = "TFV_B_95x40_Ball";
		count = 200;
		initspeed = 925;
		picture = "\OPTRE_Weapons\MG\icons\magazine.paa";
		descriptionshort = "100 Round Box Magazine<br/>9.5x40mm";
		mass = 70;
		tracersEvery = 0;
		lastRoundsTracer = 200;
		model = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	};
	class TFV_200Rnd_95x40_Box_Tracer:150Rnd_762x51_Box
	{
		scope=0;
		scopearsenal=0;
		dlc = "OPTRE";
		displayname = "[TFV] 200Rnd 9.5x40mm Box Magazine [Tracer]";
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		displaynameshort = "9.5x40mm";
		ammo = "TFV_B_95x40_Ball";
		count = 200;
		initspeed = 925;
		picture = "\OPTRE_Weapons\MG\icons\magazine.paa";
		descriptionshort = "200 Round Box Magazine<br/>9.5x40mm";
		mass = 40;
		tracersEvery = 0;
		lastRoundsTracer = 200;
		model = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	};
	class TFV_200Rnd_95x40_Box_Tracer:150Rnd_762x51_Box
	{
		scope=2;
		scopearsenal=2;
		dlc = "OPTRE";
		displayname = "[TFV] 200Rnd 9.5x40mm Box Magazine [Tracer]";
		displaynameshort = "9.5x40mm";
		ammo = "TFV_B_95x40_Ball";
		count = 200;
		initspeed = 925;
		picture = "\OPTRE_Weapons\MG\icons\magazine.paa";
		descriptionshort = "200 Round Box Magazine<br/>9.5x40mm";
		mass = 40;
		tracersEvery = 1;
		lastRoundsTracer = 200;
		model = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecial = "\OPTRE_Weapons\MG\m73_mag_proxy.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\MG\data\M73_100rndBox_co.paa"};
	};
    class TFV_100Rnd_M247_Ball:OPTRE_100Rnd_762x51_Box
	{
		scope=2;
		scopearsenal=2;
		dlc="OPTRE";
		displayName="[TFV] 100Rnd 12.7x99 Box Magazine";
		displayNameShort="12.7x99";
		ammo="TFV_M247_Ball_Ammo";
		initSpeed=750;
		count=100;
		tracersEvery=0;
		lastRoundsTracer=100;
<<<<<<< HEAD
        mass=20;
=======
        mass=75;
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		descriptionshort="100 Round Box Magazine of 12.7x99";
	};
    class TFV_100Rnd_M247_Ball_Tracer:OPTRE_100Rnd_762x51_Box
	{
<<<<<<< HEAD
		scope=2;
		scopearsenal=2;
=======
		scope=0;
		scopearsenal=0;
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		dlc="OPTRE";
		displayName="[TFV] 100Rnd 12.7x99 Box Magazine [Tracer]";
		displayNameShort="12.7x99";
		ammo="TFV_M247_Ball_Ammo";
		initSpeed=750;
		count=100;
		tracersEvery=1;
		lastRoundsTracer=100;
<<<<<<< HEAD
        mass=20;
=======
        mass=75;
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		descriptionshort="100 Round Box Magazine of 12.7x99";
    };
	class TFV_200Rnd_M247_Ball:OPTRE_100Rnd_762x51_Box
	{
		scope=2;
		scopearsenal=2;
		dlc="OPTRE";
		displayName="[TFV] 250Rnd 12.7x99 Box Magazine";
		displayNameShort="12.7x99";
		ammo="TFV_M247_Ball_Ammo";
		initSpeed=600;
		count=250;
		tracersEvery=0;
		lastRoundsTracer=200;
<<<<<<< HEAD
        mass=50;
=======
        mass=150;
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		descriptionshort="250 Round Box Magazine of 12.7x99";
    };

	class TFV_200Rnd_M247_Ball_Tracer:OPTRE_100Rnd_762x51_Box
	{
<<<<<<< HEAD
		scope=2;
		scopearsenal=2;
=======
		scope=0;
		scopearsenal=0;
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		dlc="OPTRE";
		displayName="[TFV] 250Rnd 12.7x99 Box Magazine [Tracer]";
		displayNameShort="12.7x99";
		ammo="TFV_M247_Ball_Ammo";
		initSpeed=600;
		count=250;
		tracersEvery=1;
		lastRoundsTracer=200;
<<<<<<< HEAD
        mass=50;
=======
        mass=150;
>>>>>>> ae9ae0a32438dd2e4b4d42229ce198cf51c74988
		descriptionshort="250 Round Box Magazine of 12.7x99";
    };
};
    class CfgAmmo
{
    class B_127x99_Ball;
    class B_762x51_Ball;
	class TFV_B_95x40_Ball:B_762x51_Ball
	{
		caliber = 4.0;
		hit = 13;
		typicalSpeed = 600;
		cartridge = "FxCartridge_93x64_Ball";
		model = "\A3\Weapons_f\Data\bullettracer\tracer_red";
	};
    class TFV_M247_Ball_Ammo: B_127x99_Ball
	{
		model="\A3\Weapons_f\Data\bullettracer\tracer_yellow";
		tracerStartTime=0;
		tracerEndTime=10;
		caliber=8;
		aiAmmoUsageFlags="64 + 128 + 256";
		cost=15;
		airFriction=-0.00036000001;
		hit=50;
		indirectHit=0;
		indirectHitRange=0;
		typicalSpeed=1215;
		tracerScale=1.3;
	};
};