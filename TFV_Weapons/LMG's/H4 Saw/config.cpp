class CfgPatches
{
	class TFV_Weapons_H4
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_H4_LMG",
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{
	class arifle_Mk20_F;
	class Pistol_Base_F;
	class WeaponSlotsInfo;
	class UGL_F;
	class TFV_Rifle_Base: arifle_Mk20_F
	{
		scope=1;
		displayName="-";
		recoil="";
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			allowedSlots[]={701,901};
			mass=1;
			class MuzzleSlot: MuzzleSlot
			{
			};
			class CowsSlot: CowsSlot
			{
			};
			class PointerSlot: PointerSlot
			{
			};
			class UnderBarrelSlot: UnderBarrelSlot
			{
			};
		};
	};
	class TFV_H4_LMG: TFV_Rifle_Base
	{
		scope=2;
		scopeArsenal=2;
		ace_overheating_mrbs=3000000000;
		ACE_Overheating_SlowdownFactor=0;
		ACE_Overheating_JamChance=0;
		ACE_Overheating_Dispersion=0;
		ACE_overheating_allowSwapBarrel=0;
		author="Task Force Vargr Aux Team";
		model="MA_Weapons\data\SAW.p3d";
		displayName="[Vargr] H4 Light Machine gun";
		descriptionShort="UNSC Squad Automatic Weapon";
		canShootInWater=1;
		baseweapon="TFV_H4_LMG";
		cursor="LMG_Crosshair";
		handAnim[]=
		{
			"OFP2_ManSkeleton",
			"\A3\Weapons_F\Rifles\MX\data\Anim\MX_gl.rtm"
		};
		ace_clearJamAction="GestureReloadAK12_Drum";
		reloadAction="GestureReloadAK12_Drum";
		reloadMagazineSound[]=
		{
			"MA_Weapons\data\SAW\Saw_Reload.ogg",
			2.5,
			1,
			100
		};
		recoil="MA_recoil_MA5";
		magazines[]=
		{
			"TFV_H4_AP_Magazine",
			"TFV_H4_JHP_MagAzine",
		};
		magazineWell[]=
		{
			"TFV_H4_Magwell",
		};
		selectionFireAnim="zasleh";
		class Library
		{
			libTextDesc="";
		};
		drySound[]=
		{
			"",
			0.39810717,
			1,
			20
		};
		modes[]=
		{
			"Single",
			"FullAuto",
		};
		fireLightDuration=0.050000001;
		fireLightIntensity=0.40000001;
		fireLightDiffuse[]={0,0,0.0099999998};
		fireLightAmbient[]={0,0,0};
		class Single: Mode_FullAuto
		{
			sounds[]=
			{
				"StandardSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					1,
					1800
				};
				begin2[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					1.015,
					1800
				};
				begin3[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					0.985,
					1800
				};
				begin4[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					1.01,
					1800
				};
				begin5[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					0.995,
					1800
				};
				soundBegin[]=
				{
					"begin1",
					0.20,
					"begin2",
					0.20,
					"begin3",
					0.20,
					"begin4",
					0.20,
					"begin5",
					0.20,
				};
				beginwater1[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1,
					1,
					400
				};
				soundBeginWater[]=
				{
					"beginwater1",
					1,
				};
			};
			reloadTime=0.0667;
			dispersion=0.00085;
			minRange=2;
			minRangeProbab=0.0099999998;
			midRange=200;
			midRangeProbab=0.0099999998;
			maxRange=400;
			maxRangeProbab=0.0099999998;
		};
		class FullAuto: Mode_FullAuto
		{
			sounds[]=
			{
				"StandardSound"
			};
			class BaseSoundModeType
			{
				weaponSoundEffect="DefaultRifle";
				closure1[]={};
				closure2[]={};
				soundClosure[]=
				{
					"closure1",
					0.5,
					"closure2",
					0.5
				};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					1,
					1800
				};
				begin2[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					1.015,
					1800
				};
				begin3[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					0.985,
					1800
				};
				begin4[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					1.01,
					1800
				};
				begin5[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1.5,
					0.995,
					1800
				};
				soundBegin[]=
				{
					"begin1",
					0.20,
					"begin2",
					0.20,
					"begin3",
					0.20,
					"begin4",
					0.20,
					"begin5",
					0.20,
				};
				beginwater1[]=
				{
					"MA_Weapons\data\SAW\Saw_Shot.ogg",
					1,
					1,
					400
				};
				soundBeginWater[]=
				{
					"beginwater1",
					1,
				};
			};
			reloadTime=0.0667;
			dispersion=0.00075000001;
			minRange=2;
			minRangeProbab=0.0099999998;
			midRange=200;
			midRangeProbab=0.0099999998;
			maxRange=400;
			maxRangeProbab=0.0099999998;
		};
		inertia=1.4;
		dexterity=1.7;
		initSpeed=-1;
		maxRecoilSway=0.015;
		swayDecaySpeed=1;
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				compatibleitems[]={};
			};
			class CowsSlot: CowsSlot
			{
				compatibleitems[]=
				{
					"optre_br55hb_scope",
					"OPTRE_M7_Sight",
					"OPTRE_Evo_Sight",
					"OPTRE_M73_SmartLink",
				};
				iconPosition[]={0.60000002,0.27000001};
				iconScale=0.15000001;
			};
			class PointerSlot: PointerSlot
			{
				compatibleitems[]=
				{
						"acc_pointer_IR",
						"ace_acc_pointer_green",
						"OPTRE_BMR_Laser",
						"OPTRE_DMR_Light",
						"OPTRE_BMR_Flashlight"
				};
			};
			class UnderBarrelSlot: UnderBarrelSlot
			{
				compatibleitems[]=
				{
					"bipod_01_F_blk",
					"bipod_02_F_blk",
					"bipod_03_F_blk"
				};
			};
		};
	};
};
class cfgMagazineWells
{
	class TFV_H4_Magwell 
	{
		TFV_LMG_Ammo[] = 
		{
			"TFV_H4_AP_Magazine",
			"TFV_H4_JHP_MagAzine",
		};
	};
};
class CfgMagazines
{
    class 20Rnd_762x51_Mag;
	class TFV_H4_AP_Magazine: 20Rnd_762x51_Mag
	{
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Team";
		displayName="[Vargr]200Rnd 7.62x51mm AP Drum Magazine";
		count=200;
		initSpeed=850;
		displaynameshort="7.62x51mm AP";
		descriptionshort="200 Rounds of Armor Piercing Ammo";
		mass=5;
		ammo="TFV_LMG_AP_Ammo";
		tracersEvery=1;
		lastRoundsTracer=200;
	};
    class TFV_H4_JHP_Magazine: 20Rnd_762x51_Mag 
    {
		scope=2;
		scopeArsenal=2;
		author="Task Force Vargr Aux Team";
		displayName="[Vargr] 200Rnd 7.62x51mm JHP Magazine";
		count=200;
		initSpeed=820;
		displaynameshort="7.62x51mm JHP";
		descriptionshort="200 Rounds of Jacketed Holoow Point Ammo";
		mass=5;
		ammo="TFV_LMG_JHP_Ammo";
		tracersEvery=1;
		lastRoundsTracer=200;
	};
};
