class CfgPatches
{
	class TFV_Weapons_M6D_Carbine
	{
		addonRootClass="OPTRE_Weapons_Pistol";
		author="Task Force Vargr Aux Mod";
		weapons[]=
		{
			"TFV_M6DC_Base",
            "TFV_M6DC_Foregrip",
			"TFV_M6DC_Black",
			"TFV_M6DC_Foregrip_B",
		};
		requiredAddons[]=
		{
			"OPTRE_Weapons_Pistol"
		};
		units[]={};
	};
};
class OPTRE_MuzzleSlot;
class OPTRE_CowsSlot_Rail;
class OPTRE_Pointers;
class OPTRE_UnderBarrelSlot_rail;
class CfgWeapons
{
    class ItemCore;
	class InventoryMuzzleItem_Base_F;
    class WeaponSlotsInfo;
    class Single;
    class FullAuto;
	class muzzle_snds_H;
	class OPTRE_M6D_Carbine_F;
	class TFV_M6DC_Base: OPTRE_M6D_Carbine_F
	{
        scope=1;
		author="Task Force Vargr Aux Team";
		displayName="[Vargr] M6D Carbine";
		baseWeapon="TFV_M6DC_Base";
		model="\OPTRE_Weapons_Pistols\M6D_Carbine\m6d_carbine.p3d";
		hiddenSelections[]=
		{
			"camoMain",
			"camoEmiss",
			"camoEmiss2",
			"camoFurniture"
		};
		hiddenSelectionsTextures[]=
		{
			"\OPTRE_Weapons_Pistols\M6D\Data\M6D_Main_co.paa",
			"\OPTRE_Weapons_Pistols\M6D\Data\M6D_Emmisve_co.paa",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\Furniture_co.paa",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\Furniture_co.paa"
		};
		hiddenSelectionsMaterials[]=
		{
			"\OPTRE_Weapons_Pistols\M6D\Data\M6D.rvmat",
			"\OPTRE_Weapons_Pistols\M6D\Data\M6D_Emmisve.rvmat",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\Furniture_Emissive.rvmat",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\Furniture.rvmat"
		};
		magazines[]=
		{
			"TFV_M6_Carbine_30rnd_AP_Magazine",
			"TFV_M6_Carbine_30rnd_JHP_Magazine",
			"TFV_M6_Carbine_60rnd_AP_Magazine",
			"TFV_M6_Carbine_60rnd_JHP_Magazine",
		};
		magazineWell[]=
		{
			"TFV_M6_Carbine_Magwell", 
		};
		handAnim[]=
		{
			"OFP2_ManSkeleton",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\anim\human_handanim.rtm",
			"Spartan_ManSkeleton",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\anim\m6d_carbine_spartan.rtm"
		};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: OPTRE_MuzzleSlot
			{
				class compatibleItems
				{
					OPTRE_M6D_Carbine_Suppressor=1;
					OPTRE_M6D_Carbine_Brake=1;
				};
			};
			class CowsSlot: OPTRE_CowsSlot_Rail
			{
				iconPosition[]={0.40000001,0.31999999};
				iconScale=0.15000001;
			};
			class PointerSlot: OPTRE_Pointers
			{
				class compatibleItems
				{
					OPTRE_M6D_Carbine_IR=1;
					OPTRE_M6D_Carbine_Vis_Red=1;
					OPTRE_M6D_Carbine_Flashlight=1;
				};
			};
			class UnderBarrelSlot: OPTRE_UnderBarrelSlot_rail
			{
			};
			mass=33;
		};
		modelOptics="-";
		inertia=0.0099999998;
		aimTransitionSpeed=1.5;
		dexterity=1.8;
		class ItemInfo
		{
			priority=2;
		};
		picture="\OPTRE_Weapons_Pistols\M6D_Carbine\Data\icons\weapon\silver.paa";
		UiPicture="\A3\Weapons_F\data\UI\icon_regular_CA.paa";
		class Library
		{
			libTextDesc="TODO";
		};
		ACE_barrelLength=177.8;
		ACE_barrelTwist=228.60001;
		initSpeed=-1.15;
		reloadAction="OPTRE_GestureReload_M6D_Carbine";
		recoil="OPTRE_M6D_Carbine_Recoil";
		reloadMagazineSound[]=
		{
			"OPTRE_Weapons_Pistols\data\sounds\reload",
			1,
			1,
			10
		};
		class GunParticles
		{
			class FirstEffect
			{
				directionName="Konec hlavne";
				effectName="OPTRE_LinearCompensator_Cloud";
				positionName="Usti hlavne";
			};
		};
		modes[]=
		{
			"Single",
			"FullAuto",
			"close",
			"short",
			"medium",
			"far"
		};
		class Single: Single
		{
			dispersion=0.001;
			reloadTime=0.1;
			minRange=0;
			midRange=0;
			maxRange=0;
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType;
			class StandardSound: BaseSoundModeType
			{
				soundSetShot[]=
				{
					"OPTRE_M6_Pistol_Shot_SoundSet",
					"4Five_Tail_SoundSet",
					"4Five_InteriorTail_SoundSet"
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[]=
				{
					"4Five_silencerShot_SoundSet",
					"4Five_silencerTail_SoundSet",
					"4Five_silencerInteriorTail_SoundSet"
				};
			};
		};
		class FullAuto: FullAuto
		{
			dispersion=0.001;
			reloadTime=0.1;
			minRange=0;
			midRange=0;
			maxRange=0;
			sounds[]=
			{
				"StandardSound",
				"SilencedSound"
			};
			class BaseSoundModeType;
			class StandardSound: BaseSoundModeType
			{
				soundSetShot[]=
				{
					"OPTRE_M6_Pistol_Shot_SoundSet",
					"4Five_Tail_SoundSet",
					"4Five_InteriorTail_SoundSet"
				};
			};
			class SilencedSound: BaseSoundModeType
			{
				SoundSetShot[]=
				{
					"4Five_silencerShot_SoundSet",
					"4Five_silencerTail_SoundSet",
					"4Five_silencerInteriorTail_SoundSet"
				};
			};
		};
		class close: FullAuto
		{
			aiBurstTerminable=0;
			showToPlayer=0;
			burst=7;
			burstRangeMax=12;
			aiRateOfFire=0.1;
			aiRateOfFireDispersion=0.30000001;
			aiRateOfFireDistance=50;
			minRange=0;
			minRangeProbab=0.69999999;
			midRange=50;
			midRangeProbab=0.75;
			maxRange=100;
			maxRangeProbab=0.2;
		};
		class short: close
		{
			burst=5;
			burstRangeMax=10;
			aiRateOfFire=0.1;
			aiRateOfFireDispersion=0.5;
			aiRateOfFireDistance=150;
			minRange=75;
			minRangeProbab=0.69999999;
			midRange=150;
			midRangeProbab=0.75;
			maxRange=200;
			maxRangeProbab=0.2;
		};
		class medium: close
		{
			burst=3;
			burstRangeMax=6;
			aiRateOfFire=0.1;
			aiRateOfFireDispersion=1;
			aiRateOfFireDistance=250;
			minRange=175;
			minRangeProbab=0.69999999;
			midRange=250;
			midRangeProbab=0.75;
			maxRange=350;
			maxRangeProbab=0.2;
		};
		class far: close
		{
			burst=2;
			burstRangeMax=5;
			aiRateOfFire=0.1;
			aiRateOfFireDispersion=1;
			aiRateOfFireDistance=150;
			minRange=250;
			minRangeProbab=0.69999999;
			midRange=350;
			midRangeProbab=0.75;
			maxRange=500;
			maxRangeProbab=0.2;
		};
	};
	class TFV_M6DC_Black: TFV_M6DC_Base
	{
        scope=2;
		picture="\OPTRE_Weapons_Pistols\M6D_Carbine\Data\icons\weapon\black.paa";
		baseWeapon="TFV_M6DC_Black";
		displayName="[Vargr] M6D Carbine Black";
		hiddenSelectionsTextures[]=
		{
			"\OPTRE_Weapons_Pistols\M6D\Data\camo\black\M6D_Main_co.paa",
			"\OPTRE_Weapons_Pistols\M6D\Data\M6D_Emmisve_co.paa",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\colors\black\Furniture_co.paa",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\colors\black\Furniture_co.paa"
		};
	};
	class TFV_M6DC_Foregrip: TFV_M6DC_Base
	{
        scope=1;
		baseWeapon="TFV_M6DC_Foregrip";
		displayName="[Vargr] M6D Carbine Foregrip";
		model="\OPTRE_Weapons_Pistols\M6D_Carbine\m6ds_foregrip_carbine.p3d";
		handAnim[]=
		{
			"OFP2_ManSkeleton",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\anim\m6d_carbine_foregrip_human_handanim.rtm",
			"Spartan_ManSkeleton",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\anim\m6d_carbine_foregrip_spartan_handanim.rtm"
		};
	};
	class TFV_M6DC_Foregrip_B: TFV_M6DC_Foregrip
	{
        scope=2;
		picture="\OPTRE_Weapons_Pistols\M6D_Carbine\Data\icons\weapon\black.paa";
		baseWeapon="TFV_M6DC_Foregrip_B";
		displayName="[Vargr] M6D Carbine Foregrip (Black)";
		hiddenSelectionsTextures[]=
		{
			"\OPTRE_Weapons_Pistols\M6D\Data\camo\black\M6D_Main_co.paa",
			"\OPTRE_Weapons_Pistols\M6D\Data\M6D_Emmisve_co.paa",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\colors\black\Furniture_co.paa",
			"\OPTRE_Weapons_Pistols\M6D_Carbine\Data\colors\black\Furniture_co.paa"
		};
	};
};
class cfgMagazinesWells
{
    class TFV_M6_Carbine_Magwell 
	{
		TFV_SMG_Ammo[] = 
		{
			"TFV_M6_Carbine_30rnd_AP_Magazine",
			"TFV_M6_Carbine_30rnd_JHP_Magazine",
			"TFV_M6_Carbine_60rnd_AP_Magazine",
			"TFV_M6_Carbine_60rnd_JHP_Magazine",
		};
	};
};
class cfgMagazines
{
	class OPTRE_26Rnd_127x40_Mag_Black_Tracer;
	class OPTRE_40Rnd_127x40_Drum_Black_Tracer;
	class TFV_M6_Carbine_30rnd_AP_Magazine: OPTRE_26Rnd_127x40_Mag_Black_Tracer
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr] 30Rnd .45 ACP AP";
	    ammo="TFV_SMG_AP_Ammo";
	    count=30;
	    initSpeed=320;
	    tracersEvery=1;
	    descriptionShort="30-round magazine loaded with .45 ACP Armor-Piercing rounds.";
	    mass=5;
	    ace_isbelt=0;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
		model = "\OPTRE_Weapons_Pistols\M6D_Carbine\m6_big_stick_mag.p3d";
		picture = "\OPTRE_Weapons_Pistols\M6D_Carbine\Data\colors\black\24BigStick_co.paa";
	    lastRoundsTracer = 30;
	};
	class TFV_M6_Carbine_30rnd_JHP_Magazine: TFV_M6_Carbine_30rnd_AP_Magazine
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr] 30Rnd .45 ACP JHP";
	    ammo="TFV_SMG_JHP_Ammo";
	    count=30;
	    initSpeed=280;
	    descriptionShort="30-round magazine loaded with .45 ACP Jacketed Hollow Point rounds.";
	};
    class TFV_M6_Carbine_60rnd_AP_Magazine: OPTRE_40Rnd_127x40_Drum_Black_Tracer
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr] 60Rnd .45 ACP AP";
	    ammo="TFV_SMG_AP_Ammo";
	    count=60;
	    initSpeed=320;
	    tracersEvery=1;
	    descriptionShort="60-round magazine loaded with .45 ACP Armor-Piercing rounds.";
	    mass=8;
	    ace_isbelt=0;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
		model = "\OPTRE_Weapons_Pistols\M6D_Carbine\m6_drum_mag.p3d";
		picture = "\OPTRE_Weapons_Pistols\M6D\Data\icons\magazine\drum\Black.paa";
	    lastRoundsTracer = 60;
	};
	class TFV_M6_Carbine_60rnd_JHP_Magazine: TFV_M6_Carbine_60rnd_AP_Magazine
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr] 60Rnd .45 ACP JHP";
	    ammo="TFV_SMG_JHP_Ammo";
	    count=60;
	    initSpeed=280;
	    descriptionShort="60-round magazine loaded with .45 ACP Jacketed Hollow Point rounds.";
	};
};
