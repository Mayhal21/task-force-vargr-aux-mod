class CfgPatches
{
	class TFV_Weapons_Socom
	{
		addonRootClass="A3_Weapons_F";
		requiredAddons[]=
		{
			"OPTRE_Core",
			"OPTRE_Weapons",
			"A3_Weapons_F"
		//	"ace_common"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"TFV_Socom",
			"TFV_M6C_compensator",
			"TFV_M6C_Laser",
			"TFV_M6C_Vis_Red_Laser",
			"TFV_M6C_Flashlight",
			"TFV_M6C_Scope",
			"TFV_M6_silencer"
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class WeaponSlotsInfo;
class CfgWeapons
{
    class hgun_Pistol_heavy_01_F;
	class TFV_Handgun_Base: hgun_Pistol_heavy_01_F
	{
		class WeaponSlotsInfo;
		class Single;
	};
	class TFV_Socom : TFV_Handgun_Base
	{
		dlc = "OPTRE";
		author = "Task Force Vargr Aux Team";	
		model					= "\OPTRE_Weapons_Pistols\M6C\m6c.p3d";
		displayName 			= "[Vargr] M6 Socom";
		descriptionShort 		= "9mm Handgun";
		picture 				= "\OPTRE_Weapons_Pistols\M6C\data\icons\m6c_icon.paa";
		magazines[] 			= {"TFV_Socom_AP_Magazine","TFV_Socom_JHP_Magazine"};
        magazineWell[]          = {"TFV_Socom_Magwell"};
		recoil					= "recoil_pistol_zubr";
		baseWeapon 				= "TFV_Socom";
		cursor					= "OPTRE_M6C";
		pictureWire 			= "\OPTRE_Weapons\data\Pictures\WireWeaponIcons\Pistols\M6F_IRON.paa";
		ODST_1					= "OPTRE_ODST_HUD_AmmoCount_PistolODST";
		Glasses					= "OPTRE_GLASS_HUD_AmmoCount_PistolODST";
		Eye						= "OPTRE_EYE_HUD_AmmoCount_PistolSmart";
		HUD_BulletInARows		= 1;
		HUD_TotalPosibleBullet	= 18;
		
		hiddenSelections[] 			= {"camo1"};
		hiddenSelectionsTextures[]	= {"\OPTRE_Weapons_Pistols\M6C\data\M6C_M6C_co.paa"};

		class WeaponSlotsInfo: WeaponSlotsInfo 
 		{
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="Muzzle Slot";
                compatibleItems[]=
                {
                    "TFV_M6C_compensator",
                    "TFV_M6_silencer"
                };
                iconPosition[]={0,0.44999999};
                iconScale=0.2;
            };
            class CowsSlot: CowsSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\TOP";
                displayName="$STR_A3_CowsSlot0";
                compatibleitems[]=
                {
                    "TFV_M6C_Scope"
                };
                iconPosition[]={0.5,0.34999999};
                iconScale=0.2;
            };
            class PointerSlot: PointerSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\BOTTOM";
                displayName="$STR_A3_PointerSlot0";
                compatibleItems[]=
                {
                    "TFV_M6C_Flashlight",
                    "TFV_M6C_Vis_Red_Laser",
                    "TFV_M6C_Laser"
                };
                iconPosition[]={0.2,0.44999999};
                iconScale=0.25;
            };
        };
		mass = 16;
		class Single : Single
		{
			sounds[] = {"StandardSound","SilencedSound"};
			class BaseSoundModeType;											
			class StandardSound: BaseSoundModeType								
			{								
				soundSetShot[] = {
					"OPTRE_M6_Pistol_Shot_SoundSet",
					"4Five_Tail_SoundSet",
					"4Five_InteriorTail_SoundSet"
				};
			};
            class SilencedSound: BaseSoundModeType
            {
				SoundSetShot[] = {
					"4Five_silencerShot_SoundSet",
					"4Five_silencerTail_SoundSet",
					"4Five_silencerInteriorTail_SoundSet"
				};
            };
			reloadTime 			= 0.025; //0.075
			minRange 			= 10;
			minRangeProbab 		= 0.5;
			midRange 			= 25;
			midRangeProbab 		= 0.1;
			maxRange 			= 100;
			maxRangeProbab 		= 0.05;
			distanceZoomMin 	= 100;
			distanceZoomMax 	= 100;
		};
	};
	class ItemCore;
	class optic_Aco: ItemCore
	{
		class ItemInfo;
	};
	class acc_pointer_IR;
	class muzzle_snds_acp;
	class InventoryMuzzleItem_Base_F;
	class InventoryFlashLightItem_Base_F;
	class TFV_M6C_compensator: muzzle_snds_acp
	{
		dlc="OPTRE";
		author="Task Force Vargr Aux Team";
		scope=2;
		scopeArsenal=2;
		displayName="[Vargr] M6C Compensator";
		model="\OPTRE_Weapons_Pistols\M6C\m6c_comp.p3d";
		descriptionShort="M6C Compensator";
		inertia=0.1;
		class ItemInfo: InventoryMuzzleItem_Base_F
		{
			mass=1;
			class MagazineCoef
			{
				initSpeed=1;
			};
			class AmmoCoef
			{
				hit=1;
				typicalSpeed=1;
				airFriction=0.80000001;
				visibleFire=0.75;
				audibleFire=0.75;
				visibleFireTime=0.5;
				audibleFireTime=0.5;
				cost=1;
			};
			muzzleEnd="zaslehPoint";
			alternativeFire="OPTRE_muzzleFlash_suppressed";
			soundTypeIndex=2;
			class MuzzleCoef
			{
				dispersionCoef="0.75f";
				artilleryDispersionCoef=1;
				fireLightCoef="0.1f";
				recoilCoef="0.25f";
				recoilProneCoef="0.25f";
				minRangeCoef="1.0f";
				minRangeProbabCoef="1.0f";
				midRangeCoef="1.0f";
				midRangeProbabCoef="1.0f";
				maxRangeCoef="1.0f";
				maxRangeProbabCoef="1.0f";
			};
		};
	};
	class TFV_M6C_Laser: acc_pointer_IR
	{
		dlc="OPTRE";
		author="Task Force Vargr Aux Team";
		scope=2;
		scopeArsenal=2;
		displayName="[Vargr} M6C LAM";
		descriptionShort="IR Laser for the M6C Handgun";
		model="\OPTRE_Weapons_Pistols\M6C\m6c_lam.p3d";
		inertia=0.1;
		class ItemInfo: InventoryFlashLightItem_Base_F
		{
			mass=1;
			class Pointer
			{
				irLaserPos="laser dir";
				irLaserEnd="laser";
				irDistance=25;
			};
		};
		MRT_SwitchItemNextClass="TFV_M6C_Vis_Red_Laser";
		MRT_SwitchItemPrevClass="TFV_M6C_Flashlight";
		MRT_switchItemHintText="IR Laser";
	};
	class TFV_M6C_Vis_Red_Laser: TFV_M6C_Laser
	{
		displayName="[Vargr] M6C Laser";
		scopeArsenal=1;
		class ItemInfo: ItemInfo
		{
			mass=6;
			class Pointer
			{
				irLaserPos="laser dir";
				irLaserEnd="laser";
				beamColor[]={15,0,0};
				dotColor[]={1000,0,0};
				dotSize=1;
				beamThickness=0.25;
				beamMaxLength=3000;
				isIR=0;
			};
			class FlashLight
			{
				irLaserPos="laser dir";
				irLaserEnd="laser";
				irDistance=25;
			};
		};
		MRT_SwitchItemNextClass="TFV_M6C_Flashlight";
		MRT_SwitchItemPrevClass="TFV_M6C_Laser";
		MRT_switchItemHintText="Visible Red";
	};
	class TFV_M6C_Flashlight: TFV_M6C_Laser
	{
		displayName="{Vargr] M6C Flashlight";
		model="\OPTRE_Weapons_Pistols\M6C\m6c_lam.p3d";
		MRT_SwitchItemNextClass="TFV_M6C_Laser";
		MRT_SwitchItemPrevClass="TFV_M6C_Vis_Red_Laser";
		MRT_switchItemHintText="Flashlight";
		picture="\A3\weapons_F\Data\UI\gear_accv_pointer_CA.paa";
		scopeArsenal=1;
		class ItemInfo: ItemInfo
		{
			class FlashLight
			{
				color[]={180,156,120};
				ambient[]={0.89999998,0.77999997,0.60000002};
				intensity=5;
				size=1;
				innerAngle=20;
				outerAngle=80;
				coneFadeCoef=5;
				position="laser dir";
				direction="laser";
				useFlare=1;
				flareSize=1.4;
				flareMaxDistance="100.0f";
				dayLight=0;
				class Attenuation
				{
					start=0.5;
					constant=0;
					linear=0;
					quadratic=1.1;
					hardLimitStart=20;
					hardLimitEnd=30;
				};
				scale[]={0};
			};
		};
	};
	class TFV_M6C_Scope: optic_Aco
	{
		dlc="OPTRE";
		author="Task Force Vargr Aux Team";
		scope=2;
		scopeArsenal=2;
		displayName="[Vargr] M6C SmartLink Scope";
		picture="\OPTRE_Weapons_Pistols\M6C\data\icons\scope.paa";
		model="\OPTRE_Weapons_Pistols\M6C\m6c_scope.p3d";
		descriptionShort="VnSLS/V 6E 4x SmartLink Scope for M6C Magnum";
		weaponInfoType="RscWeaponZeroing";
		class ItemInfo: ItemInfo
		{
			modelOptics="\OPTRE_Weapons_Pistols\M6C\m6c_scope.p3d";
			class OpticsModes
			{
				class OPTRE_Scope_BUIS
				{
					opticsID=1;
					useModelOptics=0;
					opticsPPEffects[]=
					{
						""
					};
					opticsFlare=0;
					opticsDisablePeripherialVision=0;
					opticsZoomMin=0.40000001;
					opticsZoomMax=1.1;
					opticsZoomInit=0.75;
					memoryPointCamera="opticView2";
					visionMode[]={};
					distanceZoomMin=100;
					distanceZoomMax=600;
					discreteDistance[]={100,200,300,400,500,600};
					discreteDistanceInitIndex=0;
					cameraDir="";
				};
				class OPTRE_Scope_BUISZoom: OPTRE_Scope_BUIS
				{
					opticsID=2;
					useModelOptics=1;
					opticsZoomMin="2 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')";
					opticsZoomMax="2 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')";
					opticsZoomInit="2 call (uiNamespace getVariable 'cba_optics_fnc_setOpticMagnificationHelper')";
					modelOptics[]=
					{
						"\OPTRE_Weapons_Pistols\M6C\M6C_Optic_2x"
					};
					visionMode[]=
					{
						"Normal",
						"NVG"
					};
				};
			};
		};
		inertia=0.1;
	};
	class TFV_M6_silencer: muzzle_snds_acp
	{
		dlc="OPTRE";
		author="Task Force Vargr Aux Team";
		scope=2;
		scopeArsenal=2;
		displayName="[Vargr] M6 Suppressor";
		model="\OPTRE_Weapons_Pistols\M6C\m6c_silencer.p3d";
		descriptionShort="M6 Series Suppressor";
		inertia=0.1;
		class ItemInfo: InventoryMuzzleItem_Base_F
		{
			mass=1;
			class MagazineCoef
			{
				initSpeed=1;
			};
			class AmmoCoef
			{
				hit=1;
				typicalSpeed=1;
				airFriction=1;
				visibleFire=0.5;
				audibleFire=0.30000001;
				visibleFireTime=0.5;
				audibleFireTime=0.5;
				cost=1;
			};
			muzzleEnd="zaslehPoint";
			alternativeFire="OPTRE_muzzleFlash_suppressed";
			soundTypeIndex=1;
			class MuzzleCoef
			{
				dispersionCoef=1;
				artilleryDispersionCoef=1;
				fireLightCoef="0.1f";
				recoilCoef="0.8f";
				recoilProneCoef="0.8f";
				minRangeCoef="1.0f";
				minRangeProbabCoef="1.0f";
				midRangeCoef="1.0f";
				midRangeProbabCoef="1.0f";
				maxRangeCoef="1.0f";
				maxRangeProbabCoef="1.0f";
			};
		};
	};
    class TFV_Socom_SF: TFV_Socom
	{
		scope=1;
		scopeArsenal=1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				slot="CowsSlot";
				item="TFV_M6C_Scope";
			};
			class LinkedItemsMuzzle
			{
				slot="MuzzleSlot";
				item="TFV_M6C_compensator";
			};
			class LinkedItemsAcc
			{
				slot="PointerSlot";
				item="TFV_M6C_Laser";
			};
		};
	};
};
class cfgMagazineWells
{
	class TFV_Socom_Magwell 
	{
		TFV_Pistol_Ammo[] = 
		{
			"TFV_Socom_AP_Magazine",
			"TFV_Socom_JHP_Magazine",
		};
	};
};
class CfgMagazines
{
    class CA_Magazine;
    class TFV_Socom_AP_Magazine: CA_Magazine
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr] 18Rnd 9mm AP Magazine";
	    ammo="TFV_Pistol_AP_Ammo";
	    count=18;
	    initSpeed=850;
	    tracersEvery=1;
	    descriptionShort="18-round magazine loaded with 9mm Armor-Piercing rounds.";
	    mass=5;
	    ace_isbelt=0;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
        model= "\OPTRE_Weapons_Pistols\M6D\m6_extended_magazine.p3d";
        modelSpecial = "\OPTRE_Weapons_Pistols\M6D\m6_extended_magazine.p3d";
        modelSpecialIsProxy= 1;
        hiddenSelections[]= {"camoMag"};
        hiddenSelectionsTextures[]= {"\OPTRE_Weapons_Pistols\M6D\Data\camo\black\M6D_Magazine_co.paa"};
        picture= "\OPTRE_Weapons_Pistols\M6D\Data\icons\magazine\mag_base.paa";
	    lastRoundsTracer = 18;
	};
    class TFV_Socom_JHP_Magazine: CA_Magazine
	{
	    author="Task Force Vargr Aux Team";
	    scope=2;
	    displayName="[Vargr] 18Rnd 9mm JHP Magazine";
	    ammo="TFV_Pistol_JHP_Ammo";
	    count=18;
	    initSpeed=800;
	    tracersEvery=1;
	    descriptionShort="18-round magazine loaded with 9mm Jacketed Hollow Point rounds.";
	    mass=5;
	    ace_isbelt=0;
	    ace_attachable=0;
	    ace_placeable=0;
	    ace_setupobject=0;
	    ace_delaytime=0;
	    ace_triggers=0;
	    ace_magazines_forcemagazinemuzzlevelocity=1;
        model= "\OPTRE_Weapons_Pistols\M6D\m6_extended_magazine.p3d";
        modelSpecial = "\OPTRE_Weapons_Pistols\M6D\m6_extended_magazine.p3d";
        modelSpecialIsProxy= 1;
        hiddenSelections[]= {"camoMag"};
        hiddenSelectionsTextures[]= {"\OPTRE_Weapons_Pistols\M6D\Data\camo\black\M6D_Magazine_co.paa"};
        picture= "\OPTRE_Weapons_Pistols\M6D\Data\icons\magazine\mag_base.paa";
	    lastRoundsTracer = 18;
	};
};