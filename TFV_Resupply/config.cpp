/*#define MACRO_ITEM_ADD(CLASSNAME,COUNT) class _xx_##CLASSNAME { \
    name = #CLASSNAME; \
    count = COUNT; \
}
#define MACRO_MAG_ADD(CLASSNAME,COUNT) class _xx_##CLASSNAME { \
    magazine = #CLASSNAME; \
    count = COUNT; \
}
#define MACRO_WEAPON_ADD(CLASSNAME,COUNT) class _xx_##CLASSNAME { \
    weapon = #CLASSNAME; \
    count = COUNT; \
}*/
class CfgPatches
{
    class tfv_props
    {
        units[]=
        {
            //Pods
            "TFV_Base_Pod",
            "TFV_Standard_Pod",
            "TFV_Medical_Pod",
        };
        weapons[]={};
        author="Task Force Vargr Aux Team";
        requiredAddons[]={ "OPTRE_Misc_Crates" };
    };
};
class CfgFactionClasses
{
	class TFV
	{
		displayName="Task Force Vargr";
		priority=3;
		side=1;
		icon="TFV_Armor\data\Vargr_Logo\PAA\Vargr_Emblem.paa";
	};
};
class CfgEditorSubcategories
{
    class TFV_Resupply
    {
        displayName="Supply Pods";
    };
};
class DefaultEventhandlers;
class Module_F;
class CfgVehicles
{
    class OPTRE_Ammo_SupplyPod_Empty;
    class TFV_Base_Pod:OPTRE_Ammo_SupplyPod_Empty
{
	scope=1;
	scopecurator=1;
    faction="TFV";
    editorSubcategory="TFV_Resupply";
	maximunLoad=1000000;
	hiddenselections[]=
	{
		"camo"
	};
	hiddenselectionstextures[]=
	{
		"optre_misc\crates\data\supplypod_co.paa"
	};
};
    class TFV_Standard_Pod: TFV_Base_Pod
{
    scope=2;
    scopeCurator=2;
    displayName="[TFV] Supply Pod (Standard)";
    model="\OPTRE_misc\crates\Supply_pod.p3d";
    author="Task Force Vargr Aux Team";
    icon="iconCrateWpns";
    faction="TFV";
    editorSubcategory="TFV_Resupply";
    hiddenselections[]=
    {
        "camo"
    };
    hiddenselectionstextures[]=
    {
        "optre_misc\crates\data\supplypod_co.paa"
    };
    supplyRadius=1.5;
    armor=10000;
    class TransportMagazines
    {
        /*MACRO_MAG_ADD(TCF_36Rnd_95x40_Mag_JHPT,200);
        MACRO_MAG_ADD(TCF_60Rnd_762x51_Mag_JHP,200);
        MACRO_MAG_ADD(OPTRE_8Rnd_127x40_AP_Mag,100);*/
        //BR Mags
        class _TFV_BR55_Mag
        {
            magazine="TFV_BR55_Mag";
            count=100;
        };
        class _TFV_BR55_Mag_Tracer
        {
            magazine="TFV_BR55_Mag_Tracer";
            count=100;
        };
        class _TFV_BR55_Mag_AP
        {
            magazine="TFV_BR55_Mag_AP";
            count=100;
        };
        //Bulldog Mags
        class _TFV_12Gauge_Mag 
        {
            magazine="TFV_12Gauge_Mag";
            count=100;
        };
        class _TFV_12Gauge_Mag_Tracer 
        {
            magazine="TFV_12Gauge_Mag_Tracer";
            count=100;
        };
        class _TFV_12Gauge_slug_Tracer
        {
            magazine="TFV_12Gauge_slug_Tracer";
            count=100;
        };
        //Eltika Mags
        class _TFV_500Rnd_Etilka_HE
        {
            magazine="TFV_500Rnd_Etilka_HE";
            count=100;
        };
        class _TFV_1000Rnd_Etilka_Ball
        {
            magazine="TFV_1000Rnd_Etilka_Ball";
            count=100;
        };
        //GRL-45 Mags
        class _TFV_GRL45_6rnd_HEAT
        {
            magazine="TFV_GRL45_6rnd_HEAT";
            count=100;
        };
        class _ACE_HuntIR_M203
        {
            magazine="ACE_HuntIR_M203";
            count=100;
        };
        class _TFV_GRL45_6rnd_Smoke
        {
            magazine="TFV_GRL45_6rnd_Smoke";
            count=100;
        };
        class _TFV_GRL45_6rnd_Smoke_Blue
        {
            magazine="TFV_GRL45_6rnd_Smoke_Blue";
            count=100;
        };
        class _TFV_GRL45_6rnd_Smoke_Purple
        {
            magazine="TFV_GRL45_6rnd_Smoke_Purple";
            count=100;
        };
        class _TFV_GRL45_6rnd_Smoke_Green
        {
            magazine="TFV_GRL45_6rnd_Smoke_Green";
            count=100;
        };
        class _TFV_GRL45_6rnd_Smoke_Red
        {
            magazine="TFV_GRL45_6rnd_Smoke_Red";
            count=100;
        };
        //H4 Saw Mags
        class _TFV_400Rnd_Mag
        {
            magazine="TFV_400Rnd_Mag";
            count=100;
        };
        class _TFV_400Rnd_Mag_Tracer
        {
            magazine="TFV_400Rnd_Mag_Tracer";
            count=100;
        };
        //M6C Mags
        class _TFV_M6C_Mag
        {
            magazine="TFV_M6C_Mag";
            count=100;
        };
        class _TFV_M6C_Mag_Tracer
        {
            magazine="TFV_M6C_Mag_Tracer";
            count=100;
        };
        //M7 Mags
        class _TFV_M7_Mag
        {
            magazine="TFV_M7_Mag";
            count=100;
        };
        class _TFV_M7_Mag_Tracer
        {
            magazine="TFV_M7_Mag_Tracer";
            count=100;
        };
        //M90 Mags
        class _TFV_8Gauge_Mag
        {
            magazine="TFV_8Gauge_Mag";
            count=100;
        };
        class _TFV_8Gauge_Mag_Tracer
        {
            magazine="TFV_8Gauge_Mag_Tracer";
            count=100;
        };
        class _TFV_8Gauge_slug_tracer
        {
            magazine="TFV_8Gauge_slug_tracer";
            count=100;
        };
        //DMR Mags
        class _TFV_M392_Mag
        {
            magazine="TFV_M392_Mag";
            count=100;
        };
        class _TFV_M392_Mag_Tracer
        {
            magazine="TFV_M392_Mag_Tracer";
            count=100;
        };
        //MA5A Mags
        class _TFV_MA5A_Mag
        {
            magazine="TFV_MA5A_Mag";
            count=100;
        };
        class _TFV_MA5A_Mag_Tracer
        {
            magazine="TFV_MA5A_Mag_Tracer";
            count=100;
        };
        //MK50 Sidekick
        class _TFV_MK50_Mag
        {
            magazine="TFV_MK50_Mag";
            count=100;
        };
        class _TFV_MK50_Mag_Tracer
        {
            magazine="TFV_MK50_Mag_Tracer";
            count=100;
        };
        //SRS99 Mags
        class _TFV_MA5A_SRS_APFSDS_Mag
        {
            magazine="TFV_MA5A_SRS_APFSDS_Mag";
            count=100;
        };
        class _TFV_MA5A_SRS_APFSDS_Mag_tracer
        {
            magazine="TFV_MA5A_SRS_APFSDS_Mag_tracer";
            count=100;
        };
        class _TFV_MA5A_SRS_HEAP_Mag
        {
            magazine="TFV_MA5A_SRS_HEAP_Mag";
            count=100;
        };
        class _TFV_MA5A_SRS_HEAP_Mag_Tracer
        {
            magazine="TFV_MA5A_SRS_HEAP_Mag_Tracer";
            count=100;
        };
        //GL Tube Ammo
        class _TFV_GRL45_3rnd_HEAT
        {
            magazine="TFV_GRL45_3rnd_HEAT";
            count=100;
        };
        class _TFV_GRL45_3rnd_Smoke
        {
            magazine="TFV_GRL45_3rnd_Smoke";
            count=100;
        };
        class _TFV_GRL45_3rnd_Smoke_Blue
        {
            magazine="TFV_GRL45_3rnd_Smoke_Blue";
            count=100;
        };
        class _TFV_GRL45_3rnd_Smoke_Purple
        {
            magazine="TFV_GRL45_3rnd_Smoke_Purple";
            count=100;
        };
        class _TFV_GRL45_3rnd_Smoke_Green
        {
            magazine="TFV_GRL45_3rnd_Smoke_Green";
            count=100;
        };
        class _TFV_GRL45_3rnd_Smoke_Red
        {
            magazine="TFV_GRL45_3rnd_Smoke_Red";
            count=100;
        };
        //M41 Rocket Ammo
        class _TFV_M41_HEAT_Ungiuded

        {
            magazine="TFV_M41_HEAT_Ungiuded";
            count=20;
        };
        class _TFV_M41_HEAT_SACLOS
        {
            magazine="TFV_M41_HEAT_SACLOS";
            count=20;
        };
        class _TFV_M41_Twin_HEAT_Thermal
        {
            magazine="TFV_M41_Twin_HEAT_Thermal";
            count=20;
        };
        //Laser Ammo
        class _TFV_Laser_Mag
        {
            magazine="TFV_Laser_Mag";
            count=20;
        };
        //M96 Law
        class _TFV_M96_AT
        {
            magazine="TFV_M96_AT";
            count=20;
        };
        // Grenades
        class _MA_M9_Frag_Mag
        {
            magazine="MA_M9_Frag_Mag";
            count=100;
        };
        class _ACE_M84
        {
            magazine="ACE_M84";
            count=100;
        };
        class _MA_M8_Smoke_Blue
        {
            magazine="MA_M8_Smoke_Blue";
            count=100;
        };
        class _MA_M8_Smoke_Green
        {
            magazine="MA_M8_Smoke_Green";
            count=100;
        };
        class _MA_M8_Smoke_Purple
        {
            magazine="MA_M8_Smoke_Purple";
            count=100;
        };
        class _MA_M8_Smoke_Red
        {
            magazine="MA_M8_Smoke_Red";
            count=100;
        };
        class _MA_M8_Smoke_White
        {
            magazine="MA_M8_Smoke_White";
            count=100;
        };
    };
    class TransportWeapons
    {
        //BR55 Rifles
        class _TFV_UNSC_BR55
        {
            weapon="TFV_UNSC_BR55";
            count=50;
        };
        class _TTFV_UNSC_BR55_GL
        {
            weapon="TFV_UNSC_BR55_GL";
            count=50;
        };
        class _TFV_UNSC_BR55_HB
        {
            weapon="TFV_UNSC_BR55_HB";
            count=50;
        };
        //Bulldog
        class _TFV_Bulldog
        {
            weapon="TFV_Bulldog";
            count=50;
        };
        //Etilka Machine Gun
        class _TFV_Etilka
        {
            weapon="TFV_Etilka";
            count=50;
        };
        //GRL-45
        class _TFV_GRL_45
        {
            weapon="TFV_GRL_45";
            count=50;
        };
        //H4 SAW
        class _TFV_H4_SAW
        {
            weapon="TFV_H4_SAW";
            count=50;
        };
        //M6C Pistol
        class _TFV_UNSC_M6C
        {
            weapon="TFV_UNSC_M6C";
            count=50;
        };
        //M7 Submachine Gun
        class _TFV_M7_Sub
        {
            weapon="TFV_M7_Sub";
            count=50;
        };
        class _TFV_M7_Side
        {
            weapon="TFV_M7_Side";
            count=50;
        };
        // M90 Shotgun
        class _TFV_UNSC_M90
        {
            weapon="TFV_UNSC_M90";
            count=50;
        };
        //DMR Rifle
        class _TFV_392_DMR
        {
            weapon="TFV_392_DMR";
            count=50;
        };
        //MA5A Rifle
        class _TFV_UNSC_MA5A
        {
            weapon="TFV_UNSC_MA5A";
            count=50;
        };
        class _TFV_UNSC_MA5A_GL
        {
            weapon="TFV_UNSC_MA5A_GL";
            count=50;
        };
        //MK50 Sidekick
        class _TTFV_MK50
        {
            weapon="TFV_MK50";
            count=50;
        };
        //SRS99 Rifle
        class _TFV_UNSC_SRS99
        {
            weapon="TFV_UNSC_SRS99";
            count=50;
        };
    };
    class TransportItems
    {
        //Misc
        class _ACE_CableTie
        {
            name="ACE_CableTie";
            count=100;
        };
        class _WBK_HeadLampItem
        {
            name="WBK_HeadLampItem";
            count=100;
        };
        class _ACE_IR_Strobe_Item
        {
            name="ACE_IR_Strobe_Item";
            count=100;
        };
        class _ItemcTabHCam
        {
            name="ItemcTabHCam";
            count=100;
        };
        //Weapon Scopes
        class _19_UNSC_br_scope
        {
            name="19_UNSC_br_scope";
            count=100;
        };
        class _Optre_Evo_Sight_Riser
        {
            name="Optre_Evo_Sight_Riser";
            count=100;
        };
        class _Optre_Evo_Sight
        {
            name="Optre_Evo_Sight";
            count=100;
        };
        class _OPTRE_BR55HB_Scope
        {
            name="OPTRE_BR55HB_Scope";
            count=100;
        };
        class _OPTRE_M7_Sight
        {
            name="OPTRE_M7_Sight";
            count=100;
        };
        class _OPTRE_M73_SmartLink
        {
            name="OPTRE_M73_SmartLink";
            count=100;
        };
        class _19_UNSC_M7_optic
        {
            name="19_UNSC_M7_optic";
            count=100;
        };
        class _19_UNSC_evo
        {
            name="19_UNSC_evo";
            count=100;
        };
        class _19_UNSC_evosd
        {
            name="19_UNSC_evosd";
            count=100;
        };
        class _19_UNSC_MA5A_smartlink
        {
            name="19_UNSC_MA5A_smartlink";
            count=100;
        };
        class _19_UNSC_SRS99AM_scope
        {
            name="19_UNSC_SRS99AM_scope";
            count=100;
        };
        //Lasers and Lights
        class _19_UNSC_BR55_LAM
        {
            name="19_UNSC_BR55_LAM";
            count=100;
        };
        //Suppressors
        class _19_UNSC_BR55_Suppressor
        {
            name="19_UNSC_BR55_Suppressor";
            count=100;
        };
        class _19_UNSC_m7_Suppressor
        {
            name="_19_UNSC_m7_Suppressor";
            count=100;
        };
        class _muzzle_snds_acp
        {
            name="_muzzle_snds_acp";
            count=100;
        };
    };
    editorPreview="\OPTRE_Misc\Image\OPTRE\Supply\OPTRE_Ammo_SupplyPod_Empty.jpg";
};
    class TFV_Medical_Pod: TFV_Base_Pod
{
    scope=2;
    scopeCurator=2;
    displayName="[TFV] Supply Pod (Medical)";
    model="\OPTRE_misc\crates\Supply_pod.p3d";
    author="Task Force Vargr Aux Team";
    icon="iconCrateWpns";
    faction="TFV";
    editorSubcategory="TFV_Resupply";
    hiddenselections[]=
    {
        "camo"
    };
    hiddenselectionstextures[]=
    {
        "optre_misc\crates\data\supplypod_co.paa"
    };
    supplyRadius=1.5;
    armor=10000;
    class TransportMagazines{};
    class TransportWeapons{};
    class TransportItems
    {
        //Bandages
        class _ACE_elasticBandage
        {
            name="ACE_elasticBandage";
            count=100;
        };
		class _ACE_packingBandage
        {
            name="ACE_packingBandage";
            count=100;
        };
		class _ACE_quickclot
        {
            name="ACE_quickclot";
            count=25;
        };
		class _OPTRE_Biofaom
        {
            name="OPTRE_Biofaom";
            count=25;
        };
		class _OPTRE_Medigel
        {
            name="OPTRE_Medigel";
            count=20;
        };
		//Tools
		class _ACE_splint
        {
            name="ACE_splint";
            count=16;
        };
		class _ACE_tourniquet
        {
            name="ACE_tourniquet";
            count=16;
        };
		class _kat_IO_FAST
        {
            name="kat_IO_FAST";
            count=6;
        };
		class _kat_IV_16
        {
            name="kat_IV_16";
            count=6;
        };
		//Medicine
		class _ACE_adenosine
        {
            name="ACE_adenosine";
            count=8;
        };
		class _ACE_epinephrine
        {
            name="ACE_epinephrine";
            count=25;
        };
		class _ACE_morphine
        {
            name="ACE_morphine";
            count=10;
        };
		class _kat_Painkiller
        {
            name="kat_Painkiller";
            count=25;
        };
		class _kat_phenylephrineAuto
        {
            name="kat_phenylephrineAuto";
            count=25;
        };
		class _kat_TXA
        {
            name="kat_TXA";
            count=6;
        };
		class _kat_EACA
        {
            name="kat_EACA";
            count=6;
        };
		//Blood
		class _ACE_plasmaIV
        {
            name="ACE_plasmaIV";
            count=18;
        };
		class _ACE_plasmaIV_250
        {
            name="ACE_plasmaIV_500";
            count=10;
        };
		class _ACE_plasmaIV_500
        {
            name="ACE_plasmaIV_250";
            count=8;
        };
    };
    editorPreview="\OPTRE_Misc\Image\OPTRE\Supply\OPTRE_Ammo_SupplyPod_Empty.jpg";
};
    class Module_OPTRE_PelicanSupplyDrop: Module_F
{
    class Arguments
    {
        class box1
          {
            class values
            {
                class n1    {name = "none"; value = "none"; }; 
                class n2    {name = "Random Supply Pod"; value = "rdm"; default = 1;}; 
                class n3    {name = "Sniper [OLD] Supply Pod"; value = "OPTRE_Ammo_SupplyPod_Sniper";}; 
                class n4    {name = "Medical Supply Pod"; value = "OPTRE_Ammo_SupplyPod_Medical";}; 
                class n5    {name = "MA5B Supply Pod"; value = "OPTRE_Ammo_SupplyPod_AR";}; 
                class n6    {name = "M73 LMG Supply Pod"; value = "OPTRE_Ammo_SupplyPod_LMG";}; 
                class n7    {name = "M7 SMG Supply Pod"; value = "OPTRE_Ammo_SupplyPod_SMG";}; 
                class n8    {name = "Pistol Supply Pod"; value = "OPTRE_Ammo_SupplyPod_Pistol";}; 
                class n9    {name = "Shotgun Supply Pod"; value = "OPTRE_Ammo_SupplyPod_Shotgun";}; 
                class n10    {name = "Launcher Supply Pod"; value = "OPTRE_Ammo_SupplyPod_Launcher";}; 
                class n11    {name = "DMR Supply Pod"; value = "OPTRE_Ammo_SupplyPod_DMR";};                     
                class n12    {name = "BR Supply Pod"; value = "OPTRE_Ammo_SupplyPod_BR";};
                
                class n13    {name = "MA5K Supply Pod"; value = "OPTRE_Ammo_SupplyPod_MA5K";};                     
                class n14    {name = "SRS99C Supply Pod"; value = "OPTRE_Ammo_SupplyPod_SRS99C";};                    
                class n15    {name = "MA5C Supply Pod"; value = "OPTRE_Ammo_SupplyPod_ARC";};                     
                class n16    {name = "MA5C-GL Supply Pod"; value = "OPTRE_Ammo_SupplyPod_ARC_GL";};                
                class n17    {name = "M37 Supply Pod"; value = "OPTRE_Ammo_SupplyPod_37";};                     
                class n18    {name = "M37-GL Supply Pod"; value = "OPTRE_Ammo_SupplyPod_37_GL";};                
                class n19    {name = "MA5B-GL Supply Pod"; value = "OPTRE_Ammo_SupplyPod_AR_GL";};                                         
                
                class n20    {name = "CUSTOM Supply Pod"; value = "OPTRE_Ammo_SupplyPod_Empty";};

                class n21    {name = "TFV Standard Pod"; value = "TFV_Standard_Pod";};
                class n22    {name = "TFV Medical Pod"; value = "TFV_Medical_Pod";};  
            };
        };
        class box2: box1 {displayName = "Supply Pod 2";};
        class box3: box1 {displayName = "Supply Pod 3";};
        class box4: box1 {displayName = "Supply Pod 4";};
        class box5: box1 {displayName = "Supply Pod 5";};
        class box6: box1 {displayName = "Supply Pod 6";};
    };    
};
};